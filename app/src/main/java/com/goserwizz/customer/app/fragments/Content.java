package com.goserwizz.customer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;

/**
 * Created by Kaptas on 3/11/2016.
 */
public class Content extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_main, container, false);

        WebView view = new WebView(getActivity());
        view.setVerticalScrollBarEnabled(false);

        ((LinearLayout)v.findViewById(R.id.ContentLayout)).addView(view);

        String content = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CONTENT, "");


        if(content.equals("callsupport")) {
            view.loadData(getString(R.string.callsupport), "text/html", "utf-8");

        }
        else if(content.equals("aboutus")) {
            //view.loadData(getString(R.string.aboutus), "text/html", "utf-8");
            view.loadUrl("file:///android_asset/about-us.html");
        }
        else if(content.equals("terms")) {

            view.getSettings().setJavaScriptEnabled(true);
            view.loadUrl("file:///android_asset/terms-and-conditions.html");


        }
        else if(content.equals("privacypolicy")) {

            view.getSettings().setJavaScriptEnabled(true);
            view.loadUrl("file:///android_asset/privacy-policy.html");


        }


        return v;
    }

    }
