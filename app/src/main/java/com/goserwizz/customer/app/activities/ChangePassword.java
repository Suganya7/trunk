package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/15/2016.
 */
public class ChangePassword extends AppCompatActivity{
    private Toolbar toolbar;
    EditText currentPassword, newPassword, confirmPassword;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";

    private static String url2 = "http://goserwizz.com/custLogin.do";
    private static final String TAG_SESSION = "session";
    // JSON Node names
    private static final String TAG_PWD = "c_pwd";
    private static final String TAG_NEW_PWD = "n_pwd";
    private static final String TAG_CONFIRM_PWD = "confirm_pwd";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    String status, resultmsg, loginsession, logoutsession;
    String current_pwd, new_pwd, confirm_pwd, gu_id, pwd;
    Button confirmpwd_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);

        SettingsUtils.init(this);
        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");

        toolbar = (Toolbar)findViewById(R.id.Passwordtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");

        currentPassword = (EditText)findViewById(R.id.currentPassword);
        newPassword = (EditText)findViewById(R.id.newPassword);
        confirmPassword = (EditText)findViewById(R.id.confirmPwd);
        confirmpwd_btn = (Button)findViewById(R.id.pwd_confirm_btn);

        currentPassword.setTypeface( Typeface.DEFAULT );
        newPassword.setTypeface( Typeface.DEFAULT );
        confirmPassword.setTypeface( Typeface.DEFAULT );

       // currentPassword.setText(pwd);

        confirmpwd_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChangePwd();
            }
        });



    }

    public void ChangePwd() {
        current_pwd = currentPassword.getText().toString().trim();
        new_pwd = newPassword.getText().toString().trim();
     //   confirm_pwd = confirmPassword.getText().toString().trim();

        if (!validateCtPwd()) {
            return;
        }

        if (!validateNewPwd()) {
            return;
        }

        if(!validateConfirmPwd()) {
            return;
        }

        if(!validateNewAndConfirmPwd()) {
            return;
        }


        if(Connectivity.isConnected(this)) {

            new JsonGetChangePwd().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }

    }

    private boolean validateCtPwd() {
        if (currentPassword.getText().toString().trim().isEmpty()) {
            currentPassword.setError(getString(R.string.err_ctpwd));
            requestFocus(currentPassword);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateNewPwd() {
        if (newPassword.getText().toString().trim().isEmpty()) {
            newPassword.setError(getString(R.string.err_newpwd));
            requestFocus(newPassword);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateConfirmPwd() {
        if (confirmPassword.getText().toString().trim().isEmpty()) {
            confirmPassword.setError(getString(R.string.err_confirmpwd));
            requestFocus(confirmPassword);
            return false;
        } else {


            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateNewAndConfirmPwd() {

        String conpwd = confirmPassword.getText().toString().trim();
        String newpwd = newPassword.getText().toString().trim();

        if(!conpwd.equals(newpwd)) {
            confirmPassword.setError(getString(R.string.err_confirmpwdm));
            requestFocus(confirmPassword);
            return false;
        }
        else {
            //Toast.makeText(getApplicationContext(), "pwd matched", Toast.LENGTH_LONG).show();
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetChangePwd extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, current_pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_NEW_PWD, new_pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "changepwd"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            resultmsg = jObj.getString(TAG_MSG);

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
             if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(ChangePassword.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetChangePwd().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {
                        showResultDialog(resultmsg);
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetChangePwd().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    } else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                       // finish();

                            if(Connectivity.isConnected(ChangePassword.this)) {

                                new JSONSignOutData().execute();
                            }
                            else {
                                Connectivity.showNoConnectionDialog(ChangePassword.this);
                            }


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JSONSignOutData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setMessage("Logged Out...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            loginsession = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LOGINSESSION, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SESSION, loginsession));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signout"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");
                        logoutsession = object.getString(TAG_SESSION);

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGOUTSESSION, logoutsession);
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GU_ID, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PWD, "");
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new JSONSignOutData().execute();
                }
                else {

                 //   showResultDialog1(resultmsg);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, false);
                    startActivity(new Intent(ChangePassword.this, MainActivity.class));
                }

            }
            else if(result.equals("fail")) {
                if(result.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new JSONSignOutData().execute();
                }
                else { showResultDialogFail1(resultmsg); }
            }
        }
    }

    private void showResultDialog1(final String result) {
        if (this != null && !this.isFinishing()) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.dialog_register_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();

                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, false);

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialogFail1(final String result) {
        if (this != null && !this.isFinishing()) {
            new AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

}
