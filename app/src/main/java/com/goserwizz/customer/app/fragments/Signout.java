package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;
import com.goserwizz.customer.app.activities.MainActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaptas on 4/26/2016.
 */
public class Signout extends Fragment {

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custLogin.do";
    // JSON Node names

    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_SESSION = "session";
    private static final String TAG_GUID = "gu_id";

    String status, loginsession, logoutsession, resultmsg, gu_id, emailId;
    TextView signOut, signedInEmailId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.settings, container, false);

        SettingsUtils.init(getActivity());
        emailId = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

        signedInEmailId = (TextView)v.findViewById(R.id.signedInEmailId);
        signedInEmailId.setText(emailId);

        signOut = (TextView)v.findViewById(R.id.signOut);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Connectivity.isConnected(getActivity())) {

                    new JSONSignOutData().execute();

                }
                else {
                    Connectivity.showNoConnectionDialog(getActivity());
                }

            }
        });



        return v;

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JSONSignOutData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            loginsession = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LOGINSESSION, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SESSION, loginsession));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signout"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");
                        logoutsession = object.getString(TAG_SESSION);

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGOUTSESSION, logoutsession);
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GU_ID, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PWD, "");
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure")) {
                    new JSONSignOutData().execute();
                }
                else { showResultDialog(resultmsg); }

            }
            else if(result.equals("fail")) {
                if(resultmsg.contains("Communications link failure")) {
                    new JSONSignOutData().execute();
                }
                else { showResultDialogFail(resultmsg); }
            }
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_register_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, false);
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GU_ID, "");

                            SettingsUtils.getInstance().clear();

                            Intent newIntent = new Intent(getActivity(), MainActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);

                            getActivity().finish();

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }
}
