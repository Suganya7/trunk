package com.goserwizz.customer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.goserwizz.customer.app.R;

/**
 * Created by Kaptas on 5/4/2016.
 */
public class faqBooking extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_main, container, false);

        WebView view = new WebView(getActivity());
        view.setVerticalScrollBarEnabled(false);

        ((LinearLayout)v.findViewById(R.id.ContentLayout)).addView(view);

        view.loadData(getString(R.string.booking), "text/html", "utf-8");


        return v;
    }
}
