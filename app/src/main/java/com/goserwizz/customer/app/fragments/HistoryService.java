package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.goserwizz.customer.app.Adapters.CustomListAdapter;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;
import com.goserwizz.customer.app.activities.TrackVehicle;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/14/2016.
 */
public class HistoryService extends Fragment {

    ListView listview;
    public static String[] Datetime ={};
    public static String[] RegNum ={};
    public static String[] ModelNum ={};
    public static String[] serviceDetails ={};
    public static String[] serviceID ={};
    public static String[] Mileage ={};
    public static String[] serviceCenter ={};
    public static String[] statusList ={};

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";

    String status, resultmsg, gu_id, emptyData, bookstatus;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
    dealerState, dealerEmail, serviceId, addr, email, mileage, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
    paystatus, slotDate, dealerPerson, dealerMob;
    ArrayList<String> VehicleRegnoList, dateTimeList, VehicleModelList, StatusList, ServiceDetailList, ServiceIDList, MileageList, ServiceCenterList;
    CustomListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history_services,container,false);

        SettingsUtils.init(getActivity());
        listview=(ListView)v.findViewById(R.id.HistoryListView);

        VehicleRegnoList = new ArrayList<String>();
        dateTimeList = new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        ServiceDetailList = new ArrayList<String>();
        ServiceIDList = new ArrayList<String>();
        MileageList = new ArrayList<String>();
        ServiceCenterList = new ArrayList<String>();
        StatusList = new ArrayList<String>();


       // adapter=new CustomListAdapter(getActivity(), Datetime, ModelNum, RegNum, serviceDetails, serviceID, Mileage,serviceCenter);
       // listview.setAdapter(adapter);

        String history_tab = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_HISTORY_TAB, "");

      /*  if(history_tab.equals("ALL SERVICE")) {

            if(Connectivity.isConnected(getActivity())) {

                new JsonGetHistoryService().execute();

            }
            else {
                Connectivity.showNoConnectionDialog(getActivity());
            }
        }  */

        if(Connectivity.isConnected(getActivity())) {

            new JsonGetHistoryService().execute();

        }
        else {
            Connectivity.showNoConnectionDialog(getActivity());
        }

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Slecteditem= serviceID[position];
                String status= statusList[position];

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_S_ID_TRACK, Slecteditem);

                if(status.equals("No Show")) {
                    Toast.makeText(getActivity(), "No Show", Toast.LENGTH_SHORT).show();
                }
                else {

                    //   Toast.makeText(getActivity(), "sid: "+Slecteditem, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), TrackVehicle.class));
                }


            }
        });


        return v;


    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetHistoryService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            VehicleRegnoList.clear();
            dateTimeList.clear();
            VehicleModelList.clear();
            ServiceDetailList.clear();
            ServiceIDList.clear();
            MileageList.clear();
            ServiceCenterList.clear();
            StatusList.clear();

            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "100"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getbookhistory"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                emptyData = jObj.get("data").toString();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    serviceType = tmpObj.get("serviceType").toString();
                                    dealerCountry = tmpObj.get("dealerCountry").toString();
                                    mob = tmpObj.get("mob").toString();
                                    dealerCity = tmpObj.get("dealerCity").toString();
                                    cmts = tmpObj.get("cmts").toString();
                                    dealerAddr = tmpObj.get("dealerAddr").toString();
                                    advance = tmpObj.get("advance").toString();
                                    dealerLocation = tmpObj.get("dealerLocation").toString();
                                    slotTime = tmpObj.get("slotTime").toString();
                                    paymode = tmpObj.get("paymode").toString();
                                    dealerCompName = tmpObj.get("dealerCompName").toString();
                                    dealerState = tmpObj.get("dealerState").toString();
                                    dealerEmail = tmpObj.get("dealerEmail").toString();
                                    serviceId = tmpObj.get("serviceId").toString();
                                    addr = tmpObj.get("addr").toString();
                                    email = tmpObj.get("email").toString();
                                    mileage = tmpObj.get("mileage").toString();
                                    dealerPincode = tmpObj.get("dealerPincode").toString();
                                    pickup = tmpObj.get("pickup").toString();
                                    vehicleNo = tmpObj.get("vehicleNo").toString();
                                    modelName = tmpObj.get("modelName").toString();
                                    bookedDate = tmpObj.get("bookedDate").toString();
                                    name = tmpObj.get("name").toString();
                                    paystatus = tmpObj.get("paystatus").toString();
                                    slotDate = tmpObj.get("slotDate").toString();
                                    dealerPerson = tmpObj.get("dealerPerson").toString();
                                    dealerMob = tmpObj.get("dealerMob").toString();
                                    bookstatus = tmpObj.get("status").toString();


//Check greater and ct date and then display resch slot lists
                                    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                                    Calendar cal2 = Calendar.getInstance();
                                    String currentdate = format2.format(cal2.getTime());
                                    Log.d("current DATE:", slotDate);   // Date comp Not needed, no show is added in api

                                    VehicleRegnoList.add(vehicleNo);
                                    dateTimeList.add(slotDate + "/" + slotTime);
                                    VehicleModelList.add(modelName);
                                    ServiceDetailList.add(serviceType);
                                    ServiceIDList.add(serviceId);
                                    MileageList.add(mileage);
                                    ServiceCenterList.add(dealerCompName);
                                    StatusList.add(bookstatus);

                                 /*   if (bookstatus.equals("Booked") && slotDate.compareTo(currentdate) < 0) {
                                        System.out.println("smaller than ctdate");

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        ServiceDetailList.add(serviceType);
                                        ServiceIDList.add(serviceId);
                                        MileageList.add(mileage);
                                        ServiceCenterList.add(dealerCompName);
                                        StatusList.add("No Show");
                                    }
                                    else {
                                        System.out.println("greater than ctdate");

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        ServiceDetailList.add(serviceType);
                                        ServiceIDList.add(serviceId);
                                        MileageList.add(mileage);
                                        ServiceCenterList.add(dealerCompName);
                                        StatusList.add(bookstatus);
                                    }  */
//Check greater and ct date and then display resch slot lists according to Booked status

                                    /*    if(!(bookstatus.equals("Cancelled"))) {

                                    VehicleRegnoList.add(vehicleNo);
                                    dateTimeList.add(slotDate + "/" + slotTime);
                                    VehicleModelList.add(modelName);
                                    ServiceDetailList.add(serviceType);
                                    ServiceIDList.add(serviceId);
                                    MileageList.add(mileage);
                                    ServiceCenterList.add(dealerCompName);
                                    StatusList.add(bookstatus);  */


                                    System.out.println(tmpObj.get("serviceId"));
                                    System.out.println(tmpObj.get("vehicleNo") + "   bookstatus:  " + tmpObj.get("status"));
                            //    }
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog

            /**
             * Updating parsed JSON data into ListView
             * */

          /*  VehicleRegnoList.add(vehicleNo);
            dateTimeList.add(slotDate + "/" + slotTime);
            VehicleModelList.add(modelName);
            ServiceDetailList.add(serviceType);
            ServiceIDList.add(serviceId);
            MileageList.add(mileage);
            ServiceCenterList.add(dealerCompName);  */

            RegNum = new String[VehicleRegnoList.size()];
            RegNum = VehicleRegnoList.toArray(RegNum);

            Datetime = new String[dateTimeList.size()];
            Datetime = dateTimeList.toArray(Datetime);

            ModelNum = new String[VehicleModelList.size()];
            ModelNum = VehicleModelList.toArray(ModelNum);

            serviceDetails = new String[ServiceDetailList.size()];
            serviceDetails = ServiceDetailList.toArray(serviceDetails);

            serviceID = new String[ServiceIDList.size()];
            serviceID = ServiceIDList.toArray(serviceID);

            Mileage = new String[MileageList.size()];
            Mileage = MileageList.toArray(Mileage);

            serviceCenter = new String[ServiceCenterList.size()];
            serviceCenter = ServiceCenterList.toArray(serviceCenter);

            statusList = new String[StatusList.size()];
            statusList = StatusList.toArray(statusList);

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetHistoryService().execute();
                    }
                    else {

                        if (pDialog.isShowing())
                            pDialog.dismiss();

                        int length = serviceID.length;
                        if(! (length==0)) {   //!emptyData.equals("{}"
                            adapter = new CustomListAdapter(getActivity(), Datetime, ModelNum, RegNum, serviceDetails, serviceID, Mileage, serviceCenter);
                            listview.setAdapter(adapter);
                        }
                        else {
                            showResultDialog("You have not yet booked a service");
                        }
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetHistoryService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                         //   startActivity(new Intent(getActivity(), HomeActivity.class));

                            Intent newIntent = new Intent(getActivity(), HomeActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                         //   startActivity(new Intent(getActivity(), HomeActivity.class));

                            Intent newIntent = new Intent(getActivity(), HomeActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);

                        }
                    })
                    .show();
        }
    }

}
