package com.goserwizz.customer.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 4/23/2016.
 */
public class CancelServiceDetail extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener{

    private Toolbar toolbar;
    RadioGroup reasons_radiogroup;
    RadioButton service_radioBtn;
    EditText cancel_Comments;
    CheckBox cancelCheck;
    Button proceedCancel;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_S_ID = "s_id";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_REASONS = "reason";
    private static final String TAG_GUID = "gu_id";

    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";

    String status, resultmsg, dealer_id, gu_id, service_id;
    String reasonsText, Comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_services);

        toolbar = (Toolbar)findViewById(R.id.Cancel_service_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cancel Services");

        reasons_radiogroup = (RadioGroup)findViewById(R.id.cancelservice_RadioGroup);
        cancel_Comments = (EditText)findViewById(R.id.cancel_Comments);
        cancelCheck = (CheckBox)findViewById(R.id.cancelCheckbox);
        proceedCancel = (Button)findViewById(R.id.proceedCancel);

       // LinearLayout cancelParent = (LinearLayout)findViewById(R.id.cancelParent);
      //  setupUI(cancelParent);

        reasons_radiogroup.setOnCheckedChangeListener(this);
        proceedCancel.setOnClickListener(this);

        if(cancelCheck.isChecked()) {

            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_CHECKBOX, "Y");

        }
        else {
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_CHECKBOX, "N");
        }




    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        int selectedId = reasons_radiogroup.getCheckedRadioButtonId();
        service_radioBtn = (RadioButton)group.findViewById(selectedId);

        String reason = service_radioBtn.getText().toString();

        if(reason.equals("Other reasons")) {

            cancel_Comments.setVisibility(View.VISIBLE);

        }
        else{
            cancel_Comments.setVisibility(View.GONE);
        }


      //  Toast.makeText(CancelServiceDetail.this, service_radioBtn.getText().toString(), Toast.LENGTH_SHORT).show();
    }

    public void getCancelDetail() {

        int selectedId = reasons_radiogroup.getCheckedRadioButtonId();
        service_radioBtn = (RadioButton) findViewById(selectedId);

        String reason = service_radioBtn.getText().toString();
        if(reason.equals("Other reasons")) {
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_REASON, reason);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_COMMENTS, cancel_Comments.getText().toString());

            if (!validateCancelComments()) {
                return;
            }
        }
        else {
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_REASON, reason);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_COMMENTS, "");

        }


        if(cancelCheck.isChecked() && service_radioBtn.isChecked()) {

          //  Toast.makeText(CancelServiceDetail.this, "checked both", Toast.LENGTH_SHORT).show();

            if(Connectivity.isConnected(this)) {

                new JsonGetCancelService().execute();
            }
            else {
                Connectivity.showNoConnectionDialog(this);
            }

        }
        else {
            showResultDialog("Select are you sure want to cancel");
        }

    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(CancelServiceDetail.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private boolean validateCancelComments() {
        if (cancel_Comments.getText().toString().trim().isEmpty()) {
            cancel_Comments.setError(getString(R.string.err_reasons));
            requestFocus(cancel_Comments);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceedCancel:

                getCancelDetail();

                break;
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetCancelService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(CancelServiceDetail.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            service_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CANCEL_SERVICEID, "");
            reasonsText = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CANCEL_REASON, "");
            Comments = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CANCEL_COMMENTS, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_S_ID, service_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_REASONS, reasonsText));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, Comments));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "cancelbooking"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(CancelServiceDetail.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetCancelService().execute();
                    }
                    else {

                        showResultDialogFail(resultmsg);

                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetCancelService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            startActivity(new Intent(CancelServiceDetail.this, HomeActivity.class));
                            finish();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


}
