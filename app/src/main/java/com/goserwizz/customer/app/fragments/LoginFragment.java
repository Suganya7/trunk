package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.BaseLoginTest;
import com.goserwizz.customer.app.activities.ForgetPassword;
import com.goserwizz.customer.app.activities.HomeActivity;
import com.goserwizz.customer.app.activities.PrivacyPolicy;
import com.goserwizz.customer.app.activities.TermsCondition;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class LoginFragment extends BaseLoginTest implements BaseLoginTest.ConnectionTestListener{

    public LoginFragment() {
        // Required empty public constructor
    }

    EditText email, password;
    LinearLayout facebook, signin;

  /*  private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;  */
    TextView loginfacebook, forgetPwd;

    private ProgressDialog pDialog;
    // URL to get contacts JSON  3vikram.in
    private static String url = "http://goserwizz.com/custLogin.do?";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_SESSION = "session";
    private static final String TAG_FNAME = "fname";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> userList;
    String Email, Pwd, gu_id, session, digest, fname, status, resultmsg;

   /* @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();


    }

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
          //
          //  Toast.makeText(getActivity(), profile.getFirstName().toString(), Toast.LENGTH_LONG).show();

            submitForm();
          /*  if(accessToken != null){
                LoginManager.getInstance().logOut();
            }  //


        }

        @Override
        public void onCancel() {
            Toast.makeText(getActivity(), "Login cancelled by user!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException e) {
            Toast.makeText(getActivity(), "Login unsuccessful!", Toast.LENGTH_LONG).show();
        }
    };

*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        SettingsUtils.init(getActivity());
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_FNAME, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_LNAME, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_MOBILE, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGEMAIL, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGPWD, "");
      /*  facebook = (LinearLayout) view.findViewById(R.id.loginfacebookLayout);
        loginfacebook = (TextView) view.findViewById(R.id.ORtext);  */
        forgetPwd = (TextView) view.findViewById(R.id.forgetPwd);
        email = (EditText) view.findViewById(R.id.login_email);
        password = (EditText) view.findViewById(R.id.login_pwd);
        signin = (LinearLayout) view.findViewById(R.id.SignInLayout);

        password.setTypeface(Typeface.DEFAULT);

      //  email.addTextChangedListener(new MyTextWatcher(email));
      //  password.addTextChangedListener(new MyTextWatcher(password));

       /* String inputemail = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_EMAIL, "");
        String inputpwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_PWD, "");

        if(!inputemail.isEmpty()) {
            email.setText(inputemail);

        }
        if(!inputpwd.isEmpty()) {
            password.setText(inputpwd);

        }
        */

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        userList = new ArrayList<HashMap<String, String>>();

        /*  Login with facebook functions will start here

        //Register a callback
        callbackManager = CallbackManager.Factory.create();

        final LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);

        loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login with facebook on clicking custom button

                loginButton.performClick();

                //  LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends"));
            }
        });

        */

        forgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


             /*   if (!validateEmail()) {
                    return;
                }

                /*
                if (!validatePassword()) {
                    return;
                }

                Pwd = password.getText().toString();

                if(Connectivity.isConnected(getActivity())) {

                    new GetForgetPwd().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(getActivity());
                }
                */

                Email = email.getText().toString();
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_EMAIL, Email);

                startActivity(new Intent(getActivity(), ForgetPassword.class));

                email.setText("");



            }
        });


/*
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // login successful

                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile profile = Profile.getCurrentProfile();
                        displayMessage(profile);

                        System.out.println("Facebook Login Successful!");
                        System.out.println("Logged in user Details : ");
                        System.out.println("--------------------------");
                        System.out.println("User ID  : " + loginResult.getAccessToken().getUserId());
                        System.out.println("Authentication Token : " + loginResult.getAccessToken().getToken());
                        Toast.makeText(getActivity(), "Login Successful!", Toast.LENGTH_LONG).show();


                        Log.d("loginsuccess====>", "yesssssss");
                    }

                    @Override
                    public void onCancel() {
                        // login cancelled
                        Toast.makeText(getActivity(), "Login cancelled by user!", Toast.LENGTH_LONG).show();
                        System.out.println("Facebook Login failed!!");

                        Log.d("loginsuccess====>", "Nooooo");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // login error
                        Toast.makeText(getActivity(), "Login unsuccessful!", Toast.LENGTH_LONG).show();
                        System.out.println("Facebook Login failed!!");

                    }
                });



        /*  Login with facebook functions ends */

        /* Spannable functions for underline text which clicks to open another activity  */
        TextView terms = (TextView) view.findViewById(R.id.agreeText1);
        TextView condition = (TextView) view.findViewById(R.id.agreeText2);
        TextView privacypolicy = (TextView) view.findViewById(R.id.agreeText4);

        terms.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans = (Spannable) terms.getText();
        ClickableSpan clickSpan = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
               startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans.setSpan(clickSpan, 0, spans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//
        condition.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans1 = (Spannable) condition.getText();
        ClickableSpan clickSpan1 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans1.setSpan(clickSpan1, 0, spans1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
        privacypolicy.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans2 = (Spannable) privacypolicy.getText();
        ClickableSpan clickSpan2 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), PrivacyPolicy.class));
            }
        };
        spans2.setSpan(clickSpan2, 0, spans2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

 /* Spannable functions for underline text functions finist */

        return view;
    }


 /*   @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void displayMessage(Profile profile){
        if(profile != null){
          //  loginfacebook.setText(profile.getName());
            Toast.makeText(getActivity(), "Logged in as " + profile.getName().toString(), Toast.LENGTH_LONG).show();
            Log.d("loginsuccess====>" ,profile.getName().toString());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    */

    /**
     * Validating form
     */
    private void submitForm() {

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        SettingsUtils.init(getActivity());

        testConnection(false, this);

    }

    private boolean validateEmail() {
        String emailreg = email.getText().toString().trim();

        if (emailreg.isEmpty() || !isValidEmail(emailreg)) {
            email.setError(getString(R.string.err_msg_email));
            requestFocus(email);
            return false;
        } else {
            //email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            password.setError(getString(R.string.err_msg_password));
            requestFocus(password);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.login_email:
                    validateEmail();

                    String input_email = email.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, input_email);

                    break;
                case R.id.login_pwd:
                    validatePassword();

                    String input_pwd = password.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, input_pwd);

                    break;
            }
        }
    }


    public void onBackPressed() {
       /* Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
       */
        Log.d("Login Fragment ===>", "user pressed the back button");

        getActivity().moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);


    }

    @Override
    protected void testConnection(boolean isShared, ConnectionTestListener listener) {
        sEmail = email.getText().toString();
        sPwd = password.getText().toString();
        sMode = "signin";

        super.testConnection(isShared, listener);
    }

    @Override
    public void onConnectionChecked(String result) {
        if (!TextUtils.isEmpty(result) && result.contains("Successfully login")) {

            resultmsg = "Successfully login into goserwizz.com";

            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, sEmail);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PWD, sPwd);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, true);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MODE, 1);

            String value = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
            String name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");

            Toast.makeText(getActivity(), resultmsg , Toast.LENGTH_LONG).show();
         //   Toast.makeText(getActivity(), resultmsg + " " + value + name, Toast.LENGTH_LONG).show();

            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, "");
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, "");

            Intent i = new Intent(getActivity(), HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
        else if (!TextUtils.isEmpty(result) && result.contains("fail")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully"))    {
                    resultmsg = "Try again";
                }
                 else {
                    resultmsg = result;
                }

            }

    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetLoginData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, Pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signin"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");
                        gu_id = object.getString(TAG_GUID);
                        session = object.getString(TAG_SESSION);
                        fname = object.getString(TAG_FNAME);
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {
                Toast.makeText(getActivity(), " login => " + fname + resultmsg, Toast.LENGTH_LONG).show();
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }
            else if(result.equals("fail")) {
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetForgetPwd extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "forgetpwd"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");

                        digest = object.getString("digest");

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DIGEST, digest);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

            if(result.equals("success")) {
              //  Toast.makeText(getActivity(), " forgetpwd => " +  resultmsg, Toast.LENGTH_LONG).show();

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new GetForgetPwd().execute();
                }
                else {
                    new GetForgetPwdVerfify().execute();
                }

            }
            else if(result.equals("fail")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new GetForgetPwd().execute();
                } else {
                    showResultDialogFail(resultmsg); }

            }
            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetForgetPwdVerfify extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Reset Verfification...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            digest = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DIGEST, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DIGEST, digest));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, Pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "forgetpwdverify"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new GetForgetPwdVerfify().execute();
                } else {
                    showResultDialogN(resultmsg);
                }


            }
            else if(result.equals("fail")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new GetForgetPwdVerfify().execute();
                }
                else { showResultDialogFail(resultmsg); }
            }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogN(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }

}
