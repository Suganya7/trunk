package com.goserwizz.customer.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/15/2016.
 */
public class Feedback extends Fragment implements View.OnClickListener {

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_SUBJECT = "sub";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_NAME = "name";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    String status, resultmsg;
    String sub="", msg_to, msg, gu_id, email, name;
    Spinner feedbackSpinner;
    TextView fbEmail;
    EditText typeFeedbck;
    Button feedbackBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.feedback, container, false);

        SettingsUtils.init(getActivity());

        fbEmail = (TextView)v.findViewById(R.id.fbEmail);
        typeFeedbck = (EditText)v.findViewById(R.id.typeFeedbck);
        feedbackSpinner = (Spinner)v.findViewById(R.id.feedbackSpinner);
        feedbackBtn = (Button)v.findViewById(R.id.feedbackBtn);
        feedbackBtn.setOnClickListener(this);

        LinearLayout feedbackparent = (LinearLayout)v.findViewById(R.id.feedbackParent);
        setupUI(feedbackparent);

        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
        name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        fbEmail.setText(email);

        typeFeedbck.addTextChangedListener(new MyTextWatcher(typeFeedbck));

        feedbackSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = parent.getItemAtPosition(pos).toString();
                //  System.out.println("it works...   " + item);

                if (item.equals("Select Category")) {
                    System.out.println("it works...   " + item);
                } else {
                    System.out.println("it works...   " + item);

                    sub = item;

                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        return v;

    }

    private void submitFeedback() {
        if (!validateFeedback()) {
            return;
        }

        if(!sub.equals("")) {

            if (Connectivity.isConnected(getActivity())) {

                new JsonFeedbackDetail().execute();
            } else {
                Connectivity.showNoConnectionDialog(getActivity());
            }
        }
        else  {
            showResultDialogFail("Please Select Category");
        }

       }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private boolean validateFeedback() {
        if (typeFeedbck.getText().toString().trim().isEmpty()) {
            typeFeedbck.setError(getString(R.string.err_feedbck));
            requestFocus(typeFeedbck);
            return false;
        } else {
            // typeFeedbck.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.typeFeedbck:
                    validateFeedback();
                    break;

            }
        }
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {

            case R.id.feedbackBtn:

                msg_to = fbEmail.getText().toString();
                msg = typeFeedbck.getText().toString();
                submitFeedback();

                break;

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonFeedbackDetail extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
            name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, email));
            nameValuePairs.add(new BasicNameValuePair(TAG_NAME, name));
            nameValuePairs.add(new BasicNameValuePair(TAG_MSG, msg));
            nameValuePairs.add(new BasicNameValuePair(TAG_SUBJECT, sub));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "feedback"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                        System.out.println(resultmsg);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonFeedbackDetail().execute();

                    }
                    else {
                        showResultDialog(resultmsg);
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonFeedbackDetail().execute();


                    } else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


}
