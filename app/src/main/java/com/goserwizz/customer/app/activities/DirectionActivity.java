package com.goserwizz.customer.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.goserwizz.customer.app.ImageLoader.FileCache;
import com.goserwizz.customer.app.ImageLoader.MemoryCache;
import com.goserwizz.customer.app.ImageLoader.Utils;
import com.goserwizz.customer.app.MapJSONParser.DirectionsJSONParser;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kaptas on 4/23/2016.
 */
public class DirectionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private GoogleMap mMap;
    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mGoogleMap;

    ArrayList<LatLng> mMarkerPoints;
    double mLatitude=0;
    double mLongitude=0;
    LocationManager locationManager;
    Location location;
    LocationListener locationListener;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    Button checkAvailable;

    private static final int PERMISSION_REQUEST_CODE = 1;
    private View view;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;
    String d_lat, d_long, dealer_name;
    double dest_lat, dest_long;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.direction_activity);

        context = this;

        toolbar = (Toolbar)findViewById(R.id.directiontoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Directions");

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }

        checkAvailable = (Button)findViewById(R.id.checkavail_dxn);

        checkAvailable.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                startActivity(new Intent(DirectionActivity.this, ServiceConfirm.class));
            }
        });

        /***Needed for Marshmallow to check permission while at runtime
         ****
         ****/
        checkGPSStatus();


        if (!checkPermission()) {


         //   requestPermission();

            mJumpRunnable = new Runnable() {
                public void run() {

                    getRouteDirection();
                }
            };
            mHandler = new Handler();
            mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);


        } else {

            getRouteDirection();

        }

    }

    public void getRouteDirection() {

        d_lat = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_LAT, "");
        d_long = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_LONG, "");
        dealer_name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
        dest_lat = Double.parseDouble(d_lat);
        dest_long = Double.parseDouble(d_long);

        // Map view methods started here
        mSupportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    if (googleMap != null) {

                        if (Build.VERSION.SDK_INT >= 23 &&
                                ContextCompat.checkSelfPermission(DirectionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(DirectionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(DirectionActivity.this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
                        }

                        locationListener = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {

                                mLatitude = location.getLatitude();
                                mLongitude = location.getLongitude();
                                LatLng point = new LatLng(mLatitude, mLongitude);

                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {

                            }

                            @Override
                            public void onProviderEnabled(String provider) {

                            }

                            @Override
                            public void onProviderDisabled(String provider) {

                            }
                        };



                        // Getting Google Play availability status
                        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

                        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

                            int requestCode = 10;
                            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, DirectionActivity.this, requestCode);
                            dialog.show();

                        } else { // Google Play Services are available
                            mGoogleMap = googleMap;
                            // Initializing
                            mMarkerPoints = new ArrayList<LatLng>();
                            googleMap.getUiSettings().setAllGesturesEnabled(true);
                            // Enable MyLocation Button in the Map
                            googleMap.setMyLocationEnabled(true);
                            // Getting LocationManager object from System Service LOCATION_SERVICE
                            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                            // Creating a criteria object to retrieve provider
                            Criteria criteria = new Criteria();
                            // Getting the name of the best provider
                            String provider = locationManager.getBestProvider(criteria, true);
                            // Getting Current Location From GPS

                            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);


                            if (location != null) {
                                onLocationChanged(location);
                            }

                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                            // Current Loctiaon of the marker
                            //    FragmentManager fm2 = getActivity().getSupportFragmentManager(); // getSupportFragmentManager
                            mMarkerPoints.clear();
                            googleMap.clear();
                            LatLng startPoint = new LatLng(mLatitude, mLongitude);
                            drawMarker(startPoint, googleMap);
                            LatLng destPoint = new LatLng(dest_lat, dest_long);
                            drawMarker(destPoint, googleMap);

                            // Getting URL to the Google Directions API
                            String url = getDirectionsUrl(startPoint, destPoint);
                            DownloadTask downloadTask = new DownloadTask();
                            // Start downloading json data from Google Directions API
                            downloadTask.execute(url);
                        }
                    }

                }
            });


        }
    }



    public static Bitmap getBitmap(String url,Context context)
    {
        FileCache fileCache=new FileCache(context);
        MemoryCache memoryCache=new MemoryCache();
        File f=fileCache.getFile(url);
        //from SD cache
        //CHECK : if trying to decode file which not exist in cache return null
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        // Download image file from web
        try {
            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            // Constructs a new FileOutputStream that writes to file
            // if file not exist then it will create file
            OutputStream os = new FileOutputStream(f);
            // See Utils class CopyStream method
            // It will each pixel from input stream and
            // write pixels to output stream (file)
            Utils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            //Now file created and going to resize file with defined height
            // Decodes image and scales it to reduce memory consumption
            b = decodeFile(f);
            return bitmap;

        } catch (Throwable ex){
            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    //Decodes image and scales it to reduce memory consumption
    private static Bitmap decodeFile(File f){

        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();
            //Find the correct scale value. It should be the power of 2.
            // Set width/height of recreated image
            final int REQUIRED_SIZE=150;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with current scale values
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;

        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private String getDirectionsUrl(LatLng origin,LatLng dest){
        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpsURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpsURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb  = new StringBuffer();
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        }catch(Exception e){
            // Log.d("Exception while downloading url", "");
            Log.d("Exception ===>", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /** A class to download data from Google Directions URL */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /** A class to parse the Google Directions in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);
                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            mGoogleMap.addPolyline(lineOptions);
        }
    }


    private void drawMarker(LatLng point, GoogleMap googleMap){
        mMarkerPoints.add(point);
        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();
        // Setting the position of the marker
        options.position(point);

        String pinpoint_url = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_PINPOINT, "");

        /**
         * For the start location, the color of marker is GREEN and
         * for the end location, the color of marker is RED.
         */
        if(mMarkerPoints.size()==1){
          //  options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Current Location");


        }else if(mMarkerPoints.size()==2){
            //options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).title(dealer_name);

            options.icon(BitmapDescriptorFactory.fromBitmap(getBitmap(pinpoint_url, context))).title(dealer_name);
            googleMap.addMarker(options);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));

        }

        // Add new marker to the Google Map Android API V2
     /*   googleMap.addMarker(options);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));  */
    }


    public void onLocationChanged(Location location) {

        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng point = new LatLng(mLatitude, mLongitude);

        //   mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
        //   mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12));

    }

    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    /* check Runtime permission for access fine location */
    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)){

            Toast.makeText(this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Toast.makeText(DirectionActivity.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {

                            Toast.makeText(DirectionActivity.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }

    private void checkGPSStatus() {
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Enable GPS Location");
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        }
        else {

        }
    }


}
