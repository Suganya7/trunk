package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaptas on 4/26/2016.
 */
public class ForgetPassword extends AppCompatActivity {

    private Toolbar toolbar;
    EditText email, pwd;
    Button forgetPwdBtn;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custLogin.do?";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_SESSION = "session";
    private static final String TAG_FNAME = "fname";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";

    String Email, Pwd, gu_id, session, digest, fname, status, resultmsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetpassword);

        SettingsUtils.init(this);
        toolbar = (Toolbar)findViewById(R.id.forgetPwdtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forget Password");

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, "");
    //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, "");

        email = (EditText)findViewById(R.id.forgetpwd_email);
    //    pwd = (EditText)findViewById(R.id.forget_pwd);
        forgetPwdBtn = (Button)findViewById(R.id.forgetPwdBtn);

        Email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_USER_EMAIL, "");
     //   email.addTextChangedListener(new MyTextWatcher(email));

        if(Email.isEmpty()) {
            email.setText(Email);

        }
        else {
            email.setText(Email);

        }

        forgetPwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                forgetpwd();
            }
        });
    }

    public void forgetpwd() {


        if(!validateEmail()) {
            return;
        }

     /*   if (!validatePwd()) {
            return;
        }
*/
        Email = email.getText().toString();
      // Pwd = pwd.getText().toString();


        if(Connectivity.isConnected(this)) {

            new GetForgetPwd().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }


    }

    private boolean validateEmail() {
        String emailreg = email.getText().toString().trim();

        if (emailreg.isEmpty() || !isValidEmail(emailreg)) {
            email.setError(getString(R.string.err_msg_email));
            requestFocus(email);
            return false;
        } else {
            //email.setErrorEnabled(false);

        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validatePwd() {
        if (pwd.getText().toString().trim().isEmpty()) {
            pwd.setError(getString(R.string.err_msg_password));
            requestFocus(pwd);
            return false;
        } else {


            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetForgetPwd extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ForgetPassword.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "forgetpwd"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");

                        digest = object.getString("digest");

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DIGEST, digest);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(ForgetPassword.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {
                    //  Toast.makeText(getActivity(), " forgetpwd => " +  resultmsg, Toast.LENGTH_LONG).show();

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new GetForgetPwd().execute();
                    }
                    else {
                        //new GetForgetPwdVerfify().execute();

                        showResultDialog("Please check the mail to reset the password");
                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new GetForgetPwd().execute();
                    } else {
                        showResultDialogFail(resultmsg); }

                }
            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetForgetPwdVerfify extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ForgetPassword.this);
            pDialog.setMessage("Reset Verfification...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            digest = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DIGEST, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DIGEST, digest));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, Pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "forgetpwdverify"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(ForgetPassword.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new GetForgetPwdVerfify().execute();
                    } else {
                        showResultDialogN(resultmsg);
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new GetForgetPwdVerfify().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogN(final String result) {
        if (this != null && !this.isFinishing()) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.dialog_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            startActivity(new Intent(ForgetPassword.this, MainActivity.class));

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            startActivity(new Intent(ForgetPassword.this, MainActivity.class));
                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // NavUtils.navigateUpFromSameTask(this);
               // startActivity(new Intent(ForgetPassword.this, MainActivity.class));
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, "");

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, "");


    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.forgetpwd_email:
                    validateEmail();

                    break;

            }
        }
    }

}
