package com.goserwizz.customer.app.Util;


import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.goserwizz.customer.app.R;

import java.util.ArrayList;
import java.util.Arrays;


public class SettingsUtils {

    private static final String TAG = SettingsUtils.class.getSimpleName();

    public static final String API_BASE_URL = "base_url";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_PWD = "pwd";
    public static final String KEY_MODE = "mode";

    public static final String KEY_LOGINSESSION = "KEY_LOGINSESSION";
    public static final String KEY_LOGOUTSESSION = "KEY_LOGOUTSESSION";
    public static final String KEY_SESSION = "KEY_SESSION";
    public static final String KEY_DIGEST = "KEY_DIGEST";
    public static final String KEY_URL_BASE = "KEY_URL_BASE";
    public static final String KEY_URL = "KEY_URL";

    public static final String KEY_INPUT_EMAIL = "KEY_INPUT_EMAIL";
    public static final String KEY_INPUT_PWD = "KEY_INPUT_PWD";

    public static final String KEY_INPUT_REGEMAIL = "KEY_INPUT_REGEMAIL";
    public static final String KEY_INPUT_REGPWD = "KEY_INPUT_REGPWD";
    public static final String KEY_INPUT_FNAME = "KEY_INPUT_FNAME";
    public static final String KEY_INPUT_LNAME = "KEY_INPUT_LNAME";
    public static final String KEY_INPUT_MOBILE = "KEY_INPUT_MOBILE";
    public static final String KEY_STATUS = "KEY_STATUSs";

    public static final String KEY_BOOKSERVICEID = "service_idd";
    public static final String KEY_BOOKEDDATE = "KEY_BOOKEDDATE";

    public static final String KEY_CONTENT = "KEY_CONTENT";
    public static final String KEY_ONLINESLOT = "KEY_ONLINESLOT";

    public static final String KEY_GU_ID = "gu_id";
    public static final String KEY_FIRSTNAME = "fname";
    public static final String KEY_LASTNAME = "lname";
    public static final String KEY_MOBILE = "mob";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_STATE = "state";
    public static final String KEY_USER_CITY = "city";
    public static final String KEY_ADDRESS = "addr";
    public static final String KEY_DEALER_ADDR = "d_addr";
    public static final String KEY_DEALER_ID_LIST = "KEY_DEALER_ID_LIST";

    public static final String KEY_USER_EMAIL = "user_emailid";

    public static final String KEY_HISTORY_TAB = "history_tab";
    public static final String KEY_S_ID_TRACK = "s_id_track";
    public static final String KEY_FRAGMENT = "KEY_FRAGMENT";
    public static final String KEY_SERVICE_CATEGORY = "service_category";
    public static final String KEY_RESCH_REGNO = "regno";
    public static final String KEY_RESCH_SCENTER = "scenter";
    public static final String KEY_RESCH_BOOKSTATUS = "bookstatus";
    public static final String KEY_RESCH_SERVICEID = "serviceId";
    public static final String KEY_RESCH_STYPE = "sType";
    public static final String KEY_RESCH_SDATE = "sDate";
    public static final String KEY_RESCH_STIME = "sTime";
    public static final String KEY_RESCH_S_DATETIME = "s_datetime";
    public static final String KEY_RESCH_R_DATETIME = "r_datetime";
    public static final String KEY_CANCEL_SERVICEID = "cancel_sid";
    public static final String KEY_CANCEL_regNO = "cancel_regno";
    public static final String KEY_CANCEL_CHECKBOX = "cancel_checkbox";
    public static final String KEY_CANCEL_REASON = "cancelreasons";
    public static final String KEY_CANCEL_COMMENTS = "cancel_comments";

    public static final String KEY_VEHADD_REGNO = "regno";
    public static final String KEY_VEHADD_MILEAGE = "mileage";
    public static final String KEY_VEH_REGNO = "regno";
    public static final String KEY_VEH_MILEAGE = "mileage";
    public static final String KEY_VEH_MODEL = "veh_model";
    public static final String KEY_SERVICE_TYPE = "stype";
    public static final String KEY_PICKUP = "pickup";
    public static final String KEY_PICKUP_ADDRESS = "pickup_addr";
    public static final String KEY_PAYMODE = "paymode";
    public static final String KEY_SERVICE_AMOUNT = "amount";
    public static final String KEY_DEALER_CITY = "d_city";
    public static final String KEY_MODELID = "m_id";
    public static final String KEY_COMMENTS = "cmnts";
    public static final String KEY_DEALERID = "d_id";
    public static final String KEY_DATE = "s_date";
    public static final String KEY_TIME = "s_time";
    public static final String KEY_SELECTINDEX_TIME = "pos";
    public static final String KEY_BOOKEDSLOTS = "bookedslot";
    public static final String KEY_BOOKSLOT_NUM = "bookslot_num";
    public static final String KEY_BOOKSLOT_2 = "bookslot_2";
    public static final String KEY_BOOKSLOT_3 = "bookslot_3";
    public static final String KEY_BOOKSLOT_4 = "bookslot_4";
    public static final String KEY_BOOKSLOT_5 = "bookslot_5";
    public static final String KEY_SLOTAVAILABILITY = "select_slot_availability";

    public static final String KEY_SERVICE_ID = "s_id";

    public static final String KEY_CITY = "city";
    public static final String KEY_VTYPE = "vtype";
    public static final String KEY_VMAKEid = "vmake_id";
    public static final String KEY_VMODELid = "vmodel_id";
    public static final String KEY_VMAKE = "vmake";
    public static final String KEY_VMODEL = "vmodel";

    public static final String KEY_DEALER_NAME = "dealer_name";
    public static final String KEY_DEALER_LAT = "lat";
    public static final String KEY_DEALER_LONG = "long";
    public static final String KEY_DEALER_PINPOINT = "pin_point";


    public static final String KEY_CURRENT_DATA = "KEY_CURRENT_DATA";
    public static final String KEY_LOGOUT_TIME = "KEY_LOGOUT_TIME";
    public static final String KEY_FIRST_LAUNCH = "KEY_FIRST_LAUNCH";
    public static final String KEY_LOGGED_IN = "KEY_LOGGED_IN";

    private static SettingsUtils instance;

    private SharedPreferences prefs;

    public SettingsUtils(Context context) {
        this.prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    public static SettingsUtils getInstance() {
        if (instance == null) {
            throw new IllegalStateException("should use SettingsUtils.init(context) first");
//            Log.e(TAG, "should use SettingsUtils.init(context) first");
        }
        return instance;
    }

    public static void init(Context context) {
        instance = new SettingsUtils(context);
    }

    public void putValue(String key, String value) {

        prefs.edit().putString(key, value).apply();
    }

    public void putValue(String key, int value) {
        prefs.edit().putInt(key, value).apply();
    }

    public void putValue(String key, boolean value) {
        prefs.edit().putBoolean(key, value).apply();
    }

    public void putValue(String key, long value) {
        prefs.edit().putLong(key, value).apply();
    }

    public void putValue(String key, float value) {
        prefs.edit().putFloat(key, value).apply();
    }

    public String getValue(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    public int getValue(String key, int defaultValue) {
        return prefs.getInt(key, defaultValue);
    }

    public boolean getValue(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    public long getValue(String key, long defaultValue) {
        return prefs.getLong(key, defaultValue);
    }

    public float getValue(String key, float defaultValue) {
        return prefs.getFloat(key, defaultValue);
    }

    public boolean contains(String key) {
        return prefs.contains(key);
    }

    public void clear() {
        prefs.edit().clear().apply();
    }


    public void putServiceValues(String key,String value) {
            prefs.edit().putString(key, value).commit();
    }
    public String getServiceValue(String key)
    {
        Log.d("ID_INGet", prefs.getString(key, ""));
        return prefs.getString(key,"");
    }

    public void removeService(ArrayList<String> batchList)
    {

        for (String id:batchList)
        {
            Log.d("RemoveId",id);
            prefs.edit().putString(id,"").commit();
        }
    }

    /**
     * Put ArrayList of String into SharedPreferences with 'key' and save
     * @param key SharedPreferences key
     * @param stringList ArrayList of String to be added
     */
    public void putListString(String key, ArrayList<String> stringList) {
        checkForNullKey(key);
        String[] myStringList = stringList.toArray(new String[stringList.size()]);
        prefs.edit().putString(key, TextUtils.join("‚‗‚", myStringList)).apply();
    }
    /**
     * Get parsed ArrayList of String from SharedPreferences at 'key'
     * @param key SharedPreferences key
     * @return ArrayList of String
     */
    public ArrayList<String> getListString(String key) {
        return new ArrayList<String>(Arrays.asList(TextUtils.split(prefs.getString(key, ""), "‚‗‚")));
    }
    /**
     * null keys would corrupt the shared pref file and make them unreadable this is a preventive measure
     * @param //the pref key
     */
    public void checkForNullKey(String key){
        if (key == null){
            throw new NullPointerException();
        }
    }
    /**
     * null keys would corrupt the shared pref file and make them unreadable this is a preventive measure
     * @param //the pref key
     */
    public void checkForNullValue(String value){
        if (value == null){
            throw new NullPointerException();
        }
    }

    public void clearValue(String key, String value) {

        prefs.edit().remove(key).commit();

    }
}