package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.goserwizz.customer.app.Adapters.CustomAdapter;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.ServiceConfirm;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/11/2016.
 */
public class TimeFragment extends Fragment {

    ListView listView;
    ArrayList< String> timeList, getTimeslots, getOnlineslot, getBooketslot; // list of the strings that should appear in ListView
    ArrayAdapter arrayAdapter;
    String[] TimeList = {};
    String[] OnlineslotList = {};
    String[] BookedSlot = {};

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "s_date";
    private static final String TAG_STIME = "s_time";

    String status, resultmsg, dealer_id, s_date, s_time, r_date;
    String online_slot, slot_time, onlineslotavailable, bookedslot;
    String[] timeListArr;
    CustomAdapter customAdapter;
    Button saveTime;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.time_fragment, container, false);

        SettingsUtils.init(getActivity());
        timeList = new ArrayList<>();
        getTimeslots = new ArrayList<>();

        listView = (ListView)v.findViewById(R.id.timelist);

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        saveTime = (Button)v.findViewById(R.id.saveTime);

       /* s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        if(!s_date.equals("") && !(s_date.isEmpty())) {
            ((DateTime) getActivity()).showDate(s_date);
        }
        */
       // this.onResume();


        getTimeslots = SettingsUtils.getInstance().getListString("timelist");
        TimeList = new String[getTimeslots.size()];
        TimeList = getTimeslots.toArray(TimeList);

        getOnlineslot = SettingsUtils.getInstance().getListString("onlineslotlist");
        OnlineslotList = new String[getOnlineslot.size()];
        OnlineslotList = getOnlineslot.toArray(OnlineslotList);

        getBooketslot = SettingsUtils.getInstance().getListString("bookedslotlist");
        BookedSlot = new String[getBooketslot.size()];
        BookedSlot = getBooketslot.toArray(BookedSlot);

        customAdapter = new CustomAdapter (getActivity(), TimeList);
        listView.setAdapter(customAdapter);

        String time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
        String timepos = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SELECTINDEX_TIME, "");


        if(!timepos.isEmpty()) {
            customAdapter.setSelectedPosition(Integer.parseInt(timepos));
            customAdapter.notifyDataSetChanged();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                       long arg3) {

                customAdapter.setSelectedIndex(position);
                customAdapter.notifyDataSetChanged();


                s_time = (String) adapter.getItemAtPosition(position);

                onlineslotavailable = OnlineslotList[position];
                bookedslot = BookedSlot[position];
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_ONLINESLOT, onlineslotavailable);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKEDSLOTS, bookedslot);
                // set time on time tab
               // ((DateTime) getActivity()).showTime(s_time);

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, s_time);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SELECTINDEX_TIME, Integer.toString(position));

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, s_time);
                r_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_SDATE, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_R_DATETIME, r_date + " / " + s_time);



             //   Toast.makeText(getActivity(), s_time, Toast.LENGTH_LONG).show();

            }
        });

        saveTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String fragment = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FRAGMENT, "");
                if(fragment.equals("servicecenter")) {

                    Intent newIntent = new Intent(getActivity(), ServiceConfirm.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);

                 /*   mJumpRunnable = new Runnable() {
                        public void run() {


                            String bookedslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKEDSLOTS, "");
                            Intent newIntent = new Intent(getActivity(), ServiceConfirm.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            newIntent.putExtra("bookedslot", bookedslot);
                            startActivity(newIntent);
                        }
                    };
                    mHandler = new Handler();
                    mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);*/

                  //  startActivity(new Intent(getActivity(), ServiceConfirm.class));



                }else if(fragment.equals("reschedule")) {
                  //  startActivity(new Intent(getActivity(), RescheduleSlot.class));

                    Intent newIntent = new Intent(getActivity(), RescheduleSlot.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                    getActivity().finish();
                }

                getActivity().finish();

            }
        });
        return v;

    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    /**
     * Async task class to get json by making HTTP call
     *
    private class JsonGetSlotTime extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
         /*   pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
            s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
            timeList.clear();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, dealer_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, s_date));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getslottime"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    online_slot = tmpObj.get("online_slot").toString();
                                    slot_time = tmpObj.get("slot_time").toString();

                                    timeList.add(slot_time);

                                    System.out.println(tmpObj.get("online_slot"));
                                    System.out.println(tmpObj.get("slot_time"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
          /*  if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             *
            timeListArr = new String[timeList.size()];
            timeListArr = timeList.toArray(timeListArr);

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(result.contains("Communications link failure")) {
                        new JsonGetSlotTime().execute();
                    }
                    else {
                        customAdapter = new CustomAdapter (getActivity(), timeListArr);
                        listView.setAdapter(customAdapter);
                    }


                }
                else if(result.equals("fail")) {

                    if(result.contains("Communications link failure")) {
                     //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetSlotTime().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

*/


    @Override
    public void onResume()
    {
        super.onResume();
        //do the data changes. In this case,
        // I am refreshing the arrayList and then calling the listview to refresh.
        getTimeslots = SettingsUtils.getInstance().getListString("timelist");
        TimeList = new String[getTimeslots.size()];
        TimeList = getTimeslots.toArray(TimeList);

        customAdapter = new CustomAdapter (getActivity(), TimeList);
        listView.setAdapter(customAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

}
