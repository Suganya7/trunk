package com.goserwizz.customer.app.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.util.Timer;
import java.util.TimerTask;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class AutoScrollImageSlide extends AppCompatActivity {

    int[] mResources = {
            R.drawable.banner1,
            R.drawable.banner2,
            R.drawable.banner3,

    };
    AutoScrollViewPager mViewPager;
    Timer timer;
    int page = 0;
    PageIndicator mIndicator;
    Button StartBtn;
    LinearLayout btnLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_scroll_image_slide);


        SettingsUtils.init(AutoScrollImageSlide.this);

        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(this);

        btnLayout = (LinearLayout)findViewById(R.id.ButtonLayout);
        StartBtn = (Button)findViewById(R.id.GetStartBtn);
        mViewPager = (AutoScrollViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        /*  Auto scroll started               */
            mViewPager.startAutoScroll(3000);
            pageSwitcher(1);
        /*  Inbuilt cirle page to auto scroll */

        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.titles);
        mIndicator = indicator;
        indicator.setViewPager(mViewPager);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);
        indicator.setPageColor(0x99999999);  //0xFFFFFFFF
        indicator.setFillColor(0xFFFFFFFF); // 0x880000FF

    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 3000); // delay

    }
    // this is an inner class...
    class RemindTask extends TimerTask {
        @Override
        public void run() {

            runOnUiThread(new Runnable() {
                public void run() {
            if (page == 3) { // In my case the number of pages are 3 // if (page > 3)
                timer.cancel();

                btnLayout.setVisibility(View.VISIBLE);
                StartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        startActivity(new Intent(AutoScrollImageSlide.this, MainActivity.class));
                        /*
                        boolean value = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LOGGED_IN, false);
                        if(value==true) {
                            startActivity(new Intent(AutoScrollImageSlide.this, HomeActivity.class));
                        }
                        else {
                            startActivity(new Intent(AutoScrollImageSlide.this, MainActivity.class));
                        }
                        */

                    }
                });

            } else {
                mViewPager.setCurrentItem(page++);
            }
                }
            });
        }
    }

    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
            //   count_layout = (LinearLayout) findViewById(R.id.image_count);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView.setImageResource(mResources[position]);

            container.addView(itemView);
            mViewPager.stopAutoScroll();
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);





        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }


}
