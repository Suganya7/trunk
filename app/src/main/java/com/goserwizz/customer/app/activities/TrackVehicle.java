package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.goserwizz.customer.app.Adapters.TrackListAdapter;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/16/2016.
 */
public class TrackVehicle extends AppCompatActivity {

    private Toolbar toolbar;
    ListView listview;
    ImageView imgTrack;
    TextView titleTrack;
    public static String[] Datetime ={ };
    public static String[] RegNum ={};

    public static Integer[] imgid={};

    public static String[] ModelNum ={};

    public static String[] serviceDetails ={};

    public static String[] serviceID ={};

    public static String[] Mileage ={};

    public static String[] serviceCenter ={};

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_SID= "s_id";

    String status, resultmsg, gu_id;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
            dealerState, dealerEmail, serviceId, addr, email, mileage, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
            paystatus, slotDate, dealerPerson, dealerMob, trackvehicleStatus;
    ArrayList<String> VehicleRegnoList, dateTimeList, VehicleModelList, ServiceDetailList, ServiceIDList, MileageList, ServiceCenterList;
    TrackListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trackvehicle);

        toolbar = (Toolbar)findViewById(R.id.trackVehicletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Track your Vehicle");

        VehicleRegnoList = new ArrayList<String>();
        dateTimeList = new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        ServiceDetailList = new ArrayList<String>();
        ServiceIDList = new ArrayList<String>();
        MileageList = new ArrayList<String>();
        ServiceCenterList = new ArrayList<String>();

        listview=(ListView)findViewById(R.id.trackListView);
        titleTrack = (TextView)findViewById(R.id.titleTrack);
        imgTrack = (ImageView)findViewById(R.id.imageTrack);

      //  adapter=new TrackListAdapter(this, Datetime, ModelNum, RegNum, imgid, serviceDetails, serviceID, Mileage,serviceCenter);
      //  listview.setAdapter(adapter);

        if(Connectivity.isConnected(this)) {

            new JsonGetServiceDetail_TrackVehicle().execute();

        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Slecteditem = RegNum[+position];

              /*  if(position == 0) {
                    titleTrack.setText("Your Vehicle is under service");
                    imgTrack.setImageResource(R.drawable.under_service);
                }
                else if(position == 1) {
                    titleTrack.setText("Ready for Delivery");
                    imgTrack.setImageResource(R.drawable.delivery);
                }
                else if(position == 2) {
                    titleTrack.setText("Vehicle Delivered");
                    imgTrack.setImageResource(R.drawable.vehi_delivery);
                }
                else if(position == 3) {
                    titleTrack.setText("Cancelled");
                    imgTrack.setImageResource(R.drawable.cancelled);
                }  */

             //   Toast.makeText(getApplicationContext(), "track Regno: " + Slecteditem, Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetServiceDetail_TrackVehicle extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(TrackVehicle.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            VehicleRegnoList.clear();
            dateTimeList.clear();
            VehicleModelList.clear();
            ServiceDetailList.clear();
            ServiceIDList.clear();
            MileageList.clear();
            ServiceCenterList.clear();

            String s_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_S_ID_TRACK, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SID, s_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getservicedet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                            //    JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                            //    Iterator<?> dataObj_keys = dataObj.keys();

                                    JSONObject tmpObj = (JSONObject) jObj.get(jObj_key);
                                    serviceType = tmpObj.get("serviceType").toString();
                                    dealerCountry = tmpObj.get("dealerCountry").toString();
                                    mob = tmpObj.get("mob").toString();
                                    dealerCity = tmpObj.get("dealerCity").toString();
                                    cmts = tmpObj.get("cmts").toString();
                                    dealerAddr = tmpObj.get("dealerAddr").toString();
                                    advance = tmpObj.get("advance").toString();
                                    dealerLocation = tmpObj.get("dealerLocation").toString();
                                    slotTime = tmpObj.get("slotTime").toString();
                                    paymode = tmpObj.get("paymode").toString();
                                    dealerCompName = tmpObj.get("dealerCompName").toString();
                                    dealerState = tmpObj.get("dealerState").toString();
                                    dealerEmail = tmpObj.get("dealerEmail").toString();
                                    serviceId = tmpObj.get("serviceId").toString();
                                    addr = tmpObj.get("addr").toString();
                                    email = tmpObj.get("email").toString();
                                    mileage = tmpObj.get("mileage").toString();
                                    dealerPincode = tmpObj.get("dealerPincode").toString();
                                    pickup = tmpObj.get("pickup").toString();
                                    vehicleNo = tmpObj.get("vehicleNo").toString();
                                    modelName = tmpObj.get("modelName").toString();
                                    bookedDate = tmpObj.get("bookedDate").toString();
                                    name = tmpObj.get("name").toString();
                                    paystatus = tmpObj.get("paystatus").toString();
                                    slotDate = tmpObj.get("slotDate").toString();
                                    dealerPerson = tmpObj.get("dealerPerson").toString();
                                    dealerMob = tmpObj.get("dealerMob").toString();
                                    trackvehicleStatus =  tmpObj.get("status").toString();

                                    VehicleRegnoList.add(vehicleNo);
                                    dateTimeList.add(slotDate + "/" + slotTime);
                                    VehicleModelList.add(modelName);
                                    ServiceDetailList.add(serviceType);
                                    ServiceIDList.add(serviceId);
                                    MileageList.add(mileage);
                                    ServiceCenterList.add(dealerCompName);


                                    System.out.println(tmpObj.get("serviceId"));
                                    System.out.println(tmpObj.get("vehicleNo") + "   cmts:  " + tmpObj.get("cmts"));

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            RegNum = new String[VehicleRegnoList.size()];
            RegNum = VehicleRegnoList.toArray(RegNum);

            Datetime = new String[dateTimeList.size()];
            Datetime = dateTimeList.toArray(Datetime);

            ModelNum = new String[VehicleModelList.size()];
            ModelNum = VehicleModelList.toArray(ModelNum);

            serviceDetails = new String[ServiceDetailList.size()];
            serviceDetails = ServiceDetailList.toArray(serviceDetails);

            serviceID = new String[ServiceIDList.size()];
            serviceID = ServiceIDList.toArray(serviceID);

            Mileage = new String[MileageList.size()];
            Mileage = MileageList.toArray(Mileage);

            serviceCenter = new String[ServiceCenterList.size()];
            serviceCenter = ServiceCenterList.toArray(serviceCenter);

            String Connection = (Connectivity.isConnected(TrackVehicle.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetServiceDetail_TrackVehicle().execute();
                    }
                    else {
                        adapter=new TrackListAdapter(TrackVehicle.this, Datetime, ModelNum, RegNum, serviceDetails, serviceID, Mileage,serviceCenter);
                        listview.setAdapter(adapter);

                        if(trackvehicleStatus.equals("Booked")) {
                            titleTrack.setText("Your Vehicle is Booked for a service");
                            imgTrack.setImageResource(R.drawable.under_service);
                        }
                        else if(trackvehicleStatus.equals("Vehicle Received")) {
                            titleTrack.setText("Your Vehicle is Received");
                            imgTrack.setImageResource(R.drawable.under_service);
                        }
                        else if(trackvehicleStatus.equals("Under Service")) {
                            titleTrack.setText("Your Vehicle is under service");
                            imgTrack.setImageResource(R.drawable.under_service);
                        }
                        else if(trackvehicleStatus.equals("Work In Progress")) {
                            titleTrack.setText("Your Vehicle is Work In Progress");
                            imgTrack.setImageResource(R.drawable.under_service);
                        }
                        else if(trackvehicleStatus.equals("Ready for Delivery")) {
                            titleTrack.setText("Your Vehicle is Ready for Delivery");
                            imgTrack.setImageResource(R.drawable.delivery);
                        }
                        else if(trackvehicleStatus.equals("Vehicle Delivered")) {

                            titleTrack.setText("Your Vehicle is Delivered");
                            imgTrack.setImageResource(R.drawable.vehi_delivery);
                        }
                        else if(trackvehicleStatus.equals("Cancelled")) {
                            titleTrack.setText("Your Vehicle Service is Cancelled");
                            imgTrack.setImageResource(R.drawable.cancelled);

                        }
                        else if(trackvehicleStatus.equals("Completed")) {
                            titleTrack.setText("Your Vehicle Service is Completed");
                            imgTrack.setImageResource(R.drawable.delivery);

                        }


                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetServiceDetail_TrackVehicle().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            startActivity(new Intent(TrackVehicle.this, HomeActivity.class));

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

}
