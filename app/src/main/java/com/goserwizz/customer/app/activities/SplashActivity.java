package com.goserwizz.customer.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;

/**
 * Created by Kaptas on 3/5/2016.
 */
public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);


        mJumpRunnable = new Runnable() {
            public void run() {
               // startActivity(new Intent(SplashActivity.this, MainActivity.class));
                LoggedInAction();
            }
        };
        mHandler = new Handler();
        mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);
    }

    public void LoggedInAction() {

        SettingsUtils.init(this);
        if (!SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LOGGED_IN, false)) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            Log.d("Login Data", SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, ""));
            Log.d("Login Data", SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, ""));
            Log.d("Login Data", SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, ""));
            startActivity(new Intent(this, HomeActivity.class));
        }
      //  finish();
    }

}
