package com.goserwizz.customer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goserwizz.customer.app.R;

/**
 * Created by Kaptas on 3/14/2016.
 */
public class EmergencyAssistant extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.emergency_assistant, container, false);

        return v;
    }
}
