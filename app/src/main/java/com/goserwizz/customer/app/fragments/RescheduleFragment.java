package com.goserwizz.customer.app.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/11/2016.
 */
public class RescheduleFragment extends Fragment {

    ImageView slot1, slot2, slot3, slot4, slot5, slot6, slot7, questionMark;
    EditText r_vehi_regno, r_vehi_mileage, r_vehi_comments;
    String regno, mileage;
    Button ResheduleBtn;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_S_ID = "s_id";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "sdate";
    private static final String TAG_STIME = "stime";
    private static final String TAG_SCOMMENTS = "scmts";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_REASONS = "reasons";
    private static final String TAG_UPDATE_ID = "update_id";
    private static final String TAG_UPDATE_BY = "update_by";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";

    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";

    String status, resultmsg, dealer_id, s_date, s_time, gu_id;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
            dealerState, dealerEmail, serviceId, addr, email, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
            paystatus, slotDate, dealerPerson, dealerMob;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.reschedule_fragment, container, false);

        SettingsUtils.init(getActivity());
        r_vehi_regno = (EditText)v.findViewById(R.id.r_vehi_regno);
        r_vehi_mileage = (EditText)v.findViewById(R.id.r_vehi_mileage);
        r_vehi_comments = (EditText)v.findViewById(R.id.r_vehi_comments);
        ResheduleBtn = (Button)v.findViewById(R.id.ResheduleBtn);

        regno = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
        mileage = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MILEAGE, "");
        r_vehi_regno.setText(regno);
        r_vehi_mileage.setText(mileage);

        slot1 = (ImageView)v.findViewById(R.id.slots1);
        slot2 = (ImageView)v.findViewById(R.id.slots2);
        slot3 = (ImageView)v.findViewById(R.id.slots3);
        slot4 = (ImageView)v.findViewById(R.id.slots4);
        slot5 = (ImageView)v.findViewById(R.id.slots5);
        slot6 = (ImageView)v.findViewById(R.id.slots6);
      //  slot7 = (ImageView)v.findViewById(R.id.slots7);
        questionMark = (ImageView)v.findViewById(R.id.questionMark);

        slot1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot1.setImageResource(R.drawable.bookedcar);
            }
        });

        slot2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot2.setImageResource(R.drawable.selectcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                slot6.setImageResource(R.drawable.availcar);
                //  slot7.setImageResource(R.drawable.availcar);
            }
        });

        slot3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot3.setImageResource(R.drawable.selectcar);
                slot2.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                slot6.setImageResource(R.drawable.availcar);
                // slot7.setImageResource(R.drawable.availcar);
            }
        });

        slot4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot4.setImageResource(R.drawable.selectcar);
                slot3.setImageResource(R.drawable.availcar);
                slot2.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                slot6.setImageResource(R.drawable.availcar);
                //   slot7.setImageResource(R.drawable.availcar);
            }
        });

        slot5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot5.setImageResource(R.drawable.selectcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot2.setImageResource(R.drawable.availcar);
                slot6.setImageResource(R.drawable.availcar);
                //   slot7.setImageResource(R.drawable.availcar);
            }
        });

        slot6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot6.setImageResource(R.drawable.selectcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                slot2.setImageResource(R.drawable.availcar);
                //  slot7.setImageResource(R.drawable.availcar);
            }
        });


        questionMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDFragment alertdFragment = new AlertDFragment();
                alertdFragment.show(getFragmentManager(), "message");

            }
        });


        ResheduleBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click

                if (!validateServiceDetails()) {
                    return;
                }

                if(Connectivity.isConnected(getActivity())) {

                    new JsonGetRescheduleService().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(getActivity());
                }


            }
        });


        return v;

    }


    public static class AlertDFragment extends android.support.v4.app.DialogFragment {
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity())
                    //  .setIcon(R.drawable.alerticon)
                    .setTitle("Alert!")
                    .setMessage("Number of Slots will be here")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Do something else
                        }
                    }).create();
        }
    }

    private boolean validateServiceDetails() {
        if (r_vehi_comments.getText().toString().trim().isEmpty()) {
            r_vehi_comments.setError(getString(R.string.err_servicedetails));
            requestFocus(r_vehi_comments);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetRescheduleService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
            s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
            s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_S_ID, "BACF38609489"));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, s_date));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIME, s_time));
            nameValuePairs.add(new BasicNameValuePair(TAG_STATUS, "Booked"));
            nameValuePairs.add(new BasicNameValuePair(TAG_SCOMMENTS, ""));
            nameValuePairs.add(new BasicNameValuePair(TAG_REASONS, "Due to other commitments"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, "outstation"));
            nameValuePairs.add(new BasicNameValuePair(TAG_UPDATE_ID, "145"));
            nameValuePairs.add(new BasicNameValuePair(TAG_UPDATE_BY, "Customer"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, "145"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "rescheduleservice"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetRescheduleService().execute();
                    }
                    else {

                        showResultDialogFail(resultmsg);

                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetRescheduleService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            getActivity().finish();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetHistoryService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getbookhistory"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    serviceType = tmpObj.get("serviceType").toString();
                                    dealerCountry = tmpObj.get("dealerCountry").toString();
                                    mob = tmpObj.get("mob").toString();
                                    dealerCity = tmpObj.get("dealerCity").toString();
                                    cmts = tmpObj.get("cmts").toString();
                                    dealerAddr = tmpObj.get("dealerAddr").toString();
                                    advance = tmpObj.get("advance").toString();
                                    dealerLocation = tmpObj.get("dealerLocation").toString();
                                    slotTime = tmpObj.get("slotTime").toString();
                                    paymode = tmpObj.get("paymode").toString();
                                    dealerCompName = tmpObj.get("dealerCompName").toString();
                                    dealerState = tmpObj.get("dealerState").toString();
                                    dealerEmail = tmpObj.get("dealerEmail").toString();
                                    serviceId = tmpObj.get("serviceId").toString();
                                    addr = tmpObj.get("addr").toString();
                                    email = tmpObj.get("email").toString();
                                    mileage = tmpObj.get("mileage").toString();
                                    dealerPincode = tmpObj.get("dealerPincode").toString();
                                    pickup = tmpObj.get("pickup").toString();
                                    vehicleNo = tmpObj.get("vehicleNo").toString();
                                    modelName = tmpObj.get("modelName").toString();
                                    bookedDate = tmpObj.get("bookedDate").toString();
                                    name = tmpObj.get("name").toString();
                                    paystatus = tmpObj.get("paystatus").toString();
                                    slotDate = tmpObj.get("slotDate").toString();
                                    dealerPerson = tmpObj.get("dealerPerson").toString();
                                    dealerMob = tmpObj.get("dealerMob").toString();


                                    System.out.println(tmpObj.get("serviceId"));
                                    System.out.println(tmpObj.get("vehicleNo") + "   cmts:  " + tmpObj.get("cmts"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */


            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetHistoryService().execute();
                    }
                    else {

                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetHistoryService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }



}
