package com.goserwizz.customer.app.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.CancelServiceDetail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Kaptas on 4/22/2016.
 */
public class CancelListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] dateTime, regNO, Scenter, ServiceIDs;
    ArrayList<String> datelist;

    public CancelListAdapter(Activity context, String[] date, String[] regno, String[] serviceCenter, String[] serviceIds) {
        super(context, R.layout.cancelservice_items, regno);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.dateTime=date;
        this.regNO=regno;
        this.Scenter=serviceCenter;
        this.ServiceIDs=serviceIds;
    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {
        TextView datetime, regno, servicecenter, canel_servicedet;
    }

    public View getView(final int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.cancelservice_items, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        TextView Cancel_Item_datetime = (TextView) rowView.findViewById(R.id.Cancel_Item_datetime);
        TextView C_Item_regno = (TextView) rowView.findViewById(R.id.C_Item_regno);
        TextView C_Item_service_center = (TextView) rowView.findViewById(R.id.C_Item_service);
        TextView cancel_service_Detail = (TextView) rowView.findViewById(R.id.cancel_service_det);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.Cancel_Item_datetime, viewHolder.datetime);
        rowView.setTag(R.id.C_Item_regno, viewHolder.regno);
        rowView.setTag(R.id.C_Item_service, viewHolder.servicecenter);
        rowView.setTag(R.id.cancel_service_det, viewHolder.canel_servicedet);


        String serviceid = ServiceIDs[position];

        Cancel_Item_datetime.setText(dateTime[position]);
        C_Item_regno.setText(regNO[position]);
        C_Item_service_center.setText(Scenter[position]);

        datelist = SettingsUtils.getInstance().getListString(SettingsUtils.KEY_BOOKEDDATE);
        datelist.get(position);


        cancel_service_Detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date strDate = null;
                try {
                    strDate = sdf.parse(datelist.get(position));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (new Date().before(strDate)) {
                    //   Toast.makeText(getContext(), "Booked date not completed" + strDate, Toast.LENGTH_LONG).show();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_SERVICEID, ServiceIDs[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CANCEL_regNO, regNO[position]);

                    context.startActivity(new Intent(getContext(), CancelServiceDetail.class));
                } else {
                    //Toast.makeText(getContext(), "Your Booked Date is completed" + strDate, Toast.LENGTH_LONG).show();

                    showResultDialog("This Booked Service is over date, so you cannot be able to Cancel the Service ");

                }

                //   Toast.makeText(context, serviceId[position] +"  ,  " + regNO[position], Toast.LENGTH_LONG).show();

            }
        });


        return rowView;

    }

    private void showResultDialog(final String result) {
        if (context != null && !context.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(context)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }
}
