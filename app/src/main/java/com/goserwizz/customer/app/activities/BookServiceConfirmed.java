package com.goserwizz.customer.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;

/**
 * Created by Kaptas on 5/4/2016.
 */
public class BookServiceConfirmed extends AppCompatActivity {

    String serviceid, pickupaddress;
    String gu_id, d_id, vmodel, d_addr,d_name, vtype, name, email, mob, regnos, m_id, mileages, cmts, stype, sdate, stime, pickup, pickup_addr, paymode, amount;
    TextView service_id, datetime, dealername,regno, address, vehiclebrand, userpickup_addr;
    Button okbtn;
    LinearLayout BookPickupLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookmyservice_display);

        service_id = (TextView)findViewById(R.id.service_id);
        datetime = (TextView)findViewById(R.id.date_time);
        dealername = (TextView)findViewById(R.id.service_center_name);
        regno = (TextView)findViewById(R.id.vehicle_reg_no);
        address = (TextView)findViewById(R.id.service_center_addr);
        vehiclebrand = (TextView)findViewById(R.id.brand_name);
        BookPickupLayout = (LinearLayout)findViewById(R.id.BookPickupLayout);
        userpickup_addr = (TextView)findViewById(R.id.userpickup_addr);

        okbtn = (Button)findViewById(R.id.OkBS_Btn);


        serviceid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSERVICEID, "");
        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
        d_name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
        d_addr = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_ADDR, "");
        vtype =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
        name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
        m_id =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");
        regnos =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
        mileages =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MILEAGE, "");
        cmts =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_COMMENTS, "");
        sdate = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        stime = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
        pickup = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP, "");
        pickup_addr = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP_ADDRESS, "");
        stype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SERVICE_CATEGORY, "");
        vmodel =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODEL, "");


        if(pickup.equals("Y")) {
            BookPickupLayout.setVisibility(View.VISIBLE);
            userpickup_addr.setText(pickup_addr);
        }
        else {
            BookPickupLayout.setVisibility(View.GONE);
        }

        service_id.setText(serviceid);
        datetime.setText(sdate + " / " + stime);
        dealername.setText(d_name);
        regno.setText(regnos);
        address.setText(d_addr);
        vehiclebrand.setText(vmodel);

        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(BookServiceConfirmed.this,HomeActivity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(a);

              //  startActivity(new Intent(BookServiceConfirmed.this, HomeActivity.class));

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, "");

                finish();
            }
        });


    }

     @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {  //&& isNightModeToggled

            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, "");
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, "");
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, "");


            Intent a = new Intent(this,HomeActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
