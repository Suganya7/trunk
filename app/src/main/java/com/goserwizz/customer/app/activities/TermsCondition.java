package com.goserwizz.customer.app.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.goserwizz.customer.app.R;

/**
 * Created by Kaptas on 3/23/2016.
 */
public class TermsCondition extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_terms);

        toolbar = (Toolbar) findViewById(R.id.Termstoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Terms & Conditions");

        WebView view = new WebView(this);
        view.setVerticalScrollBarEnabled(false);

        ((LinearLayout)findViewById(R.id.ContentsLayout)).addView(view);

        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl("file:///android_asset/terms-and-conditions.html");


    //    view.loadData(getString(R.string.callsupport), "text/html", "utf-8");

    }
    }
