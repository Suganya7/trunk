package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/18/2016.
 */
public class PaymentActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://3vikram.in/custDet.do";
    // JSON Node names
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_DEALER_CITY = "d_city";
    private static final String TAG_VEH_TYPE = "vtype";
    private static final String TAG_NAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_REGNO = "regno";
    private static final String TAG_MODEL_ID = "m_id";
    private static final String TAG_MILEAGE = "mileage";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_STYPE = "stype";
    private static final String TAG_SDATE = "sdate";
    private static final String TAG_STIME = "stime";
    private static final String TAG_PICKUP = "pickup";
    private static final String TAG_PICKUP_ADDRESS = "pickup_addr";
    private static final String TAG_PAYMODE = "paymode";
    private static final String TAG_PAY_AMOUNT = "amount";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_SERVICE_ID = "s_id";
    String status, resultmsg, service_id;
    String gu_id, d_id, d_city, vtype, name, email, mob, regno, m_id, mileage, cmts, stype, sdate, stime, pickup, pickup_addr, paymode, amount;
    Button paynowBtn;
    TextView DebitCreditCard, Netbanking, PayUmoney, Paytm, vehiModel, vehiRegno, vehiDate, vehiServiceCenter;
    String model, reg, date, servicecenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbarPlace);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment");

        SettingsUtils.init(this);

        model = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MODEL, "");
        reg = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
        date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "") +"/" + SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
        servicecenter = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");

        vehiModel = (TextView)findViewById(R.id.paymentModel);
        vehiRegno = (TextView)findViewById(R.id.paymentRegmno);
        vehiDate = (TextView)findViewById(R.id.paymentDTvalue);
        vehiServiceCenter = (TextView)findViewById(R.id.PaymentServiceCenter);
        DebitCreditCard = (TextView)findViewById(R.id.Debitcard);
        Netbanking = (TextView)findViewById(R.id.NetBanking);
        PayUmoney = (TextView)findViewById(R.id.PayUmoney);
        Paytm = (TextView)findViewById(R.id.Paytm);

        vehiModel.setText(model);
        vehiRegno.setText(reg);
        vehiDate.setText(date);
        vehiServiceCenter.setText(servicecenter);

        paynowBtn = (Button)findViewById(R.id.paynowBtn);
        paynowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BookService();
            }
        });

        DebitCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DebitCreditCard.setTextColor(Color.parseColor("#cc0000"));
                Netbanking.setTextColor(Color.parseColor("#999999"));
                PayUmoney.setTextColor(Color.parseColor("#999999"));
                Paytm.setTextColor(Color.parseColor("#999999"));
            }
        });

        Netbanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DebitCreditCard.setTextColor(Color.parseColor("#999999"));
                Netbanking.setTextColor(Color.parseColor("#cc0000"));
                PayUmoney.setTextColor(Color.parseColor("#999999"));
                Paytm.setTextColor(Color.parseColor("#999999"));
            }
        });

        PayUmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DebitCreditCard.setTextColor(Color.parseColor("#999999"));
                Netbanking.setTextColor(Color.parseColor("#999999"));
                PayUmoney.setTextColor(Color.parseColor("#cc0000"));
                Paytm.setTextColor(Color.parseColor("#999999"));
            }
        });

        Paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DebitCreditCard.setTextColor(Color.parseColor("#999999"));
                Netbanking.setTextColor(Color.parseColor("#999999"));
                PayUmoney.setTextColor(Color.parseColor("#999999"));
                Paytm.setTextColor(Color.parseColor("#cc0000"));
            }
        });

    }

    public void BookService() {

       gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
       d_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
       d_city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_CITY, "");
       vtype =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
       name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
       mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
       email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
       m_id =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");
       regno =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
       mileage =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MILEAGE, "");
       cmts =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_COMMENTS, "");
       sdate = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
       stime = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
       pickup = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP, "");
       pickup_addr = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP_ADDRESS, "");


        if(Connectivity.isConnected(this)) {

            new JsonGetBookService().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetBookService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(PaymentActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, d_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_CITY, d_city));
            nameValuePairs.add(new BasicNameValuePair(TAG_VEH_TYPE, vtype));
            nameValuePairs.add(new BasicNameValuePair(TAG_NAME, name));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, email));
            nameValuePairs.add(new BasicNameValuePair(TAG_MOBILE, mob));
            nameValuePairs.add(new BasicNameValuePair(TAG_REGNO, regno));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODEL_ID, m_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MILEAGE, mileage));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, cmts));
            nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, "First Free Service"));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIME, stime));
            nameValuePairs.add(new BasicNameValuePair(TAG_PICKUP, pickup));
            nameValuePairs.add(new BasicNameValuePair(TAG_PICKUP_ADDRESS, pickup_addr));
            nameValuePairs.add(new BasicNameValuePair(TAG_PAYMODE, "PayUMoney"));
            nameValuePairs.add(new BasicNameValuePair(TAG_PAY_AMOUNT, "0.00"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "bookservice"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            resultmsg = jObj.getString(TAG_MSG);

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                                service_id = dataObj.get(TAG_SERVICE_ID).toString();

                                System.out.println(dataObj.get(TAG_SERVICE_ID));
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(PaymentActivity.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonGetBookService().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {
                        showResultDialog(resultmsg);
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonGetBookService().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialog(resultmsg); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            finish();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

}
