package com.goserwizz.customer.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

/**
 * Created by Kaptas on 3/16/2016.
 */
public class ServiceDeposit extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    Button confirmdeposit;
    ImageView slot1, slot2, slot3, slot4, slot5, slot6, slot7, questionMark;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_WEEKOFF = "weekoff";
    private static final String TAG_DAY = "day";
    private static final String TAG_DATESTATUS = "status";
    private static final String TAG_HOLIDAY = "holiday";

    private static final String TAG_SDATE = "s_date";
    private static final String TAG_STIME = "s_time";

    private static String url2 = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_DEALER_CITY = "d_city";
    private static final String TAG_VEH_TYPE = "vtype";
    private static final String TAG_NAME = "name";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_REGNO = "regno";
    private static final String TAG_MODEL_ID = "m_id";
    private static final String TAG_MILEAGE = "mileage";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_STYPE = "stype";
    private static final String TAG_SDATEs = "sdate";
    private static final String TAG_STIMEs = "stime";
    private static final String TAG_PICKUP = "pickup";
    private static final String TAG_PICKUP_ADDRESS = "pickup_addr";
    private static final String TAG_PAYMODE = "paymode";
    private static final String TAG_PAY_AMOUNT = "amount";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_SERVICE_ID = "s_id";
    String statuss, resultmsgs, service_id;
    String gu_id, d_id, d_city, vtype, name, email, mob, regnos, m_id, mileages, cmts, stype, sdate, stime, pickup, pickup_addr, paymode, amount;
    Button bookMyService_Btn;
    TextView pickupaddrHeader, DebitCreditCard, Netbanking, PayUmoney, Paytm, vehiModel, vehiRegno, vehiDate, vehiServiceCenter;
    String model, reg, date, servicecenter, onlineslotavailable;


    String fname, lname, emailid, pwd, mobileNum;
    String status, digest, resultmsg;
    String location, address, city, state, pincode, regno, mileage, bookedslots;
   // TextView pickup_address1, pickup_address2, pickup_address3;
    EditText s_vehi_regno, s_vehi_mileage, s_vehi_comments, pickup_address1;
    CheckBox pickup_drop_checkbox;
    View pickupView;

    private static final long SPLASH_TIME = 10000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;
    Timer timer;
    int page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.service_confirm, container, false);

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FRAGMENT, "servicecenter");

        slot1 = (ImageView)v.findViewById(R.id.slot1);
        slot2 = (ImageView)v.findViewById(R.id.slot2);
        slot3 = (ImageView)v.findViewById(R.id.slot3);
        slot4 = (ImageView)v.findViewById(R.id.slot4);
        slot5 = (ImageView)v.findViewById(R.id.slot5);
      //  slot6 = (ImageView)v.findViewById(R.id.slot6);
      //  slot7 = (ImageView)v.findViewById(R.id.slot7);
      //  questionMark = (ImageView)v.findViewById(R.id.questionMark);
        s_vehi_regno = (EditText)v.findViewById(R.id.s_vehi_regno);
        s_vehi_mileage = (EditText)v.findViewById(R.id.s_vehi_mileage);
        s_vehi_comments = (EditText)v.findViewById(R.id.s_vehi_comments);
        pickup_address1 = (EditText)v.findViewById(R.id.pickup_address1);
        pickupaddrHeader = (TextView)v.findViewById(R.id.pickupaddrHeader);
        pickupView = (View)v.findViewById(R.id.pickupView);

        s_vehi_regno.setOnClickListener(this);
        s_vehi_mileage.setOnClickListener(this);
        s_vehi_comments.setOnClickListener(this);

        s_vehi_regno.addTextChangedListener(new MyTextWatcher(s_vehi_regno));

       // pickup_address2 = (TextView)v.findViewById(R.id.pickup_address2);
       // pickup_address3 = (TextView)v.findViewById(R.id.pickup_address3);
        pickup_drop_checkbox = (CheckBox)v.findViewById(R.id.PickUpDropCheck);
        bookMyService_Btn = (Button)v.findViewById(R.id.bookMyService_Btn);

        location = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LOCATION, "");
        address = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_ADDRESS, "");
        city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_USER_CITY, "");
        state = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_STATE, "");
        pincode = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PINCODE, "");

      //  regno = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
      //  mileage = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MILEAGE, "");
      //  s_vehi_regno.setText(regno);
      //  s_vehi_mileage.setText(mileage);

        LinearLayout linearLayout = (LinearLayout)v.findViewById(R.id.serviceCenterParent);
        setupUI(linearLayout);

        pickup_drop_checkbox.setOnCheckedChangeListener(this);


        /* select date and time value to get the JSON response from getbooked slot function */
     /*

        mJumpRunnable = new Runnable() {
            public void run() {
              //  Bookedslot();
            }
        };
        mHandler = new Handler();
        mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);   */


          Bookedslot();


      //  timer = new Timer(); // At this line a new Thread will be created
      //  timer.scheduleAtFixedRate(new RemindTask(), 0, 2 * 3000); // delay


     /*   slot6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                slot6.setImageResource(R.drawable.selectcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                slot2.setImageResource(R.drawable.availcar);
              //  slot7.setImageResource(R.drawable.availcar);
            }
        });


        questionMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDFragment alertdFragment = new AlertDFragment();
                alertdFragment.show(getFragmentManager(), "message");

            }
        });  */



        bookMyService_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateREGno()) {
                    return;
                }

                if (!validateMileage()) {
                    return;
                }

                if (!validateServiceDetails()) {
                    return;
                }

                if(pickup_drop_checkbox.isChecked()) {

                    if (!validatePickupDropAddr()) {
                        return;
                    }

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PICKUP, "Y");

                }
                else {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PICKUP, "N");
                }

                String addr = pickup_address1.getText().toString();
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEH_REGNO,  s_vehi_regno.getText().toString());
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEH_MILEAGE,  s_vehi_mileage.getText().toString());
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_COMMENTS, s_vehi_comments.getText().toString());
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PICKUP_ADDRESS, pickup_address1.getText().toString());


                String slotvalue = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SLOTAVAILABILITY, "");
              //  Toast.makeText(getActivity(), slotvalue + " slotselect", Toast.LENGTH_LONG).show();

                if(slotvalue.equals("yes")) {

                    BookService();
                }
                else if(slotvalue.equals("no")) {
                    showResultDialogFail1("Select Slot Availability");
                }

                //startActivity(new Intent(getActivity(), PaymentActivity.class));
            }
        });




        return v;
    }


    public void Bookedslot() {


        onlineslotavailable = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_ONLINESLOT, "");

        bookedslots = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKEDSLOTS, "");

        if(!bookedslots.equals("0")) {
            //   Toast.makeText(getActivity(), bookedslots, Toast.LENGTH_LONG).show();

            if(bookedslots.equals("1")) {
                slot1.setImageResource(R.drawable.bookedcar);
                slot2.setImageResource(R.drawable.availcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSLOT_NUM, "1");

            }
            else if(bookedslots.equals("2")) {
                slot1.setImageResource(R.drawable.bookedcar);
                slot2.setImageResource(R.drawable.bookedcar);
                slot3.setImageResource(R.drawable.availcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSLOT_NUM, "2");
            }
            else if(bookedslots.equals("3")) {
                slot1.setImageResource(R.drawable.bookedcar);
                slot2.setImageResource(R.drawable.bookedcar);
                slot3.setImageResource(R.drawable.bookedcar);
                slot4.setImageResource(R.drawable.availcar);
                slot5.setImageResource(R.drawable.availcar);

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSLOT_NUM, "3");
            }
            else if(bookedslots.equals("4")) {
                slot1.setImageResource(R.drawable.bookedcar);
                slot2.setImageResource(R.drawable.bookedcar);
                slot3.setImageResource(R.drawable.bookedcar);
                slot4.setImageResource(R.drawable.bookedcar);
                slot5.setImageResource(R.drawable.availcar);

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSLOT_NUM, "4");

            }

        }
        else {
            slot1.setImageResource(R.drawable.availcar);
            slot2.setImageResource(R.drawable.availcar);
            slot3.setImageResource(R.drawable.availcar);
            slot4.setImageResource(R.drawable.availcar);
            slot5.setImageResource(R.drawable.availcar);

            //   Toast.makeText(getActivity(), bookedslots, Toast.LENGTH_LONG).show();
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSLOT_NUM, "0");
        }

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");

        onlineslotavailable = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_ONLINESLOT, "");
        bookedslots = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKEDSLOTS, "");

        if(bookedslots.equals("")) {

            Log.d("Slots are empty:", bookedslots + " online: " + onlineslotavailable);

            slot1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                }
            });

            slot2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                }
            });

            slot3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                }
            });

            slot4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                }
            });

            slot5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                }
            });
        }
        else {


            if (!onlineslotavailable.equals("")) {


                int onlineslot = Integer.parseInt(onlineslotavailable);
                int bookedslot = Integer.parseInt(bookedslots);

                if (bookedslot < onlineslot) {
                    // Booked slot is lesser than online slot only than they can pick the slots or else show dialog to pick another time//


                    slot1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String bookslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSLOT_NUM, "");
                            if (bookslot.equals("1")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");

                            } else if (bookslot.equals("2")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else if (bookslot.equals("3")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else if (bookslot.equals("4")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.bookedcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else {
                                slot1.setImageResource(R.drawable.selectcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            }


                        }
                    });

                    slot2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String bookslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSLOT_NUM, "");

                            if (bookslot.equals("1")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.selectcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("2")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else if (bookslot.equals("3")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else if (bookslot.equals("4")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.bookedcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else {
                                slot1.setImageResource(R.drawable.availcar);
                                slot2.setImageResource(R.drawable.selectcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            }


                            //  slot6.setImageResource(R.drawable.availcar);
                            //  slot7.setImageResource(R.drawable.availcar);
                        }
                    });

                    slot3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String bookslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSLOT_NUM, "");
                            if (bookslot.equals("1")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.selectcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("2")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.selectcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("3")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else if (bookslot.equals("4")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.bookedcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else {
                                slot1.setImageResource(R.drawable.availcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.selectcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            }
                            //  slot6.setImageResource(R.drawable.availcar);
                            // slot7.setImageResource(R.drawable.availcar);
                        }
                    });

                    slot4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String bookslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSLOT_NUM, "");
                            if (bookslot.equals("1")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.selectcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("2")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.selectcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("3")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.selectcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("4")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.bookedcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "no");
                            } else {
                                slot1.setImageResource(R.drawable.availcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.selectcar);
                                slot5.setImageResource(R.drawable.availcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            }


                            //  slot6.setImageResource(R.drawable.availcar);
                            //   slot7.setImageResource(R.drawable.availcar);
                        }
                    });

                    slot5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String bookslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKSLOT_NUM, "");
                            if (bookslot.equals("1")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.selectcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("2")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.selectcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("3")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.selectcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else if (bookslot.equals("4")) {
                                slot1.setImageResource(R.drawable.bookedcar);
                                slot2.setImageResource(R.drawable.bookedcar);
                                slot3.setImageResource(R.drawable.bookedcar);
                                slot4.setImageResource(R.drawable.bookedcar);
                                slot5.setImageResource(R.drawable.selectcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            } else {
                                slot1.setImageResource(R.drawable.availcar);
                                slot2.setImageResource(R.drawable.availcar);
                                slot3.setImageResource(R.drawable.availcar);
                                slot4.setImageResource(R.drawable.availcar);
                                slot5.setImageResource(R.drawable.selectcar);

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SLOTAVAILABILITY, "yes");
                            }

                        }
                    });

                } else {

                    slot1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                        }
                    });

                    slot2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                        }
                    });

                    slot3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                        }
                    });

                    slot4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                        }
                    });

                    slot5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showResultDialogFail1("Online Booking slots are Finished, Select another Time");
                        }
                    });
                }

            }
        }


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub

        if (buttonView.isChecked()) {
            //checked
            pickup_address1.setVisibility(View.VISIBLE);
            pickupView.setVisibility(View.VISIBLE);
            pickupaddrHeader.setVisibility(View.VISIBLE);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PICKUP, "Y");


        }
        else
        {   //not checked
            pickup_address1.setVisibility(View.GONE);
            pickupView.setVisibility(View.GONE);
            pickupaddrHeader.setVisibility(View.GONE);
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PICKUP, "N");

        }
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence r, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
        @Override
        public void afterTextChanged(Editable s) {

            if (!validateREGno()) {
                return;
            }

        }

    }

    private boolean validateREGno() {
        String regno = s_vehi_regno.getText().toString().trim();

        if (regno.isEmpty() || !isValidREGno(regno)) {
            s_vehi_regno.setError(getString(R.string.err_regnoc));
            requestFocus(s_vehi_regno);
            return false;
        } else {
            //email.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidREGno(String regno) {

         Pattern sPattern
                = Pattern.compile("^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}$"); // ^([1-9][0-9]{0,2})?(\\.[0-9]?)?$,, ^[A-Z]{2}\s[0-9]{2}\s[A-Z]{2}\s[0-9]{4}$

        return !TextUtils.isEmpty(regno) && sPattern.matcher(regno).matches();
    }



    public static class AlertDFragment extends android.support.v4.app.DialogFragment {
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity())
                  //  .setIcon(R.drawable.alerticon)
                    .setTitle("Alert!")
                    .setMessage("Number of Slots will be here")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Do something else
                        }
                    }).create();
        }
    }

    private boolean validateServiceDetails() {
        if (s_vehi_comments.getText().toString().trim().isEmpty()) {
            s_vehi_comments.setError(getString(R.string.err_servicedetails));
            requestFocus(s_vehi_comments);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateRegno() {
        if (s_vehi_regno.getText().toString().trim().isEmpty()) {
            s_vehi_regno.setError(getString(R.string.err_regno));
            requestFocus(s_vehi_regno);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMileage() {
        if (s_vehi_mileage.getText().toString().trim().isEmpty()) {
            s_vehi_mileage.setError(getString(R.string.err_mileage));
            requestFocus(s_vehi_mileage);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePickupDropAddr() {
        if (pickup_address1.getText().toString().trim().isEmpty()) {
            pickup_address1.setError(getString(R.string.err_address));
            requestFocus(pickup_address1);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    @Override
    public void onClick(View v) {

       String dateValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
       String timeValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");

        switch (v.getId()) {
            case R.id.s_vehi_regno:

                if(dateValue.equals("") || timeValue.equals("")) {

                    InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    showResultDialogFail1("Double tap to select Date and Time");
                } else {

                }

                break;

            case R.id.s_vehi_mileage:

                if(dateValue.equals("") || timeValue.equals("")) {
                    InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    showResultDialogFail1("Double tap to select Date and Time");
                } else {

                }

                break;

            case R.id.s_vehi_comments:

                if(dateValue.equals("") || timeValue.equals("")) {
                    InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    showResultDialogFail1("Double tap to select Date and Time");
                } else {

                }

                break;
        }


    }



    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


    public void BookService() {

        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
        d_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
        d_city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_CITY, "");
        vtype =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
        name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
        m_id =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");
        regnos =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_REGNO, "");
        mileages =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEH_MILEAGE, "");
        cmts =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_COMMENTS, "");
        sdate = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        stime = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
        pickup = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP, "");
        pickup_addr = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PICKUP_ADDRESS, "");
        stype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SERVICE_CATEGORY, "");


        if(Connectivity.isConnected(getActivity())) {

            new JsonGetBookService().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(getActivity());
        }

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetBookService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, d_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_CITY, d_city));
            nameValuePairs.add(new BasicNameValuePair(TAG_VEH_TYPE, vtype));
            nameValuePairs.add(new BasicNameValuePair(TAG_NAME, name));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, email));
            nameValuePairs.add(new BasicNameValuePair(TAG_MOBILE, mob));
            nameValuePairs.add(new BasicNameValuePair(TAG_REGNO, regnos));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODEL_ID, m_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MILEAGE, mileages));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, cmts));
            nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATEs, sdate));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIMEs, stime));
            nameValuePairs.add(new BasicNameValuePair(TAG_PICKUP, pickup));
            nameValuePairs.add(new BasicNameValuePair(TAG_PICKUP_ADDRESS, pickup_addr));
            nameValuePairs.add(new BasicNameValuePair(TAG_PAYMODE, "PayUMoney"));
            nameValuePairs.add(new BasicNameValuePair(TAG_PAY_AMOUNT, "0.00"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "bookservice"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    statuss = jObj.getString(TAG_STATUS);
                    resultmsgs = jObj.getString(TAG_MSG);


                    if(statuss.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            resultmsgs = jObj.getString(TAG_MSG);

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                                service_id = dataObj.get(TAG_SERVICE_ID).toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKSERVICEID, service_id);

                                System.out.println(dataObj.get(TAG_SERVICE_ID));
                            }
                        }

                    }
                    else if(statuss.equals("fail")) {
                        resultmsgs = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return statuss;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsgs.contains("Communications link failure") || resultmsgs.contains("The last packet successfully")) {

                        new JsonGetBookService().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {
                       // showResultDialog(resultmsgs);

                        startActivity(new Intent(getActivity(), BookServiceConfirmed.class));
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsgs.contains("Communications link failure") || resultmsgs.contains("The last packet successfully")) {

                        new JsonGetBookService().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    } else { showResultDialogFail1(resultmsgs); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            startActivity(new Intent(getActivity(), HomeActivity.class));

                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, "");

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFail1(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                           // startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }



    @Override
    public void onPause() {
        super.onPause();

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    // this is an inner class...
    class RemindTask extends TimerTask {
        @Override
        public void run() {

            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    timer.cancel();

                    Bookedslot();


                }
            });
        }
    }

}