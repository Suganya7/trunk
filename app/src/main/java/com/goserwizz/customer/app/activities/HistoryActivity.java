package com.goserwizz.customer.app.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.HistoryCompletedService;
import com.goserwizz.customer.app.fragments.HistoryService;
import com.goserwizz.customer.app.fragments.HistoryUpcomingService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaptas on 3/14/2016.
 */
public class HistoryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.location1,
            R.drawable.settings,
    };
    PagerAdapter adapter;
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbarHistory);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("History");

        viewPager = (ViewPager) findViewById(R.id.viewpagerHistory);
    ////    setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabHistory);
    ////    tabLayout.setupWithViewPager(viewPager);


        tabLayout.addTab(tabLayout.newTab().setText("ALL SERVICE"));
        tabLayout.addTab(tabLayout.newTab().setText("UPCOMING"));
        tabLayout.addTab(tabLayout.newTab().setText("COMPLETED"));

        // setupTabIcons();

        HistoryService simpleListFragment = new HistoryService();
        getSupportFragmentManager().beginTransaction().replace(R.id.framehis, simpleListFragment).commit();

        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), R.color.White));

// added newly
        SettingsUtils.init(this);

     /*   viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        viewPager.getAdapter().notifyDataSetChanged();  */

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {



            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

              String tabselect =  tab.getText().toString();

                if(tabselect.equals("ALL SERVICE")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_HISTORY_TAB, tabselect);
               }
               else if(tabselect.equals("UPCOMING")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_HISTORY_TAB, tabselect);
                }
                else if(tabselect.equals("COMPLETED")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_HISTORY_TAB, tabselect);
                }

                if (tab.getPosition() == 0) {
                    HistoryService simpleListFragment = new HistoryService();
                    getSupportFragmentManager().beginTransaction().replace(R.id.framehis, simpleListFragment).commit();
                }
                else if (tab.getPosition() == 1) {
                    HistoryUpcomingService simpleListFragment = new HistoryUpcomingService();
                    getSupportFragmentManager().beginTransaction().replace(R.id.framehis, simpleListFragment).commit();
                }
                else if (tab.getPosition() == 2) {
                    HistoryCompletedService simpleListFragment = new HistoryCompletedService();
                    getSupportFragmentManager().beginTransaction().replace(R.id.framehis, simpleListFragment).commit();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

              /*  if (fragment != null) {
                    FragmentManager fm = getSupportFragmentManager();
                    if (fm.getBackStackEntryCount()>0) {
                        fm.popBackStack(fm.getBackStackEntryAt(0).getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    ft.remove(fragment);
                }   */

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HistoryService(), "ALL SERVICE");
        adapter.addFrag(new HistoryUpcomingService(), "UPCOMING");
        adapter.addFrag(new HistoryCompletedService(), "COMPLETED");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();



        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment1, String title) {

         //   fragment = fragment1;
            mFragmentList.add(fragment1);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
