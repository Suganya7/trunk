package com.goserwizz.customer.app.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.goserwizz.customer.app.Util.UserAddvehicle;
import com.goserwizz.customer.app.Util.UserBookService;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Goserwizz";

    //  table name
    private static final String TABLE_ADDVEHICLE = "addvehicle";
    private static final String TABLE_BOOKSERVICE = "bookservice";

    // vehicle Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOB = "mob";
    private static final String KEY_VTYPE = "vtype";
    private static final String KEY_VMAKE = "vmake";
    private static final String KEY_VMODEL = "vmodel";
    private static final String KEY_VREGNO = "vregno";
    private static final String KEY_DEALER_ID = "d_id";
    private static final String KEY_CITY = "city";
    private static final String KEY_DEALER_CITY = "d_city";
    private static final String KEY_VMAKE_ID = "vmake_id";
    private static final String KEY_VMODEL_ID = "vmodel_id";
    private static final String KEY_MILEAGE = "mileage";
    private static final String KEY_slotDate = "slotDate";
    private static final String KEY_dealerPerson = "dealerPerson";
    private static final String KEY_addr = "addr";
    private static final String KEY_dealerLocation = "dealerLocation";
    private static final String KEY_dealerState = "dealerState";
    private static final String KEY_bookedDate = "bookedDate";
    private static final String KEY_paystatus = "paystatus";
    private static final String KEY_name = "name";
    private static final String KEY_dealerCountry = "dealerCountry";
    private static final String KEY_dealerMob = "dealerMob";
    private static final String KEY_vehicleNo = "vehicleNo";
    private static final String KEY_mileage = "mileage";
    private static final String KEY_paymode = "paymode";
    private static final String KEY_pickup = "pickup";
    private static final String KEY_mob = "mob";
    private static final String KEY_status = "status";
    private static final String KEY_dealerPincode = "dealerPincode";
    private static final String KEY_dealerCompName = "dealerCompName";
    private static final String KEY_serviceType = "serviceType";
    private static final String KEY_modelName = "modelName";
    private static final String KEY_serviceId = "serviceId";
    private static final String KEY_email = "email";
    private static final String KEY_dealerCity = "dealerCity";
    private static final String KEY_dealerEmail = "dealerEmail";
    private static final String KEY_cmts = "cmts";
    private static final String KEY_dealerAddr = "dealerAddr";
    private static final String KEY_slotTime = "slotTime";
    private static final String KEY_advance = "advance";


    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ADDVEHICLE_TABLE = "CREATE TABLE " + TABLE_ADDVEHICLE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_VTYPE + " TEXT," + KEY_VMAKE + " TEXT," + KEY_VMODEL + " TEXT,"
                + KEY_VREGNO + " TEXT," + KEY_NAME + " TEXT," + KEY_EMAIL + " TEXT," + KEY_MOB + " TEXT," + KEY_CITY + " TEXT," + KEY_VMAKE_ID + " TEXT," + KEY_VMODEL_ID + " TEXT,"
                + KEY_MILEAGE + " TEXT" + ")";
        db.execSQL(CREATE_ADDVEHICLE_TABLE);

        String CREATE_BOOKSERVICE_TABLE = "CREATE TABLE " + TABLE_BOOKSERVICE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_slotDate + " TEXT," + KEY_dealerPerson + " TEXT," + KEY_addr + " TEXT,"
                + KEY_dealerLocation + " TEXT," + KEY_dealerState + " TEXT," + KEY_bookedDate + " TEXT," + KEY_paystatus + " TEXT," + KEY_name + " TEXT,"
                + KEY_dealerCountry + " TEXT," + KEY_dealerMob + " TEXT," + KEY_vehicleNo + " TEXT," + KEY_mileage + " TEXT," + KEY_paymode + " TEXT,"
                + KEY_pickup + " TEXT," + KEY_mob + " TEXT," + KEY_status + " TEXT," + KEY_dealerPincode + " TEXT," + KEY_dealerCompName + " TEXT,"
                + KEY_serviceType + " TEXT," + KEY_modelName + " TEXT," + KEY_serviceId + " TEXT," + KEY_email + " TEXT," + KEY_dealerCity + " TEXT,"
                + KEY_dealerEmail + " TEXT," + KEY_cmts + " TEXT," + KEY_dealerAddr + " TEXT," + KEY_slotTime + " TEXT," + KEY_advance + " TEXT,"
                + ")";
        db.execSQL(CREATE_BOOKSERVICE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDVEHICLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKSERVICE);
        // Creating tables again
        onCreate(db);
    }

    // Adding new vehicles
    public void addVehicle(UserAddvehicle dealer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VTYPE, dealer.getVtype());
        values.put(KEY_VMAKE, dealer.getVmake());
        values.put(KEY_VMODEL, dealer.getVmodel());
        values.put(KEY_VREGNO, dealer.getVregno());
        values.put(KEY_NAME, dealer.getName());
        values.put(KEY_EMAIL, dealer.getEmail());
        values.put(KEY_MOB, dealer.getMobile());
        values.put(KEY_CITY, dealer.getCity());
        values.put(KEY_VMAKE_ID, dealer.getVmakeId());
        values.put(KEY_VMODEL_ID, dealer.getVmodelId());
        values.put(KEY_MILEAGE, dealer.getMileage());

        // Inserting Row
        db.insert(TABLE_ADDVEHICLE, null, values);
        db.close(); // Closing database connection
    }

    // Getting one shop
    public UserAddvehicle getVehicle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ADDVEHICLE, new String[]{KEY_ID,
                        KEY_VMAKE_ID, KEY_VMODEL_ID}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        UserAddvehicle Obj = new UserAddvehicle(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return shop
        return Obj;
    }

    // Getting All vehicles
    public List<UserAddvehicle> getAllVehicles() {
        List<UserAddvehicle> VehicleList = new ArrayList<UserAddvehicle>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserAddvehicle Obj = new UserAddvehicle();
                Obj.setId(Integer.parseInt(cursor.getString(0)));
                Obj.setVtype(cursor.getString(1));
                Obj.setVmake(cursor.getString(2));
                Obj.setVmodel(cursor.getString(3));
                Obj.setVregno(cursor.getString(4));
                Obj.setName(cursor.getString(5));
                Obj.setEmail(cursor.getString(6));
                Obj.setMobile(cursor.getString(7));
                Obj.setCity(cursor.getString(8));
                Obj.setVmakeId(cursor.getString(9));
                Obj.setVmodelId(cursor.getString(10));
                Obj.setMileage(cursor.getString(11));
                // Adding contact to list
                VehicleList.add(Obj);
            } while (cursor.moveToNext());
        }

        // return VehicleList
        return VehicleList;
    }

    // Getting All vehicles
    public List<UserAddvehicle> getVehiclesByType(String vtype) {
        List<UserAddvehicle> VehicleList = new ArrayList<UserAddvehicle>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE + " WHERE " + KEY_VTYPE + "=" + "'" + vtype + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserAddvehicle Obj = new UserAddvehicle();
                Obj.setId(Integer.parseInt(cursor.getString(0)));
                Obj.setVtype(cursor.getString(1));
                Obj.setVmake(cursor.getString(2));
                Obj.setVmodel(cursor.getString(3));
                Obj.setVregno(cursor.getString(4));
                Obj.setName(cursor.getString(5));
                Obj.setEmail(cursor.getString(6));
                Obj.setMobile(cursor.getString(7));
                Obj.setCity(cursor.getString(8));
                Obj.setVmakeId(cursor.getString(9));
                Obj.setVmodelId(cursor.getString(10));
                Obj.setMileage(cursor.getString(11));
                // Adding contact to list
                VehicleList.add(Obj);
            } while (cursor.moveToNext());
        }

        // return VehicleList
        return VehicleList;
    }

    // Getting Vehicle Count
    public String getVehicleModelID(String regno) {
        String selectQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE + " WHERE " + KEY_VREGNO + "=" + "'" + regno + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        String vmodel = cursor.getString(10);
        return vmodel;
    }

    // Getting Vehicle Count
    public String getVehicleMileage(String regno) {
        String selectQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE + " WHERE " + KEY_VREGNO + "=" + "'" + regno + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        String mileage = cursor.getString(11);
        return mileage;
    }

    // Getting Vehicle Count
    public String getVehicleModel(String regno) {
        String selectQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE + " WHERE " + KEY_VREGNO + "=" + "'" + regno + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null)
            cursor.moveToFirst();

        String mileage = cursor.getString(3);
        return mileage;
    }

    // Getting Vehicle Count
    public int getVehicleCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ADDVEHICLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Updating a Vehicle
    public int updateVehicle(UserAddvehicle obj) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VMAKE, obj.getVmake());
        values.put(KEY_VMODEL, obj.getVmodel());

        // updating row
        return db.update(TABLE_ADDVEHICLE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(obj.getId())});
    }

    // Deleting a Vehicle
    public void deleteVehicle(UserAddvehicle Obj) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ADDVEHICLE, KEY_ID + " = ?",
                new String[] { String.valueOf(Obj.getId()) });
        db.close();


    }

    // Adding new vehicles
    public void bookservice(UserBookService customer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_serviceId, customer.getserviceId());
       /* values.put(KEY_VMAKE, dealer.getVmake());
        values.put(KEY_VMODEL, dealer.getVmodel());
        values.put(KEY_VREGNO, dealer.getVregno());
        values.put(KEY_NAME, dealer.getName());
        values.put(KEY_EMAIL, dealer.getEmail());
        values.put(KEY_MOB, dealer.getMobile());
        values.put(KEY_CITY, dealer.getCity());
        values.put(KEY_VMAKE_ID, dealer.getVmakeId());
        values.put(KEY_VMODEL_ID, dealer.getVmodelId());
        values.put(KEY_MILEAGE, dealer.getMileage());
*/
        // Inserting Row
        db.insert(TABLE_ADDVEHICLE, null, values);
        db.close(); // Closing database connection
    }


    // Getting All vehicles
    public List<UserBookService> getAllBookServices() {
        List<UserBookService> BookServiceList = new ArrayList<UserBookService>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKSERVICE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UserBookService Obj2 = new UserBookService();
                Obj2.setId(Integer.parseInt(cursor.getString(0)));
                Obj2.setslotDate(cursor.getString(1));
             /*   Obj.setVtype(cursor.getString(1));
                Obj.setVmake(cursor.getString(2));
                Obj.setVmodel(cursor.getString(3));
                Obj.setVregno(cursor.getString(4));
                Obj.setName(cursor.getString(5));
                Obj.setEmail(cursor.getString(6));
                Obj.setMobile(cursor.getString(7));
                Obj.setCity(cursor.getString(8));
                Obj.setVmakeId(cursor.getString(9));
                Obj.setVmodelId(cursor.getString(10));
                Obj.setMileage(cursor.getString(11));*/
                // Adding contact to list
                BookServiceList.add(Obj2);
            } while (cursor.moveToNext());
        }

        // return VehicleList
        return BookServiceList;
    }

}

