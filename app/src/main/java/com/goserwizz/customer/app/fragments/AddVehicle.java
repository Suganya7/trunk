package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.goserwizz.customer.app.Database.DBHandler;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.UserAddvehicle;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/8/2016.
 */
public class AddVehicle extends Fragment {

    String[] cars = { "Select", "Hyundai Eon",
            "Hyundai I10", "Hyundai I20", "Hyundai Grand I10","Hyundai Xcent" };

    String[] vehiclenames = { "Select", "Hyundai Eon",
            "Hyundai I10", "Hyundai I20", "Hyundai Grand I10","Hyundai Xcent" };
    String[] makesitems = { "Make", "Eon",
            "I10", "I20", "Grand I10","Xcent" };
    String[] modelitems = { "Model", "Eon",
            "I10", "I20", "Grand I10","Xcent" };
    String[] yearitems = { "Year", "2016",
            "2015", "2014", "2013"};

    String[] bikes = { "Select", "Yamaha FZ V 2.0",
            "Yamaha YZF-R3", "Yamaha Fazer FI V 2.0", "Yamaha MT-09","Yamaha Vmax" };

    String[] makesBikeitems = { "Make", "FZ V 2.0",
            "YZF-R3", "FI V 2.0", "MT-09","Vmax" };
    String[] modelBikeitems = { "Model", "FZ V 2.0",
            "YZF-R3", "FI V 2.0", "MT-09","Vmax" };


    LinearLayout vehilayA1, vehilayA2, vehilayA3;
    Switch carswitch, bikeswitch;
    Spinner spinner, vehicle, make, model, year;
    View viewSp0, viewSp1, viewSp2;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://3vikram.in/bookServ.do";
    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_MODE = "mode";
    private static final String TAG_STERM = "sterm", TAG_CITY ="city";
    private static final String TAG_VTYPE = "vtype", TAG_VMODEL = "vmodel";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";
    private static final String TAG_VMAKE = "vmake";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";

    String scarbikeSwitch;
    String status, digest, resultmsg;
    String sTermText, vmake_id, vmake_name, vmodel_id, vmodel_name, dealer_id, dealer_comp_name, dealer_lat, dealer_long;
    List<String> sTermCity, sVMakeName, sVMakeId, sVModelId, sVModelName, sDealer_ID, sDealer_Name , sDealer_City, sDealer_lati, sDealer_long;
    ArrayAdapter sTermCityAdapter;
    String[] cityArr, VMakeName= {"Make"}, VMakeId, VModelId, VModelName = {"Model"}, Dealer_ID, Dealer_Name, Dealer_City, Dealer_lati, Dealer_long;
    DBHandler db;
    EditText AddVehicle_Regno, AddVehicle_Mileage;
    String vregno, vmileage, d_id, d_city, vmake, vmodel, vmakeid, vmodelid, vtype, city, name, email, mob;
    Button addVehicleBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_vehicle,container,false);

        SettingsUtils.init(getActivity());

        db = new DBHandler(getActivity());


        carswitch = (Switch)v.findViewById(R.id.CarswitchADD);
        bikeswitch = (Switch)v.findViewById(R.id.BikeswitchADD);
        vehilayA1 = (LinearLayout)v.findViewById(R.id.vehilayA1);
       // vehilayA2 = (LinearLayout)v.findViewById(R.id.vehilayA2);
        vehilayA3 = (LinearLayout)v.findViewById(R.id.vehilayA3);
       // vehicle = (Spinner)v.findViewById(R.id.AddspinnerVehicle);
        make = (Spinner)v.findViewById(R.id.AddspinnerMake);
        model = (Spinner)v.findViewById(R.id.AddspinnerModel);
        AddVehicle_Regno = (EditText)v.findViewById(R.id.AddVehicle_Regno);
        AddVehicle_Mileage = (EditText)v.findViewById(R.id.AddVehicle_Mileage);
        addVehicleBtn = (Button)v.findViewById(R.id.addVehicleBtn);
     //   year = (Spinner)v.findViewById(R.id.AddspinnerYear);
      //  viewSp0 = (View)v.findViewById(R.id.View0);
      //  viewSp1 = (View)v.findViewById(R.id.View1);
      //  viewSp2 = (View)v.findViewById(R.id.View2);

        sVMakeId = new ArrayList<String>();
        sVMakeName = new ArrayList<String>();
        sVModelId = new ArrayList<String>();
        sVModelName = new ArrayList<String>();
        sDealer_ID = new ArrayList<String>();
        sDealer_Name = new ArrayList<String>();
        sDealer_City = new ArrayList<String>();

        sDealer_lati = new ArrayList<String>();
        sDealer_long = new ArrayList<String>();

        sVMakeId.add("0");
        sVMakeName.add("Make");
        sVModelId.add("0");
        sVModelName.add("Model");


        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));
        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));


        bikeswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    carswitch.setChecked(false);
                    vehilayA1.setVisibility(View.VISIBLE);
                    scarbikeSwitch = "Bike";
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                    UserSwitchVehicle();

                } else {

                    carswitch.setChecked(true);
                    vehilayA1.setVisibility(View.VISIBLE);
                    scarbikeSwitch = "Car";
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                }

            }
        });

        carswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    bikeswitch.setChecked(false);
                    vehilayA1.setVisibility(View.VISIBLE);
                    scarbikeSwitch="Car";
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                    UserSwitchVehicle();


                } else {

                    bikeswitch.setChecked(true);
                    scarbikeSwitch="Bike";
                    vehilayA1.setVisibility(View.VISIBLE);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);


                }

            }
        });

        make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                System.out.println("it works...   " + item);

                if (item.equals("Make")) {

                    System.out.println("it works...   " + item);
                } else {
                    System.out.println("it works...   " + item);

                 //   model.notify();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKEid, sVMakeId.get(position));
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKE, sVMakeName.get(position));

                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetVehicleModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                System.out.println("it works...   " + item);

                if (item.equals("Model")) {
                    System.out.println("it works...   " + item);
                } else {
                    System.out.println("it works...   " + item);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELid, sVModelId.get(position));
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODEL, sVModelName.get(position));

                    String model = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODEL, "");

                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetDealerByModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        addVehicleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserAddVehicle();
            }
        });

        return v;
    }

 /* Switch the vehicle type functionality */

    public void UserSwitchVehicle() {
        sVMakeId.clear();
        sVMakeName.clear();
        sVModelId.clear();
        sVModelName.clear();
        sVMakeId.add("0");
        sVMakeName.add("Make");
        sVModelId.add("0");
        sVModelName.add("Model");


        VMakeName = new String[sVMakeName.size()];
        VMakeName = sVMakeName.toArray(VMakeName);

        VModelName = new String[sVModelName.size()];
        VModelName = sVModelName.toArray(VModelName);

        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));
        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));

        if (Connectivity.isConnected(getActivity())) {

            new JsonGetVehicleMake().execute();

        } else {
            Connectivity.showNoConnectionDialog(getActivity());
        }
    }

    /* Save the addvehicle details in sqlite db */

    public void UserAddVehicle() {

        String regno = AddVehicle_Regno.getText().toString();
        String mileage = AddVehicle_Mileage.getText().toString();

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEHADD_REGNO, regno);
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEHADD_MILEAGE, mileage);

        vregno = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEHADD_REGNO, "");
        vmileage = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VEHADD_MILEAGE, "");
        vtype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
        vmakeid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
        vmake = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKE, "");
        vmodelid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");
        vmodel = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODEL, "");
        d_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
        city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");
        name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");

        // Inserting UserAddvehicle/Rows
        Log.d("Insert: ", "Inserting ..");
        db.addVehicle(new UserAddvehicle(vtype, vmake, vmodel, vregno, vmileage, name, email, mob, city, vmake_id, vmodel_id));

        // Reading all shops
        Log.d("Reading: ", "Reading all vehicles..");
        List<UserAddvehicle> vehicle = db.getAllVehicles();

        for (UserAddvehicle vehi : vehicle) {
            String log = "Id: " + vehi.getId() + " ,Name: " + vehi.getVmake()+ " ,model: " + vehi.getVmodel() + " , name: email and mob " + vehi.getName() +" , "+vehi.getEmail()+  " , " + vehi.getMobile();
            // Writing shops  to log
            Log.d("UserAddvehicle: : ", log);
            Log.d("all item:", vehicle+ "");
        }

        showResultDialog("Vehicles Added Successfully");

    }

    // Adapter class for spinner control
    public class MyAdapterMake extends ArrayAdapter<String> {

        public MyAdapterMake(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(VMakeName[position]);
            }
            else {
                label.setText(VMakeName[position]);
            }

            return row;
        }
    }

    // Adapter class for spinner control
    public class MyAdapterModel extends ArrayAdapter<String> {

        public MyAdapterModel(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(VModelName[position]);

            }
            else {
                label.setText(VModelName[position]);
            }

            return row;
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetVehicleMake extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, scarbikeSwitch));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, city));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getvehiclemake"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sVMakeId.clear();
                    sVMakeName.clear();

                    sVMakeId.add("0");
                    sVMakeName.add("Make");

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    vmake_id = tmpObj.get("id").toString();
                                    vmake_name = tmpObj.get("name").toString();

                                    sVMakeId.add(vmake_id);
                                    sVMakeName.add(vmake_name);

                                    System.out.println(tmpObj.get("id"));
                                    System.out.println(tmpObj.get("name"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(result.contains("Communications link failure")) {

                        new JsonGetVehicleMake().execute();
                    }
                    else {
                        VMakeName = new String[sVMakeName.size()];
                        VMakeName = sVMakeName.toArray(VMakeName);

                        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));
                    }

                }
                else if(result.equals("fail")) {

                    if(result.contains("Communications link failure")) {

                        new JsonGetVehicleMake().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetVehicleModel extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            String vmake = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
            String city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VMAKE, vmake));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, city));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getvmodel"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sVModelId.clear();
                    sVModelName.clear();

                    sVModelId.add("0");
                    sVModelName.add("Model");
                    HashMap<String,String> map = new HashMap<String,String>();


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    String value = dataObj.getString(dataObj_key);
                                    map.put(dataObj_key,value);

                                    vmodel_id = dataObj_key;
                                    vmodel_name = value;

                                    sVModelName.add(vmodel_name);
                                    sVModelId.add(vmodel_id);
                                    System.out.println(value);
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(result.contains("Communications link failure")) {

                        new JsonGetVehicleModel().execute();
                    }
                    else {

                        VModelName = new String[sVModelName.size()];
                        VModelName = sVModelName.toArray(VModelName);
                        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));
                    }
                }
                else if(result.equals("fail")) {

                    if(result.contains("Communications link failure")) {

                        new JsonGetVehicleModel().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetDealerByModel extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            String vtype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
            String city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");
            String vmodel = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, vtype));
            nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, city));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getdealerbyvmodel"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sDealer_ID.clear();
                    sDealer_Name.clear();
                    sDealer_lati.clear();
                    sDealer_long.clear();

                    sDealer_ID.add("0");
                    sDealer_Name.add("Select service center");
                    sDealer_lati.add("0");
                    sDealer_long.add("0");

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    dealer_comp_name = tmpObj.get("compName").toString();
                                    dealer_id = tmpObj.get("d_id").toString();
                                    dealer_lat = tmpObj.get("lat").toString();
                                    dealer_long = tmpObj.get("long").toString();
                                    String dcity = tmpObj.get("city").toString();

                                    sDealer_ID.add(dealer_id);
                                    sDealer_Name.add(dealer_comp_name);
                                    sDealer_lati.add(dealer_lat);
                                    sDealer_long.add(dealer_long);

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALERID, dealer_id);
                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_CITY, dcity);

                                      System.out.println("Dealer_ID ==>" + tmpObj.get("d_id"));
                                    // System.out.println(tmpObj.get("compName"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(result.contains("Communications link failure")) {
                        new JsonGetDealerByModel().execute();
                    }
                    else {
                        Dealer_Name = new String[sDealer_Name.size()];
                        Dealer_Name = sDealer_Name.toArray(Dealer_Name);

                        //   spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));
                    }
                }
                else if(result.equals("fail")) {

                    if(result.contains("Communications link failure")) {
                        new JsonGetDealerByModel().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }




    private void showResultDialogN(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Book My Services");

                            BookMyServices fragment = new BookMyServices();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.commit();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


}
