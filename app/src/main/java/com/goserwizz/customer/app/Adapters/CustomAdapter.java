package com.goserwizz.customer.app.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/23/2016.
 */
public class CustomAdapter extends ArrayAdapter<String> {

        private Context context;
        private String[] listOfValues;
        int selectedIndex = -1;
        int selectedpos = -1;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "s_date";
    private static final String TAG_STIME = "s_time";

    String status, resultmsg, dealer_id, s_date, s_time, r_date;
    String online_slot, slot_time, onlineslotavailable;
    String[] timeListArr;
    CustomAdapter customAdapter;
    Button saveTime;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;


        /** Constructor Class */
        public CustomAdapter(Context c, String[] values) {
            super(c,R.layout.radio_items,values);
            this.context = c;
            this.listOfValues = values;
        }

        public void setSelectedIndex(int index){
        selectedIndex = index;
    }
    public void setSelectedPosition(int index){
        selectedpos = index;
    }

        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            // Creating a view of row.
            View rowView = inflater.inflate(R.layout.radio_items, parent, false);
            final ViewHolder holder = new ViewHolder();

            holder.radioButton  = (RadioButton) rowView.findViewById(R.id.radioButton);

            String values = listOfValues[position];
            holder.radioButton.setText(values);

            String time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
            String timepos = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SELECTINDEX_TIME, "");
          /*  selectedpos = Integer.parseInt(timepos);

            if(selectedpos == position) {
                if (!timepos.isEmpty()) {
                    holder.radioButton.setChecked(true);

                }

            }  */

            if(selectedIndex == position){
                holder.radioButton.setChecked(true);

            }
            else{
                holder.radioButton.setChecked(false);
            }

            return rowView;
        }


    private static class ViewHolder {

        RadioButton radioButton;
    }



}

