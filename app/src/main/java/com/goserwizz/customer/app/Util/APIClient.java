package com.goserwizz.customer.app.Util;

import android.util.Log;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class APIClient {

    public static final String TAG = APIClient.class.getName();

    public static final String EMAIL = "email";
    public static final String PWD	= "pwd";
    public static final String URL = "http://goserwizz.com/custLogin.do?";
    public static final String MODE = "signin";
    private static final String TAG_MODE = "mode";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_SESSION = "session";
    private static final String TAG_FNAME = "fname";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static String status="", resultmsg, gu_id, session, fname;

    public static String testConnection(String email, String pwd, String mode) {
        Log.d(TAG, "testConnection() - " + email + ", " + pwd + ", " + mode);

        ServiceHandler sh = new ServiceHandler();
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("email", email));
        nameValuePairs.add(new BasicNameValuePair("pwd", pwd));
        nameValuePairs.add(new BasicNameValuePair("mode", "signin"));


        // Making a reequst to url and getting response
        String jsonStr = sh.makeServiceCall(URL, ServiceHandler.POST, nameValuePairs);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                status = jsonObj.getString(TAG_STATUS);

                if(status.equals("success")) {

                    resultmsg = jsonObj.getString(TAG_MSG);
                    JSONObject object = jsonObj.getJSONObject("data");
                    gu_id = object.getString(TAG_GUID);
                    session = object.getString(TAG_SESSION);
                    fname = object.getString(TAG_FNAME);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GU_ID, gu_id);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGINSESSION, session);

                }
                else if(status.equals("fail")) {

                    resultmsg = jsonObj.getString(TAG_MSG);

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }

        return resultmsg;

    }

    public static JSONObject getUserInfo(String url, String user, String key) {
        Log.d(TAG, "testConnection() - " + url + ", " + user + ", " + key);

        return null;
    }

}
