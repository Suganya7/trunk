package com.goserwizz.customer.app.Adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.R;

/**
 * Created by Kaptas on 3/14/2016.
 */
public class TrackListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] dateTime;
    private final String[] Model, regNO,Detail, Id, Mileage, Scenter;
    LinearLayout vehi_model_Layout, service_detail_Lay, service_id_Layout, Mileage_Layout, service_center_Lay;
    int visibility=View.VISIBLE;

    public TrackListAdapter(Activity context, String[] date, String[] model, String[] regno, String[] serviceDetail,
                             String[] serviceId, String[] mileage, String[] serviceCenter) {
        super(context, R.layout.list_items, regno);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.dateTime=date;
        this.Model=model;
        this.regNO=regno;
        this.Detail=serviceDetail;
        this.Id=serviceId;
        this.Mileage=mileage;
        this.Scenter=serviceCenter;
    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {

        TextView txt;
        LinearLayout model, detail, service_Id, center, mileage;
        ImageView history_downarrow, history_uparrow;

    }

    public View getView(final int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.track_items, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        TextView date = (TextView) rowView.findViewById(R.id.Item_datetime);
        TextView modell = (TextView) rowView.findViewById(R.id.vehiModel);
        TextView Regnumber = (TextView) rowView.findViewById(R.id.Item_regno);
        TextView paid_service = (TextView) rowView.findViewById(R.id.paid_service);

        ImageView history_downarrow = (ImageView) rowView.findViewById(R.id.history_downarrow);
      //  ImageView imageView = (ImageView) rowView.findViewById(R.id.Item_img);
        ImageView history_uparrow = (ImageView) rowView.findViewById(R.id.history_uparrow);

        viewHolder.txt = (TextView) rowView.findViewById(R.id.Item_datetime);
        viewHolder.history_downarrow = (ImageView) rowView.findViewById(R.id.history_downarrow);
        viewHolder.history_uparrow = (ImageView) rowView.findViewById(R.id.history_uparrow);
        viewHolder.model = (LinearLayout) rowView.findViewById(R.id.vehi_model_Layout);
        viewHolder.detail = (LinearLayout) rowView.findViewById(R.id.service_detail_Lay);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.Item_datetime, viewHolder.txt);
        rowView.setTag(R.id.history_downarrow, viewHolder.history_downarrow);
        rowView.setTag(R.id.history_uparrow, viewHolder.history_uparrow);
        rowView.setTag(R.id.vehi_model_Layout, viewHolder.model);
        rowView.setTag(R.id.service_detail_Lay, viewHolder.detail);

        date.setText(dateTime[position]);
    //    imageView.setImageResource(imgid[position]);
        modell.setText(Model[position]);
        Regnumber.setText(regNO[position]);
        paid_service.setText(Detail[position]);

        history_downarrow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Log.d("Arrow clicked==>", "" + position);

                viewHolder.model.setVisibility(View.VISIBLE);
                viewHolder.detail.setVisibility(View.VISIBLE);
                viewHolder.history_downarrow.setVisibility(View.GONE);


            }
        });

        history_uparrow.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Log.d("Arrow clicked==>", "" + position);

                viewHolder.model.setVisibility(View.GONE);
                viewHolder.detail.setVisibility(View.GONE);
                viewHolder.history_downarrow.setVisibility(View.VISIBLE);


            }
        });

        return rowView;

    };

}


