package com.goserwizz.customer.app.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.goserwizz.customer.app.R;

/**
 * Created by kaptas
 * * forked from: https://gist.github.com/YousufSohail/acf0dab84147e56bc2c5
 */
public class Connectivity {

    /**
     * Get the network info
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     * @param context
     * @return
     */
    public static boolean isConnected(Context context){
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static void showNoConnectionDialog(Context context) {
        final Context ctx = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(false);
        builder.setMessage(R.string.no_connection);
        builder.setTitle(R.string.no_connection_title);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                ctx.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                return;
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                return;
            }
        });
        builder.show();
    }

//    <string name="no_connection_title">No Internet Connection</string>
//    <string name="no_connection">You don\'t have internet connection. Please check your connection settings and try again.</string>
//    <string name="settings_button_text">Internet Settings</string>
//    <string name="cancel_button_text">Cancel</string>
}
