package com.goserwizz.customer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaptas on 5/4/2016.
 */
public class FAQfragment extends Fragment {

private Toolbar toolbar;
private TabLayout tabLayout;
private ViewPager viewPager;
private int[] tabIcons = {
        R.drawable.location1,
        R.drawable.settings,
};
PagerAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.faq_tabs, container, false);

        viewPager = (ViewPager) v.findViewById(R.id.viewpagerFAQ);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) v.findViewById(R.id.tabFAQ);
        tabLayout.setupWithViewPager(viewPager);
        // setupTabIcons();

        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.White));

// added newly
        SettingsUtils.init(getActivity());

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "general");

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        viewPager.getAdapter().notifyDataSetChanged();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                String tabselect =  tab.getText().toString();

                if(tabselect.equals("GENERAL")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "general");
                }
                else if(tabselect.equals("ACCOUNT RELATED")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "account");
                }
                else if(tabselect.equals("BOOKING RELATED")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "booking");
                }



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return v;

    }


        private void setupTabIcons() {
            tabLayout.getTabAt(0).setIcon(tabIcons[0]);
            tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        }

        private void setupViewPager(ViewPager viewPager) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
            adapter.addFrag(new faqGeneral(), "GENERAL");
            adapter.addFrag(new faqAccount(), "ACCOUNT RELATED");
            adapter.addFrag(new faqBooking(), "BOOKING RELATED");
            viewPager.setAdapter(adapter);

        }

class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}

}
