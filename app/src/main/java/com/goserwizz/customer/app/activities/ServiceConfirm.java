package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.Adapters.CustomAdapter;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.ServiceDeposit;
import com.goserwizz.customer.app.fragments.TimeFragment;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/16/2016.
 */
public class ServiceConfirm extends AppCompatActivity implements View.OnClickListener,  View.OnTouchListener {

    private Toolbar toolbar;
    SharedPreferences prefDate, DbleTapTime;
    String dateValue = "DATE", s_date = "Date", s_time = "Time";
    SharedPreferences.Editor edit;
    private ImageView mdateImage;
    private ImageView mtimeImage;
    private ViewPager mViewPager;
    private LinearLayout mdateLay;
    private LinearLayout mtimeLays;
    TextView date, time;
    View view1, view2;
    GestureDetector gestureDetector;
    ViewPagerAdapter adapter;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "s_date";
    private static final String TAG_STIME = "s_time";

    String status, resultmsg, dealer_id, r_date;
    String online_slot, slot_time, onlineslotavailable;
    String[] timeListArr;
    CustomAdapter customAdapter;
    Button saveTime;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedule_slot);

        SettingsUtils.init(this);
        s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");

        prefDate = getSharedPreferences("S_Date", MODE_PRIVATE);
        dateValue = prefDate.getString("date", null);

        DbleTapTime = getSharedPreferences("time", MODE_PRIVATE);
        edit = DbleTapTime.edit();

        toolbar = (Toolbar) findViewById(R.id.Rescheduletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Service Center");

        mdateImage = (ImageView) findViewById(R.id.date_img);
        mtimeImage = (ImageView) findViewById(R.id.time_img);
        date = (TextView) findViewById(R.id.dateText);
        time = (TextView) findViewById(R.id.timeText);

        //  date.setText("date");
        if(s_date.isEmpty() || s_date.equals("")) {
            date.setText("Date");
        }
        else {
            date.setText(s_date);
        }
        if(s_time.isEmpty() || s_time.equals("")) {
            time.setText("Time");
        }
        else {
            time.setText(s_time);
        }

        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);

        mdateLay = (LinearLayout) findViewById(R.id.date_lay1);
        mtimeLays = (LinearLayout) findViewById(R.id.time_lay1);

        mdateLay.setOnClickListener(this);
        mtimeLays.setOnClickListener(this);


        mdateLay.setOnTouchListener(
                new LinearLayout.OnTouchListener() {
                    public boolean onTouch(View v,
                                           MotionEvent event) {
                        if (gestureDetector.onTouchEvent(event)) {
                            Log.v("tag", "screen touched");

                            return true;
                        } else {

                            edit.putString("time", "date");
                            edit.commit();

                            return false;
                        }
                    }
                }
        );

        mtimeLays.setOnTouchListener(
                new LinearLayout.OnTouchListener() {
                    public boolean onTouch(View v,
                                           MotionEvent event) {
                        if (gestureDetector.onTouchEvent(event)) {
                            Log.v("tag", "screen touched");

                            return true;
                        } else {

                            edit.putString("time", "time");
                            edit.commit();

                            return false;
                        }
                    }
                }
        );

        gestureDetector = new GestureDetector(new MyGestureDetector());


        mdateImage.setBackgroundResource(R.drawable.date1);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));

       // adapter = new ViewPagerAdapter(getSupportFragmentManager());

        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                  //  getSupportActionBar().setTitle("Date");
                    mdateImage.setBackgroundResource(R.drawable.date1);
                    mtimeImage.setBackgroundResource(R.drawable.time1);
                    date.setTextColor(Color.WHITE);
                    time.setTextColor(Color.parseColor("#ff9999"));
                    view1.setBackgroundColor(Color.WHITE);
                    view2.setBackgroundColor(Color.parseColor("#cc0000"));

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }

                   // date.setText(s_date);
                   // time.setText(s_time);

                } else if (position == 1) {

                  //  getSupportActionBar().setTitle("Time");
                    mdateImage.setBackgroundResource(R.drawable.date2);
                    mtimeImage.setBackgroundResource(R.drawable.time2);
                    date.setTextColor(Color.parseColor("#ff9999"));
                    time.setTextColor(Color.WHITE);
                    view1.setBackgroundColor(Color.parseColor("#cc0000"));
                    view2.setBackgroundColor(Color.WHITE);

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }

                  //  date.setText(s_date);
                  //  time.setText(s_time);
                }

                mViewPager.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {

            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("viewpager:");
            switch (position) {
                case 0:
                    ServiceDeposit mFrag1 = new ServiceDeposit();
                    return mFrag1;

                case 1:
                    TimeFragment mFrag2 = new TimeFragment();
                    return mFrag2;

                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new Fragment();
                    return fragment;
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.date_lay1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.time_lay1:
                mViewPager.setCurrentItem(1);
                break;

        }
    }

    public class MyGestureDetector extends GestureDetector.SimpleOnGestureListener implements GestureDetector.OnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
           // Toast.makeText(getBaseContext(), "onDoubleTapEvent", Toast.LENGTH_LONG).show();
            startActivity(new Intent(ServiceConfirm.this, DateTime.class));
           // startActivityForResult(i, 1);


            return super.onDoubleTap(e);

        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            Log.v("tag", "screen touched");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // NavUtils.navigateUpFromSameTask(this);
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   /* public void onBackPressed() {

        startActivity(new Intent(ServiceConfirm.this, HomeActivity.class));
        finish();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {  //&& isNightModeToggled
            Intent a = new Intent(this,MainActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
*/



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetBookedSlots extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ServiceConfirm.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        /*    nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, "66"));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, "2016-04-18"));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIME, "13:00:00"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getbookedslot"));
            */

            String dateValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
            String timeValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
            String Dealer_id =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");


            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, Dealer_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, dateValue));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIME, timeValue));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getbookedslot"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                //   Iterator<?> dataObj_keys = dataObj.keys();

                                String booked_slot = dataObj.get("booked_slot").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKEDSLOTS, booked_slot);

                           /*     while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    String booked_slot = dataObj.get("booked_slot").toString();


                                } */

                                System.out.println(dataObj.get("booked_slot"));
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog

            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(ServiceConfirm.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(result.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new JsonGetBookedSlots().execute();
                    }
                    else {
                    /*    mJumpRunnable = new Runnable() {
                            public void run() {

                                if ((pDialog != null) && pDialog.isShowing()) {
                                    pDialog.dismiss(); }

                                String bookedslot = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_BOOKEDSLOTS, "");
                                Intent newIntent = new Intent(getActivity(), ServiceConfirm.class);
                                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                newIntent.putExtra("bookedslot", bookedslot);
                                startActivity(newIntent);
                            }
                        };
                        mHandler = new Handler();
                        mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);  */
                    }

                }
                else if(result.equals("fail")) {

                    if(result.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new JsonGetBookedSlots().execute();
                    }
                    else {

                        if ((pDialog != null) && pDialog.isShowing()) {
                            pDialog.dismiss(); }
                        showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                String bookslot=data.getStringExtra("bookedslot");

                Log.d("BOOKSLOT====>", bookslot);
            }
        }
    }


}
