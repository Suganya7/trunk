package com.goserwizz.customer.app.Util;

/**
 * Created by Kaptas on 4/19/2016.
 */
public class UserBookService {

    private int id;

    private String slotDate, dealerPerson, addr, dealerLocation, dealerState, bookedDate, paystatus, name, dealerCountry, dealerMob,
            vehicleNo, mileage, paymode, pickup, mob, status, dealerPincode, dealerCompName, serviceType, modelName, serviceId,
            email, dealerCity, dealerEmail, cmts, dealerAddr, slotTime, advance;


    public UserBookService()
    {
    }

    public UserBookService(int id, String modelname)
    {
        this.id=id;
        this.modelName=modelname;
    }

    public UserBookService(String name, String email, String modelname)
    {
        this.id=id;
        this.name = name;
        this.email = email;
        this.modelName=modelname;
    }


    public UserBookService(int id, String slotDate, String dealerPerson, String addr, String dealerLocation,
                           String dealerState, String bookedDate, String paystatus, String name, String dealerCountry, String dealerMob,
                           String vehicleNo, String mileage, String paymode, String pickup, String mob, String status,
                           String dealerPincode, String dealerCompName, String serviceType, String modelName, String serviceId,
                           String email, String dealerCity, String dealerEmail, String cmts, String dealerAddr, String slotTime,
                           String advance) {

        this.id=id;
        this.slotDate=slotDate;
        this.dealerPerson=dealerPerson;
        this.addr=addr;
        this.dealerLocation=dealerLocation;
        this.dealerState=dealerState;
        this.bookedDate=bookedDate;
        this.paystatus=paystatus;
        this.name=name;
        this.dealerCountry=dealerCountry;
        this.dealerMob=dealerMob;
        this.vehicleNo=vehicleNo;
        this.mileage=mileage;
        this.paymode=paymode;
        this.pickup=pickup;
        this.mob=mob;
        this.status=status;
        this.dealerPincode=dealerPincode;
        this.dealerCompName=dealerCompName;
        this.serviceType=serviceType;
        this.modelName=modelName;
        this.serviceId=serviceId;
        this.email=email;
        this.dealerCity=dealerCity;
        this.dealerEmail=dealerEmail;
        this.cmts=cmts;
        this.dealerAddr=dealerAddr;
        this.slotTime=slotTime;
        this.advance=advance;
    }

    public UserBookService(String slotDate, String dealerPerson, String addr, String dealerLocation,
                           String dealerState, String bookedDate, String paystatus, String name, String dealerCountry, String dealerMob,
                           String vehicleNo, String mileage, String paymode, String pickup, String mob, String status,
                           String dealerPincode, String dealerCompName, String serviceType, String modelName, String serviceId,
                           String email, String dealerCity, String dealerEmail, String cmts, String dealerAddr, String slotTime,
                           String advance)  {
        this.slotDate=slotDate;
        this.dealerPerson=dealerPerson;
        this.addr=addr;
        this.dealerLocation=dealerLocation;
        this.dealerState=dealerState;
        this.bookedDate=bookedDate;
        this.paystatus=paystatus;
        this.name=name;
        this.dealerCountry=dealerCountry;
        this.dealerMob=dealerMob;
        this.vehicleNo=vehicleNo;
        this.mileage=mileage;
        this.paymode=paymode;
        this.pickup=pickup;
        this.mob=mob;
        this.status=status;
        this.dealerPincode=dealerPincode;
        this.dealerCompName=dealerCompName;
        this.serviceType=serviceType;
        this.modelName=modelName;
        this.serviceId=serviceId;
        this.email=email;
        this.dealerCity=dealerCity;
        this.dealerEmail=dealerEmail;
        this.cmts=cmts;
        this.dealerAddr=dealerAddr;
        this.slotTime=slotTime;
        this.advance=advance;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setslotDate(String slotDate) {
        this.slotDate = slotDate;
    }

    public void setdealerPerson(String dealerPerson) { this.dealerPerson = dealerPerson;
    }

    public void setaddr(String addr) {  this.addr = addr;    }

    public void setdealerLocation(String dealerLocation) {  this.dealerLocation = dealerLocation;    }

    public void setdealerState(String dealerState) {  this.dealerState = dealerState;    }

    public void setbookedDate(String bookedDate) {  this.bookedDate = bookedDate;    }

    public void setpaystatus(String paystatus) {  this.paystatus = paystatus;    }

    public void setname(String name) {  this.name = name;    }

    public void setdealerCountry(String dealerCountry) {  this.dealerCountry = dealerCountry;    }

    public void setdealerMob(String dealerMob) {  this.dealerMob = dealerMob;    }

    public void setvehicleNo(String vehicleNo) {  this.vehicleNo = vehicleNo;    }

    public void setmileage(String mileage) {  this.mileage = mileage;    }

    public void setpaymode(String paymode) {  this.paymode = paymode;    }

    public void setpickup(String pickup) {
        this.pickup = pickup;
    }

    public void setmob(String mob) {  this.mob = mob;    }

    public void setstatus(String status) {  this.status = status;    }

    public void setdealerPincode(String dealerPincode) {  this.dealerPincode = dealerPincode;    }

    public void setdealerCompName(String dealerCompName) {  this.dealerCompName = dealerCompName;    }

    public void setserviceType(String serviceType) {  this.serviceType = serviceType;    }

    public void setmodelName(String modelName) {  this.modelName = modelName;    }

    public void setserviceId(String serviceId) {  this.serviceId = serviceId;    }

    public void setemail(String email) {  this.email = email;    }

    public void setdealerCity(String dealerCity) {  this.dealerCity = dealerCity;    }

    public void setdealerEmail(String dealerEmail) {  this.dealerEmail = dealerEmail;    }

    public void setcmts(String cmts) {  this.cmts = cmts;    }
    public void setdealerAddr(String dealerAddr) {  this.dealerAddr = dealerAddr;    }
    public void setslotTime(String slotTime) {  this.slotTime = slotTime;    }
    public void setadvance(String advance) {  this.advance = advance;    }



    public int getId() {

        return id;
    }

    public String getslotDate() {
        return slotDate;
    }

    public String getdealerPerson() { return dealerPerson;
    }

    public String getaddr() {  return addr;  }

    public String getdealerLocation() {  return dealerLocation;    }

    public String getdealerState() {  return dealerState;    }

    public String getbookedDate() {  return bookedDate;    }

    public String getpaystatus() {  return paystatus;    }

    public String getname() {  return name;    }

    public String getdealerCountry() {  return dealerCountry;    }

    public String getdealerMob() {  return dealerMob;    }

    public String getvehicleNo() {  return vehicleNo;    }

    public String getmileage() {  return mileage;    }

    public String getpaymode() {  return paymode;    }

    public String getpickup() {
        return pickup;
    }

    public String getmob() {  return mob;    }

    public String getstatus() {  return status;    }

    public String getdealerPincode() {  return dealerPincode;    }

    public String getdealerCompName() {  return dealerCompName;    }

    public String getserviceType() {  return serviceType;    }

    public String getmodelName() {  return modelName;    }

    public String getserviceId() {  return serviceId;    }

    public String getemail() {  return email;    }

    public String getdealerCity() {  return dealerCity;    }

    public String getdealerEmail() {  return dealerEmail;    }

    public String getcmts() {  return cmts;    }
    public String getdealerAddr() {  return dealerAddr;    }
    public String getslotTime() {  return slotTime;    }
    public String getadvance(){  return advance;    }


}
