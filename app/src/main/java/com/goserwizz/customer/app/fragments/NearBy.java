package com.goserwizz.customer.app.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.*;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.goserwizz.customer.app.MapJSONParser.PlaceJSONParser;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kaptas on 3/11/2016.
 */
public class NearBy extends Fragment implements LocationListener {

    GoogleMap mGoogleMap;
    Spinner mSprPlaceType;
/*
    String[] mPlaceType=null;
    String[] mPlaceTypeName=null;   */
    String mPlaceType=null;
    String mPlaceTypeName=null;

    private SupportMapFragment mSupportMapFragment;
    double mLatitude=0;
    double mLongitude=0;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    LocationListener locationListener;
    HashMap<String, String> mMarkerPlaceLink = new HashMap<String, String>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private View view;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;
    ArrayList<String> dealerlat, dealerlong, dealerName;
    String lat, lng, dealer_name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.location,container,false);



        String provider = android.provider.Settings.Secure.getString(getActivity().getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            getActivity().sendBroadcast(poke);
        }

        /***Needed for Marshmallow to check permission while at runtime
         ****
         ****/

        if (!checkPermission()) {

            //   requestPermission();
            mJumpRunnable = new Runnable() {
                public void run() {

                    getNearByServiceCenterPlaces();
                }
            };
            mHandler = new Handler();
            mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);


        } else {

            getNearByServiceCenterPlaces();

        }

        return v;
    }

    public void getNearByServiceCenterPlaces() {
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());


        if(status!= ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();

        }else { // Google Play Services are available

            dealerlat =  SettingsUtils.getInstance().getListString("dealer_latitude");
            dealerlong = SettingsUtils.getInstance().getListString("dealer_longitude");
            dealerName = SettingsUtils.getInstance().getListString("dealer_compName");

            // Getting reference to the SupportMapFragment
            //  SupportMapFragment fragment = ( SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapNearby);

            mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapNearby);
            if (mSupportMapFragment == null) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mSupportMapFragment = SupportMapFragment.newInstance();
                fragmentTransaction.replace(R.id.mapNearby, mSupportMapFragment).commit();
            }

            if (mSupportMapFragment != null) {
                mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        if (googleMap != null) {

                            if (Build.VERSION.SDK_INT >= 23 &&
                                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
                            }

                            locationListener = new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {

                                    mLatitude = location.getLatitude();
                                    mLongitude = location.getLongitude();
                                    LatLng point = new LatLng(mLatitude, mLongitude);
                                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                                }

                                @Override
                                public void onStatusChanged(String provider, int status, Bundle extras) {

                                }

                                @Override
                                public void onProviderEnabled(String provider) {

                                }

                                @Override
                                public void onProviderDisabled(String provider) {

                                }
                            };


                            // Getting Google Map
                            //  mGoogleMap = mSupportMapFragment.getMap();
                            mGoogleMap = googleMap;

                            // Enabling MyLocation in Google Map
                            mGoogleMap.setMyLocationEnabled(true);



                            // Getting LocationManager object from System Service LOCATION_SERVICE
                            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                            // Creating a criteria object to retrieve provider
                            Criteria criteria = new Criteria();

                            // Getting the name of the best provider
                            String provider1 = locationManager.getBestProvider(criteria, true);

                            // Getting Current Location From GPS
                            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                            if(location!=null){
                                onLocationChanged(location);
                            }

                            locationManager.requestLocationUpdates(provider1, 20000, 0, locationListener);

                            for(int i=1; i<dealerlat.size(); i++) {

                                lat = dealerlat.get(i);

                                // Getting the longitude of the i-th location
                                lng = dealerlong.get(i);

                                dealer_name = dealerName.get(i);

                                // Drawing marker on the map
                                drawMarker(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)), dealer_name);
                            }

                            // Moving CameraPosition to last clicked position
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));

                            // Setting the zoom level in the map on last position  is clicked
                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(9));

                        }

                    }
                });


            }
        }

    }

    /*****
     *
     *
     *  //     locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20000, 0, this);

     mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

    @Override
    public void onInfoWindowClick(Marker arg0) {
    Intent intent = new Intent(getActivity().getBaseContext(), PlaceDetailsActivity.class);
    String reference = mMarkerPlaceLink.get(arg0.getId());
    intent.putExtra("reference", reference);

    // Starting the Place Details Activity
    startActivity(intent);
    }
    });


    mPlaceType = "car_repair";
    //       int selectedPosition = mSprPlaceType.getSelectedItemPosition();
    String type = mPlaceType;


    StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
    sb.append("location="+mLatitude+","+mLongitude);
    sb.append("&radius=5000");
    sb.append("&types="+type);
    sb.append("&sensor=true");
    sb.append("&key=AIzaSyAtJguBxLY5s4QLkvr4lmdGj8UdbTQ_Va4");


    // Creating a new non-ui thread task to download Google place json data
    PlacesTask placesTask = new PlacesTask();

    // Invokes the "doInBackground()" method of the class PlaceTask
    placesTask.execute(sb.toString());

/*
                }
            });
     *
     * ****/

    private void drawMarker(LatLng latLng, String dealer_name){
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting the position for the marker
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(dealer_name);

        // Adding marker on the Google Map
        mGoogleMap.addMarker(markerOptions);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(9));
    }


    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpsURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpsURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }


    /** A class, to download Google Places */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result){
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }

    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){

            // Clears all the existing markers
            mGoogleMap.clear();

            for(int i=0;i<list.size();i++){

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Getting a place from the places list
                HashMap<String, String> hmPlace = list.get(i);

                // Getting latitude of the place
                double lat = Double.parseDouble(hmPlace.get("lat"));

                // Getting longitude of the place
                double lng = Double.parseDouble(hmPlace.get("lng"));

                // Getting name
                String name = hmPlace.get("place_name");

                // Getting vicinity
                String vicinity = hmPlace.get("vicinity");

                LatLng latLng = new LatLng(lat, lng);

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                //This will be displayed on taping the marker
                markerOptions.title(name + " : " + vicinity);

                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

                // Placing a marker on the touched position
                Marker m = mGoogleMap.addMarker(markerOptions);

                // Linking Marker id and place reference
                mMarkerPlaceLink.put(m.getId(), hmPlace.get("reference"));


            }

        }

    }



    @Override
    public void onLocationChanged(android.location.Location location) {
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        LatLng latLng = new LatLng(mLatitude, mLongitude);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }


    /* check Runtime permission for access fine location */
    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)){

            Toast.makeText(getActivity(), "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Toast.makeText(getActivity(), "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {

                            Toast.makeText(getActivity(), "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }

    /*
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "access fine and coarse location permission granted", Toast.LENGTH_SHORT).show();

                    Log.d("Permission granted ===>" ,"yes");
                    // permission was granted, yay! Do the task you need to do.

                } else {
                    Log.d("Permission granted ===>" ,"no");
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    } */
}