package com.goserwizz.customer.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.APIClient;
import com.goserwizz.customer.app.Util.Connectivity;


public class BaseLoginTest extends Fragment {
    Activity that;

    protected String sEmail;
    protected String sPwd;
    protected String sMode;

    private ProgressDialog mProgressDialog;

    public interface ConnectionTestListener {
        void onConnectionChecked(String result);
    }

    protected void testConnection(final boolean isShared, final ConnectionTestListener listener) {
        that = getActivity();
        if (Connectivity.isConnected(getActivity())) {
            new AsyncTask<Void, Void, String>() {

                @Override
                protected void onPreExecute() {
                    mProgressDialog = new ProgressDialog(that);
                    mProgressDialog.setMessage("Logging in...");
                    mProgressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {

                    String result = APIClient.testConnection(sEmail, sPwd, sMode);
                    return result;
                }

                @Override
                protected void onPostExecute(String result) {
                    mProgressDialog.dismiss();
                    showResultDialog(result, listener);
                }
            }.execute();
        } else {
            showResultAlertfail("Failed", listener);
        }
    }

    private void showResultDialog(final String result, final ConnectionTestListener listener) {
        if (that != null && !getActivity().isFinishing()) {

            if(result.contains("Communications link failure")) {

                testConnection(false, listener);
            }
            else {

                String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {
                if (result.contains("Successfully login")) {

                    listener.onConnectionChecked(result);
                   // showResultAlert(result, listener);

                } else {

                    if (result.contains("Communications link failure")) {
                        testConnection(false, listener);
                    } else {

                        showResultAlertfail(result, listener);
                    }
                }

            }
                else {

                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.dialog_title_connection_test))
                        .setMessage(getString(R.string.dialog_system_connection_test) + (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail)) + "\n" + "\n" + " Please try again")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                listener.onConnectionChecked(result);


                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .setCancelable(false)
                        .show();

                }

/*                        new AlertDialog.Builder(getActivity())
                                .setTitle(getString(R.string.dialog_title_connection_test))
                                .setMessage(getString(R.string.dialog_system_connection_test) + (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail)) + "\n" + "\n" + result)
                                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        listener.onConnectionChecked(result);


                                    }
                                })
                                .setNegativeButton("Cancel", null)
                                .show();
*/

                    }

        }
    }

    public void showResultAlert(final String result, final ConnectionTestListener listener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(result)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onConnectionChecked(result);



                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    public void showResultAlertfail(final String result, final ConnectionTestListener listener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(result)
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onConnectionChecked(result);


                    }
                })
                .setCancelable(false)
                .show();
    }
}
