package com.goserwizz.customer.app.Util;

/**
 * Created by Bilal-PC on 010.10 Sep,15.
 */
public class UserAddvehicle {

    private int id;

    private String vtype, vmake, vmodel, vehicle_regno, vmake_id, vmodel_id, d_id, mileage, d_city, city;
    private String name, email, mob;

    public UserAddvehicle()
    {
    }

    public UserAddvehicle(int id, String vmake_id, String vmodel_id)
    {
        this.id=id;
        this.vmake_id=vmake_id;
        this.vmodel_id=vmodel_id;
    }

    public UserAddvehicle(String name, String email, String vmake_id)
    {
        this.id=id;
        this.name = name;
        this.email = email;
        this.vmake_id=vmake_id;
    }

    public UserAddvehicle(int id, String vtype, String vmake, String vmodel, String vehicle_regno, String mileage, String name, String email, String mob, String city, String vmake_id, String vmodel_id)
    {
        this.id=id;
        this.vtype=vtype;
        this.vmake=vmake;
        this.vmodel=vmodel;
        this.vehicle_regno=vehicle_regno;
        this.mileage=mileage;
        this.name=name;
        this.email=email;
        this.mob=mob;
        this.city=city;
        this.vmake_id=vmake_id;
        this.vmodel_id=vmodel_id;
    }

    public UserAddvehicle(String vtype, String vmake, String vmodel, String vehicle_regno, String mileage, String name, String email, String mob, String city, String vmake_id, String vmodel_id)
    {
        this.vtype=vtype;
        this.vmake=vmake;
        this.vmodel=vmodel;
        this.vehicle_regno=vehicle_regno;
        this.mileage=mileage;
        this.name=name;
        this.email=email;
        this.mob=mob;
        this.city=city;
        this.vmake_id=vmake_id;
        this.vmodel_id=vmodel_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }

    public void setVmake(String vmake) {
        this.vmake = vmake;
    }

    public void setVmodel(String vmodel) {  this.vmodel = vmodel;    }

    public void setVregno(String vehicle_regno) {  this.vehicle_regno = vehicle_regno;    }

    public void setMileage(String mileage) {  this.mileage = mileage;    }

    public void setName(String Name) {  this.name = Name;    }

    public void setEmail(String Email) {  this.email = Email;    }

    public void setMobile(String Mobile) {  this.mob = Mobile;    }

    public void setCity(String City) {  this.city = City;    }

    public void setDealerId(String d_id) {  this.d_id = d_id;    }

    public void setDealerCity(String d_city) {  this.d_city = d_city;    }

    public void setVmakeId(String vmake_id) {  this.vmake_id = vmake_id;    }

    public void setVmodelId(String vmodel_id) {  this.vmodel_id = vmodel_id;    }


    public int getId() {

        return id;
    }

    public String getVmake() {
        return vmake;
    }

    public String getVtype() {
        return vtype;
    }

    public String getVmodel() {  return vmodel;    }

    public String getVregno() {  return vehicle_regno;    }

    public String getMileage() {  return mileage;    }

    public String getName() {  return name;    }

    public String getEmail() {  return email;    }

    public String getMobile() {  return mob;    }

    public String getDealerId() {  return d_id;    }

    public String getDealerCity() {  return d_city;    }

    public String getCity() {  return city;    }

    public String getVmakeId() {  return vmake_id;    }

    public String getVmodelId() {  return vmodel_id;    }

}
