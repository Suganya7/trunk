package com.goserwizz.customer.app.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.goserwizz.customer.app.Adapters.CustomGridViewAdapter;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.Views.ExpandableHeightGridView;
import com.goserwizz.customer.app.activities.ServiceConfirm;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by Kaptas on 3/8/2016.
 */

public class Amenities extends Fragment {

    ExpandableHeightGridView gview;
    LinearLayout gridLayout;
    ToggleButton ShowOrHide;
    Button checkAvailable;

    public static String [] gridNameList={"Pickup & Drop","24/7 Helpline","Wifi","Customer Lounge",
                                          "Driver Lounge","Customer Drop","Sunday Working",
                                          "Road Side Assistance","Emergency Service", "Mobile/Doorstep service",
                                          "Quick service Express service", "Tyre & Battery Replacement", "Wheel Alignment & Balancing",
                                          "Extended Warranty", "Genuine car accessories", "3M Car Care"}; //"Used Car/Exchange cars/Trade in", "Cashless Insurance", "24/7 service operations", "Insurance Renewals", "AMC", "Paint Booth", "Evening Shift"
    public static int [] gridImages={R.drawable.vehi_pickup,R.drawable.quickservice,R.drawable.wifi,
                                     R.drawable.paintbooth,R.drawable.usedcar,R.drawable.ins_renewal,
                                     R.drawable.amen3m,R.drawable.tyrebattery,R.drawable.road_assistance,
                                     R.drawable.extended_warranty,R.drawable.tyrebattery,
                                     R.drawable.sun_working,R.drawable.emergencey_ser,R.drawable.amen24x7vehicle,
                                     R.drawable.wheel_align, R.drawable.tyrebattery
                                    }; // R.drawable.amen3m,R.drawable.usedcar,R.drawable.cashless, R.drawable.amen24x7vehicle,R.drawable.ins_renewal,R.drawable.amc,   R.drawable.paintbooth,R.drawable.eveningshift

    public static String [] dealerAmenities = {"1", "2", "3", "4", "5"};

    public static int [] dealerAmenities_Images={R.drawable.vehi_pickup_d,R.drawable.quickservice_d,R.drawable.wifi_d,
            R.drawable.paintbooth_d,R.drawable.usedcar_d,R.drawable.ins_renewal_d,
            R.drawable.amen3m_d,R.drawable.tyrebattery_d,R.drawable.road_assistance_d,
            R.drawable.extended_warranty_d,R.drawable.tyrebattery_d,
            R.drawable.sun_working_d,R.drawable.emergencey_ser_d,R.drawable.amen24x7vehicle_d,
            R.drawable.wheel_align_d, R.drawable.tyrebattery_d};


    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "s_date";

    String status, resultmsg, dealer_id, s_date, s_time;
    String id, icon, type, dealer_amenities;
    String[] dealerAmenitiesArr, dealerAmenitiesIdArr;
    ArrayList<String> dealerAmentiesList, dealerAmenitiesId, amenitiesImgLink;
    HashSet<String> dealerAmenitiesNameSet = new HashSet<String>();
    TreeSet<String> tset;
    TextView TextCenterModel, TextCenterAddress;
    String dealerName, dealerAddress;
   // String[] dealerAmenties = {};
   public static String [] amemitiesImageLink={ };
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;
    private static final int PERMISSION_WExternalStorage_REQUEST_CODE = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.amenties,container,false);

        SettingsUtils.init(getActivity());

        gridLayout = (LinearLayout)v.findViewById(R.id.gridViewLay);
        ShowOrHide = (ToggleButton)v.findViewById(R.id.BtnShowHide);
        checkAvailable = (Button)v.findViewById(R.id.checkAvailable);
        TextCenterModel = (TextView)v.findViewById(R.id.TextCenterModel);
        TextCenterAddress = (TextView)v.findViewById(R.id.TextAddress1);

        if (!checkWriteExternalStoragePermission()) {

            //   requestWriteExternalStoragePermission();
            mJumpRunnable = new Runnable() {
                public void run() {

                  /*
                  Load data after runtime permission
                   */
                    dealerName = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
                    dealerAddress = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_ADDR, "");

                    TextCenterModel.setText(dealerName);
                    TextCenterAddress.setText(dealerAddress);

                    dealerAmentiesList = new ArrayList<>();
                    dealerAmenitiesId = new ArrayList<>();
                    amenitiesImgLink = new ArrayList<>();

                    dealerAmentiesList.clear();
                    dealerAmenitiesId.clear();
                    amenitiesImgLink.clear();

                    gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 350));

                  //  gview = (GridView)v.findViewById(R.id.gridView);
                    gview = (ExpandableHeightGridView)v.findViewById(R.id.gridView);

                    // "38/4, Sarjapur Road, Jakkasandra, Koramangala, Bangalore  560038, gview.setAdapter(new CustomGridViewAdapter(getActivity(), gridNameList, gridImages, dealerAmenities, dealerAmenities_Images));

                    new JsonGetDealerAmenities().execute();

                    gview.setOnTouchListener(new GridView.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            int action = event.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_DOWN:
                                    // Disallow ScrollView to intercept touch events.
                                    v.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;

                                case MotionEvent.ACTION_UP:
                                    // Allow ScrollView to intercept touch events.
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }

                            // Handle ListView touch events.
                            v.onTouchEvent(event);
                            return true;
                        }
                    });


                    ShowOrHide.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            if (ShowOrHide.isChecked()) {
                                //  Toast.makeText(getActivity(), "ON", Toast.LENGTH_SHORT).show();
                                gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));

                            } else {
                                //  Toast.makeText(getActivity(), "OFF", Toast.LENGTH_SHORT).show();
                                gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 350));
                            }


                        }
                    });

                    checkAvailable.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            startActivity(new Intent(getActivity(), ServiceConfirm.class));
                        }
                    });
                }
            };
            mHandler = new Handler();
            mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);


        } else {

            /*

            Load data when accessed runtime permission
             */
            dealerName = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
            dealerAddress = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_ADDR, "");

            TextCenterModel.setText(dealerName);
            TextCenterAddress.setText(dealerAddress);

            dealerAmentiesList = new ArrayList<>();
            dealerAmenitiesId = new ArrayList<>();
            amenitiesImgLink = new ArrayList<>();

            dealerAmentiesList.clear();
            dealerAmenitiesId.clear();
            amenitiesImgLink.clear();

            gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 350));

            gview = (ExpandableHeightGridView)v.findViewById(R.id.gridView);
            // "38/4, Sarjapur Road, Jakkasandra, Koramangala, Bangalore  560038, gview.setAdapter(new CustomGridViewAdapter(getActivity(), gridNameList, gridImages, dealerAmenities, dealerAmenities_Images));

            new JsonGetDealerAmenities().execute();

            gview.setOnTouchListener(new GridView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });


            ShowOrHide.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (ShowOrHide.isChecked()) {
                        //  Toast.makeText(getActivity(), "ON", Toast.LENGTH_SHORT).show();
                        gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                500));

                    } else {
                        //  Toast.makeText(getActivity(), "OFF", Toast.LENGTH_SHORT).show();
                        gridLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 350));
                    }


                }
            });

            checkAvailable.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    startActivity(new Intent(getActivity(), ServiceConfirm.class));
                }
            });

        }


                return v;
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetDealerAmenities extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
         /*   pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show(); */
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, dealer_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "dealeramenities"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    if(status.equals("success")) {

                        dealerAmentiesList.clear();
                        dealerAmenitiesId.clear();

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> jdataObj_Keys = dataObj.keys();

                                dealer_amenities = dataObj.get("dealer_amenities").toString();
                                System.out.println(dealer_amenities);

                                while(jdataObj_Keys.hasNext() ) {
                                    String amentiesObj_key = (String) jdataObj_Keys.next();

                                    if (amentiesObj_key.equals("amenities")) {
                                    JSONObject amentiesObj = (JSONObject) dataObj.get(amentiesObj_key);
                                    Iterator<?> dataObj_keys = amentiesObj.keys();


                                        for(int i=0; i<amentiesObj.length(); i++) {

                                            String count = Integer.toString(i);
                                            JSONObject tmpObj = amentiesObj.getJSONObject(count);

                                            id = tmpObj.get("id").toString();
                                            icon = tmpObj.get("icon").toString();
                                            type = tmpObj.get("type").toString();

                                            dealerAmentiesList.add(type);
                                            dealerAmenitiesId.add(id);

                                            amenitiesImgLink.add("http://goserwizz.com/images/"+ icon);
                                        }
                                    while (dataObj_keys.hasNext()) {
                                        String dataObj_key = (String) dataObj_keys.next();

                                        JSONObject tmpObj = (JSONObject) amentiesObj.get(dataObj_key);
                                        id = tmpObj.get("id").toString();
                                        icon = tmpObj.get("icon").toString();
                                        type = tmpObj.get("type").toString();

                                     //   dealerAmentiesList.add(type);
                                     //   dealerAmenitiesId.add(id);

                                    //    dealerAmenitiesNameSet.add(id);

                                        System.out.println(tmpObj.get("id"));
                                        System.out.println(tmpObj.get("type"));
                                    }
                                }
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
          /*  if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */


            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new JsonGetDealerAmenities().execute();
                    }
                    else {

                        tset = new TreeSet<String>(dealerAmenitiesNameSet);

                        System.out.println("hashset:  "+tset);

                        dealerAmenitiesArr = new String[dealerAmentiesList.size()];
                        dealerAmenitiesArr =dealerAmentiesList.toArray(dealerAmenitiesArr);

                        amemitiesImageLink = new String[amenitiesImgLink.size()];
                        amemitiesImageLink = amenitiesImgLink.toArray(amemitiesImageLink);

                        String[] dealerAmenties = {};

                        if(!dealer_amenities.equals("")) {
                            dealerAmenties = dealer_amenities.split(",");
                        }


                        CustomGridViewAdapter adapter = new CustomGridViewAdapter(getActivity(), dealerAmenitiesArr, amemitiesImageLink, dealerAmenties, amemitiesImageLink);
                        gview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                        //   gview.setAdapter(new CustomGridViewAdapter(getActivity(), dealerAmenitiesArr, dealerAmenities_Images, dealerAmenties, gridImages));

                    }


                    }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetDealerAmenities().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


    private boolean checkWriteExternalStoragePermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestWriteExternalStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            Toast.makeText(getActivity(), "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WExternalStorage_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_WExternalStorage_REQUEST_CODE:

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }

}
