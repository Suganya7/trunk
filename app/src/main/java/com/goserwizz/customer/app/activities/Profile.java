package com.goserwizz.customer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/15/2016.
 */
public class Profile extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    TextView profile_email;
    EditText profile_name, profile_pwd, profile_mobile;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_PINCODE = "pincode";
    private static final String TAG_CITY = "city";
    private static final String TAG_LOCATION = "location";
    private static final String TAG_STATE = "state";
    private static final String TAG_ADDRESS = "addr";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    String status, resultmsg;
    String fname, lname, mob, email, pwd, pincode, location, state, city, address;
    String gu_id, mobile, name;
    TextView changePassword;
    Button ProfileSaveBtn;
    ImageView editName, editMobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        SettingsUtils.init(this);
        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

        profile_email = (TextView)findViewById(R.id.profile_emailid);
        profile_name = (EditText)findViewById(R.id.profile_name);
        profile_pwd = (EditText)findViewById(R.id.profile_pwdd);
        profile_mobile = (EditText)findViewById(R.id.profile_mobileNo);
        editName = (ImageView)findViewById(R.id.edit_name);
        editMobile = (ImageView)findViewById(R.id.edit_mobile);
        ProfileSaveBtn = (Button)findViewById(R.id.profileSaveBtn);
        ProfileSaveBtn.setOnClickListener(this);
        editName.setOnClickListener(this);
        editMobile.setOnClickListener(this);


        if(Connectivity.isConnected(this)) {

            new JsonGetUserDetails().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }

    /*    fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

        profile_email.setText(email);
        profile_name.setText(fname);
        profile_pwd.setText(pwd);
        profile_mobile.setText(mob);
*/

        toolbar = (Toolbar)findViewById(R.id.Profiletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");

        changePassword = (TextView)findViewById(R.id.changePassword);
        changePassword.setPaintFlags(changePassword.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        changePassword.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                   startActivity(new Intent(Profile.this, ChangePassword.class));
            }
            });
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {

            case R.id.profileSaveBtn:

                name = profile_name.getText().toString();
                mobile = profile_mobile.getText().toString();
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, name);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mobile);

                if(Connectivity.isConnected(this)) {

                    new JsonUpdateUserDetails().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(this);
                }

                break;

            case R.id.edit_name:
                profile_name.setEnabled(true);
                break;

            case R.id.edit_mobile:
                profile_mobile.setEnabled(true);
                break;
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonUpdateUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
            lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
            pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
            email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
            mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");


            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_FNAME, fname));
            nameValuePairs.add(new BasicNameValuePair(TAG_LNAME, lname));
            nameValuePairs.add(new BasicNameValuePair(TAG_MOBILE, mob));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, email));
            nameValuePairs.add(new BasicNameValuePair(TAG_ADDRESS, "null"));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, "null"));
            nameValuePairs.add(new BasicNameValuePair(TAG_STATE, "null"));
            nameValuePairs.add(new BasicNameValuePair(TAG_LOCATION, "null"));
            nameValuePairs.add(new BasicNameValuePair(TAG_PINCODE, "null"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "update"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                        System.out.println(resultmsg);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonUpdateUserDetails().execute();


                    }
                    else {
                        showResultDialog(resultmsg);
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonUpdateUserDetails().execute();


                    } else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            new JsonGetUserDetailsUpdate().execute();
                         //   finish();

                        }
                    })
                    //.setNegativeButton("Cancel", null)
                    .show();
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getusrdet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                fname = dataObj.get("fname").toString();
                                lname = dataObj.get("lname").toString();
                                mob = dataObj.get("mob").toString();
                                email = dataObj.get("email").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, lname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);

                             /*   pincode = dataObj.get("pincode").toString();
                                city = dataObj.get("city").toString();
                                location = dataObj.get("location").toString();
                                state = dataObj.get("state").toString();
                                address = dataObj.get("addr").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOCATION, location);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_ADDRESS, address);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_CITY, city);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_STATE, state);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PINCODE, pincode);
*/
                                System.out.println(dataObj.get("fname"));
                                System.out.println(dataObj.get("mob"));

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {

                        fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                        lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
                        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
                        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
                        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

                        profile_email.setText(email);
                        profile_name.setText(fname);
                        profile_pwd.setText(pwd);
                        profile_mobile.setText(mob);
                    }
                } else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetailsUpdate extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getusrdet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                fname = dataObj.get("fname").toString();
                                lname = dataObj.get("lname").toString();
                                mob = dataObj.get("mob").toString();
                                email = dataObj.get("email").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, lname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);

                             /*   pincode = dataObj.get("pincode").toString();
                                city = dataObj.get("city").toString();
                                location = dataObj.get("location").toString();
                                state = dataObj.get("state").toString();
                                address = dataObj.get("addr").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOCATION, location);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_ADDRESS, address);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_CITY, city);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_STATE, state);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PINCODE, pincode);
*/
                                System.out.println(dataObj.get("fname"));
                                System.out.println(dataObj.get("mob"));

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetailsUpdate().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {

                        fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                        lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
                        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
                        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
                        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

                        profile_email.setText(email);
                        profile_name.setText(fname);
                        profile_pwd.setText(pwd);
                        profile_mobile.setText(mob);

                     //   finish();
                    }
                } else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetailsUpdate().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    private void showResultDialog1(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


}
