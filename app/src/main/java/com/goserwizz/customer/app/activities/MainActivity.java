package com.goserwizz.customer.app.activities;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TabHost;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.RegisterFragment;
import com.goserwizz.customer.app.fragments.LoginFragment;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    SharedPreferences prefs;
    String signin;
    TabHost tabHost;
    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_PINCODE = "pincode";
    private static final String TAG_CITY = "city";
    private static final String TAG_LOCATION = "location";
    private static final String TAG_STATE = "state";
    private static final String TAG_ADDRESS = "addr";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    String status, resultmsg;
    String fname, lname, mob, email, pincode, location, state, city, address;
    String gu_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SettingsUtils.init(this);
        gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

   /*     if(!gu_id.isEmpty()) {
            if(Connectivity.isConnected(this)) {

                new JsonGetUserDetails().execute();
            }
            else {
                Connectivity.showNoConnectionDialog(this);
            }
        }
        */




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign In");

      //  viewPager = (ViewPager) findViewById(R.id.viewpager);
      //  setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
      //  tabLayout.setupWithViewPager(viewPager);

        tabLayout.addTab(tabLayout.newTab().setText("SIGN IN"));
        tabLayout.addTab(tabLayout.newTab().setText("REGISTER"));

        LoginFragment Fragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frameMain, Fragment).commit();


        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getApplicationContext(), R.color.White));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
              //  viewPager.setCurrentItem(tab.getPosition());
                String value =  tab.getText().toString();
                getSupportActionBar().setTitle(value);

                if (tab.getPosition() == 0) {
                    LoginFragment Fragment = new LoginFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameMain, Fragment).commit();
                }
                else if (tab.getPosition() == 1) {
                    RegisterFragment Fragment = new RegisterFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameMain, Fragment).commit();
                }



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "SIGN IN");
        adapter.addFragment(new RegisterFragment(), "REGISTER");

        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void onBackPressed() {
      /*  moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
*/
        finish();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // NavUtils.navigateUpFromSameTask(this);

                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
       /*     pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();*/
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getusrdet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                fname = dataObj.get("fname").toString();
                                lname = dataObj.get("lname").toString();
                                mob = dataObj.get("mob").toString();
                                email = dataObj.get("email").toString();
                            //    pincode = dataObj.get("pincode").toString();
                            //    city = dataObj.get("city").toString();
                            //    location = dataObj.get("location").toString();
                            //    state = dataObj.get("state").toString();
                            //    address = dataObj.get("addr").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, lname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                            //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOCATION, location);
                            //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_ADDRESS, address);
                            //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_CITY, city);
                            //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_STATE, state);
                            //    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PINCODE, pincode);

                                System.out.println(dataObj.get("fname"));
                                System.out.println(dataObj.get("mob"));

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
          /*  if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(MainActivity.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonGetUserDetails().execute();
                      //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {

                        new JsonGetUserDetails().execute();
                      //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }



    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if(appDir.exists()){
            String[] children = appDir.list();
            for(String s : children){
                if(!s.equals("lib")){
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                }
            }
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public void trimCache() {
        try {
            File dir = getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(); //if trimCache is static
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
