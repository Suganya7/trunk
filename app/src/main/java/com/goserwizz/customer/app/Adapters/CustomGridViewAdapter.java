package com.goserwizz.customer.app.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.ImageLoader.ImageLoader;
import com.goserwizz.customer.app.R;

/**
 * Created by Kaptas on 3/11/2016.
 */
    public class CustomGridViewAdapter extends BaseAdapter{

        String [] result, dealeramenties;
        Context context;
        String [] imageId, dealerimage;
        //int [] imageId, dealerimage;

        private static LayoutInflater inflater=null;
        public CustomGridViewAdapter(Context cxt, String[] gridNameList, String[] gridImages, String[] dealerAmenties, String[] dealerImages) {
            // TODO Auto-generated constructor stub
            result=gridNameList;
            context=cxt;
            imageId=gridImages;
            dealeramenties = dealerAmenties;
            dealerimage = dealerImages;

            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return result.length;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder
        {
            TextView tv;
            ImageView img;
            LinearLayout gridlay;
        }

    @Override
    public boolean isEnabled(int position) {
        // Disable the third item of GridView
        if (position == 2) {
            return false;
        } else {
            return true;
        }
    }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder=new Holder();
            View rowView;

            rowView = inflater.inflate(R.layout.grid_row, null);
            holder.gridlay = (LinearLayout)rowView.findViewById(R.id.gridViewLay);
            holder.tv=(TextView) rowView.findViewById(R.id.grid_text);
            holder.img=(ImageView) rowView.findViewById(R.id.grid_image);

           // holder.img.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
          //  holder.img.setEnabled(false);
          //  holder.img.getDrawable().mutate().setAlpha(20);
          //  holder.img.invalidate();


            holder.tv.setText(result[position]);
           // holder.img.setImageResource(imageId[position]);

            ImageLoader imgLoader = new ImageLoader(context);
            int loader = R.drawable.circle;
            String image_url = imageId[position];
            imgLoader.DisplayImage(image_url, loader, holder.img);

            for (String amenities : dealeramenties) {

                int dealerPosition = Integer.parseInt(amenities) - 1;

                if(position == dealerPosition) {

                    holder.tv.setText(result[position]);
                    holder.tv.setTextColor(Color.parseColor("#000000"));
                   // holder.img.setImageResource(dealerimage[position]);
                     /*  holder.tv.setEnabled(false);
                    holder.tv.setClickable(false);*/
                }
            }

          //  holder.tv.setTextColor(Color.parseColor("#CCCCCC"));

            rowView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                  //  Toast.makeText(context, "You Clicked " + result[position], Toast.LENGTH_LONG).show();
                }
            });

            return rowView;
        }

    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }


    public static Bitmap grayScaleImage(Bitmap src) {
        // constant factors
        final double GS_RED = 0.299;
        final double GS_GREEN = 0.587;
        final double GS_BLUE = 0.114;

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        // pixel information
        int A, R, G, B;
        int pixel;

        // get image size
        int width = src.getWidth();
        int height = src.getHeight();

        // scan through every single pixel
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get one pixel color
                pixel = src.getPixel(x, y);
                // retrieve color of all channels
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                // take conversion up to one single value
                R = G = B = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);
                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
                }
            }

        return bmOut;
        }


}
