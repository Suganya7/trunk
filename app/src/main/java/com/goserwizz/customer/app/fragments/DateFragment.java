package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.CustomCalendar.CalendarDayEvent;
import com.goserwizz.customer.app.CustomCalendar.CompactCalendarView;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kaptas on 3/12/2016.
 */
public class DateFragment extends Fragment {

    SharedPreferences prefDate;
    SharedPreferences.Editor edit;
    int day = 0;
    Date holidayDay;
    Calendar cal;
    CalendarView calendarview;

    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private boolean shouldShow = false;
    List arrList;
    TextView MonthView;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";
    // JSON Node names
    private static final String TAG_WEEKOFF = "weekoff";
    private static final String TAG_DAY = "day";
    private static final String TAG_DATESTATUS = "status";
    private static final String TAG_HOLIDAY = "holiday";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "s_date";

    String status, resultmsg, dealer_id, s_date, s_time;
    String online_slot, slot_time, booked_slot;
    String[] timeListArr, onlineSlotArr, bookedSlotArr;
    ArrayList< String> timeList, onlineSlotList, bookedSlotList;
    String weekoffstatus = "",weekoffday = "";
    String r_date, r_time;
    String currenMonthYear, displayMonthYear, NextMonth30days;
    ArrayList arrListSaturday, arrListHoliday, arrListHolidaydate, arrListHolidayReasons;
    ArrayList arrWeekOff, arrWeekoffStatus;
    CompactCalendarView compactCalendarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.date_fragment, container, false);

        SettingsUtils.init(getActivity());
        timeList = new ArrayList<>();
        onlineSlotList = new ArrayList<>();
        bookedSlotList = new ArrayList<>();

        prefDate = getActivity().getSharedPreferences("S_Date", Context.MODE_PRIVATE);
        edit = prefDate.edit();

        arrList = new ArrayList();
        arrListSaturday = new ArrayList();
        arrListHoliday = new ArrayList();
        arrListHolidaydate = new ArrayList();
        arrListHolidayReasons = new ArrayList();
        arrWeekOff = new ArrayList();
        arrWeekoffStatus = new ArrayList();

    /*    s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");
        if(!s_time.equals("") && !(s_time.isEmpty())) {
            ((DateTime) getActivity()).showTime(s_time);

        } */

       /* Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String Current_Date = df.format(c.getTime());
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, Current_Date);  */


       /*  Custom calendar view func and initialization */
        final Button showPreviousMonthBut = (Button) v.findViewById(R.id.prev_button);
        final Button showNextMonthBut = (Button) v.findViewById(R.id.next_button);
        MonthView = (TextView)v.findViewById(R.id.MonthView);

        compactCalendarView = (CompactCalendarView) v.findViewById(R.id.compactcalendar_view);
        compactCalendarView.drawSmallIndicatorForEvents(true);

        // below allows you to configure color for the current day in the month
        compactCalendarView.setCurrentDayBackgroundColor(getResources().getColor(R.color.white));
        // below allows you to configure colors for the current day the user has selected
        //    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.green));

        if(Connectivity.isConnected(getActivity())) {

            new JsonGetCenterHoliday().execute();

          //  addEventsHoliday(compactCalendarView, 1);
           // compactCalendarView.invalidate();

        }
        else {
            Connectivity.showNoConnectionDialog(getActivity());
        }


        //addEvents(compactCalendarView, 1);
       // addEventsSaturday(compactCalendarView, 1);
       // addEventsHoliday(compactCalendarView, 1);
     //   addEvents(compactCalendarView, Calendar.DECEMBER);
     //   addEvents(compactCalendarView, Calendar.AUGUST);
        compactCalendarView.invalidate();


        /* Hide previous button according to Month and year format */

        Date datef = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM - yyyy");
        currenMonthYear = sdf.format(datef).toString();
        System.out.println("Current Month in MMM format : " + currenMonthYear);

        displayMonthYear = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth());

       // Toast.makeText(getActivity(), currenMonthYear, Toast.LENGTH_LONG).show();

        //**** Added on 3th May 2016  for enabling only the dates upto 30 from the current day*****//
        SimpleDateFormat format3 = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal2 = Calendar.getInstance();
        Calendar calReturn = Calendar.getInstance();
        String currentdate = format3.format(cal2.getTime());

        calReturn.add(Calendar.DATE, 30);

        NextMonth30days = sdf.format(calReturn.getTime());

        Log.d("Addedthirtydate Month::", NextMonth30days);

        if(currenMonthYear.equals(displayMonthYear)) {

            showPreviousMonthBut.setVisibility(View.INVISIBLE);
        }
        else if(NextMonth30days.equals(displayMonthYear)){
            showNextMonthBut.setVisibility(View.INVISIBLE);
        }
        else {
            showPreviousMonthBut.setVisibility(View.VISIBLE);
        }

        // below line will display Sunday as the first day of the week
        // compactCalendarView.setShouldShowMondayAsFirstDay(false);
        //set initial title
        MonthView.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        //set title on calendar scroll
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                Date dates = null;
                String formatteds = "";

                // Make a Calendar whose DATE part is some time yesterday.
                Calendar cal = Calendar.getInstance();
                cal.roll(Calendar.DATE, -1);

                if (dateClicked.before(cal.getTime())) {
                //  myDate must be yesterday or earlier
                //  compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));
               //     Toast.makeText(getActivity(), "yesterday", Toast.LENGTH_LONG).show();
                    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                    formatteds = format2.format(dateClicked);
                    if (arrList.contains(formatteds)) {
                        setDisableDates(formatteds, false);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));
                    }
                    else if (arrListSaturday.contains(formatteds)) {
                        setDisableDates(formatteds, false);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));
                    }
                    else if (arrListHolidaydate.contains(formatteds)) {

                        for(int i=0; i<arrListHolidaydate.size(); i++) {

                            if(formatteds.equals(arrListHolidaydate.get(i).toString())) {

                                String reasons = arrListHolidayReasons.get(i).toString();

                                DateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy");
                                String Dialogdate = format3.format(dateClicked);

                                showResultDialogFail("The Day " + Dialogdate + " is holiday for " + reasons);
                            }
                        }

                        setDisableDates(formatteds, false);
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));
                    }
                    else {
                        compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));
                    }


                } else {
                    //  myDate must be today or later
               //     Toast.makeText(getActivity(), "today or tomo", Toast.LENGTH_LONG).show();

                    try {
                        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                        formatteds = format2.format(dateClicked);
                        //  dates = format2.parse(dateClicked.toString());
                        Log.d("DATe:", formatteds);

                        ///*** Extra added for 30 days from current date started ***///
                        Calendar cal2 = Calendar.getInstance();
                        Calendar calReturn = Calendar.getInstance();
                        String currentdate = format2.format(cal2.getTime());
                        calReturn.add(Calendar.DATE, 30);
                        String addedthirtydate = format2.format(calReturn.getTime());

                            String str1 = formatteds;
                            Date date1 = format2.parse(str1);

                            String str2 = addedthirtydate;
                            Date date2 = format2.parse(str2);

                            if (formatteds.equals(currentdate)) {
                                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));
                            //    Toast.makeText(getActivity(), "currentdate:: " + currentdate, Toast.LENGTH_LONG).show();

                            }

                            else if (date2.compareTo(date1) < 0) {
                                System.out.println("selected date is Greater than current 30 dates");
                                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.white));

                            }  ///*** Extra added for 30 days from current date ended ***///
                            else if (arrListHolidaydate.contains(formatteds)) {

                            for(int i=0; i<arrListHolidaydate.size(); i++) {

                                if(formatteds.equals(arrListHolidaydate.get(i).toString())) {

                                    String reasons = arrListHolidayReasons.get(i).toString();

                                    DateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy");
                                    String Dialogdate = format3.format(dateClicked);

                                    showResultDialogFail("The Day " + Dialogdate + " is holiday for " + reasons);
                                }
                            }
                            // arrListHolidayReasons.add(reason);

                            setDisableDates(formatteds, false);
                                compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));
                        }
                        else if (arrList.contains(formatteds)) {
                            setDisableDates(formatteds, false);
                            compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));

                            DateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy");
                            String Dialogdate = format3.format(dateClicked);

                            showResultDialogFail("The Day " + Dialogdate + " is Weekoff Sunday ");
                        }
                        else if (arrListSaturday.contains(formatteds)) {
                            setDisableDates(formatteds, false);
                            compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));

                            DateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy");
                            String Dialogdate = format3.format(dateClicked);

                            showResultDialogFail("The Day " + Dialogdate + " is Weekoff Saturday ");

                        }

                        else {
                            compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.PrimaryRed));

                            Toast.makeText(getActivity(), formatteds + "", Toast.LENGTH_SHORT).show();


                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, formatteds);
                            r_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_STIME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, formatteds);
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_R_DATETIME, formatteds + " / " + r_time);

                            // set date on tab text
                   //         ((DateTime) getActivity()).showDate(formatteds);


                            if (Connectivity.isConnected(getActivity())) {

                                new JsonGetSlotTime().execute();

                            } else {
                                Connectivity.showNoConnectionDialog(getActivity());
                            }


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }





            /*    if (arrList.contains(formatteds)) {
                    setDisableDates(formatteds, false);
                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.cal_grey));
                } else {
                    compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.PrimaryRed));

                    Toast.makeText(getActivity(), formatteds + "", Toast.LENGTH_SHORT).show();
                }
  */

            }


            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                MonthView.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });

        showPreviousMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showPreviousMonth();

                displayMonthYear = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth());

                if(currenMonthYear.equals(displayMonthYear)) {

                    showPreviousMonthBut.setVisibility(View.INVISIBLE);
                    showNextMonthBut.setVisibility(View.VISIBLE);
                }
                else {
                    showPreviousMonthBut.setVisibility(View.VISIBLE);
                }
            }
        });

        showNextMonthBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compactCalendarView.showNextMonth();

                displayMonthYear = dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth());

                if(NextMonth30days.equals(displayMonthYear)){
                    showNextMonthBut.setVisibility(View.INVISIBLE);
                    showPreviousMonthBut.setVisibility(View.VISIBLE);
                }
                else {
                    showPreviousMonthBut.setVisibility(View.VISIBLE);
                }

            }
        });

        return v;

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetCenterHoliday extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, dealer_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getcenterholiday"));
            // nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, "66"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    arrListHolidaydate.clear();
                    arrListHolidayReasons.clear();
                    arrWeekOff.clear();
                    arrWeekoffStatus.clear();

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                if(dataObj_key.equals("holiday")) {
                                    JSONObject dataObj1 = (JSONObject) dataObj.get(dataObj_key);
                                    Iterator<?> dataObj_keys1 = dataObj1.keys();

                                    while (dataObj_keys1.hasNext()) {
                                        String dataObj_key1 = (String) dataObj_keys1.next();

                                       JSONObject dataobjj = (JSONObject)dataObj1.get(dataObj_key1);

                                        String reason = dataobjj.get("reason").toString();
                                        String date = dataobjj.get("date").toString();

                                        arrListHolidaydate.add(date);
                                        arrListHolidayReasons.add(reason);

                                        }
                                    }


                                    if(dataObj_key.equals("weekoff")) {
                                        JSONObject dataObj1 = (JSONObject) dataObj.get(dataObj_key);
                                        Iterator<?> dataObj_keys1 = dataObj1.keys();

                                        while (dataObj_keys1.hasNext()) {
                                            String dataObj_key1 = (String) dataObj_keys1.next();

                                            JSONObject dataobjj = (JSONObject)dataObj1.get(dataObj_key1);

                                            String status = dataobjj.get("status").toString();
                                            String day = dataobjj.get("day").toString();

                                            arrWeekOff.add(day);
                                            arrWeekoffStatus.add(status);

                                        }
                                    }

                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetCenterHoliday().execute();
                    }
                    else {

                        if(arrWeekOff.contains("Sun")) {

                            addEvents(compactCalendarView, 1);

                        }
                        if(arrWeekOff.contains("Sat")) {

                            addEventsSaturday(compactCalendarView, 1);
                        }

                        if(!arrListHolidaydate.equals("")) {

                            addEventsHoliday(compactCalendarView, 1);
                        }
                        compactCalendarView.invalidate();
                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetCenterHoliday().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

/* Custom Compact Calendar View Code written by suganya */


    private void addEvents(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();

        arrList.clear();
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar cal = Calendar.getInstance();

        for (int i = 0; i <= 51; i++)
        {
            try
            {
                currentCalender.add(Calendar.WEEK_OF_YEAR, 1);
                currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

                String formatted = format1.format(currentCalender.getTime());
                date = format1.parse(formatted);
                arrList.add(formatted);

                System.out.println("sundays===>" + formatted + date);

                setDisableDates(formatted, false);

                setToMidnight(currentCalender);
                compactCalendarView.addEvent(new CalendarDayEvent(currentCalender.getTimeInMillis(), R.color.cal_grey), false);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void addEventsSaturday(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();

        arrListSaturday.clear();
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar cal = Calendar.getInstance();

        for (int i = 0; i <= 51; i++)
        {
            try
            {
                currentCalender.add(Calendar.WEEK_OF_YEAR, 1);
                currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

                String formatted = format1.format(currentCalender.getTime());
                date = format1.parse(formatted);
                arrListSaturday.add(formatted);

                System.out.println("saturday===>" + formatted + date);

                setDisableDates(formatted, false);

                setToMidnight(currentCalender);
                compactCalendarView.addEvent(new CalendarDayEvent(currentCalender.getTimeInMillis(), R.color.cal_grey), false);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addEventsHoliday(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
      //  currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();

        arrListHoliday.clear();
        DateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        for (int i = 0; i < arrListHolidaydate.size(); i++) {
            String inputString2 = arrListHolidaydate.get(i).toString();

            String[] datesplit = inputString2.split("-");
            int year = Integer.parseInt(datesplit[0]);
            int monthval = Integer.parseInt(datesplit[1]);
            int day = Integer.parseInt(datesplit[2]);

            monthval = monthval - 1;

            currentCalender.set(Calendar.YEAR, year);
            currentCalender.set(Calendar.MONTH, monthval);
            currentCalender.set(Calendar.DAY_OF_MONTH, day);

            setToMidnight(currentCalender);
            compactCalendarView.addEvent(new CalendarDayEvent(currentCalender.getTimeInMillis(), R.color.cal_grey), false);
        }
    }


    public void setDisableDates(String date, boolean obj) {

        if(!(date==null))  {

            obj = false;
        }
        return;
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    /* Custom Compact Calendar View Code ended */



    /* Caldroid calender view custom library functions  */
    private void setCustomResourceForDates() {

        Calendar cal = Calendar.getInstance();
        //highlighlighting the holidays in a month taking the static dates
        ArrayList<String> dates = new ArrayList<String>();
        dates.add("02-08-2015");
        dates.add("22-08-2015");
        dates.add("17-09-2015");
        dates.add("25-09-2015");
        dates.add("27-09-2015");
        dates.add("13-10-2015");
        dates.add("22-10-2015");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");

     //   SimpleDateFormat inputString2 = new SimpleDateFormat();

        Date date = new Date();
        for (int i = 1; i < dates.size(); i++) {
            String inputString2 = dates.get(i);
            String inputString1 = myFormat.format(date);

            try {
                //Converting String format to date format
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                //Calculating number of days from two dates
                long diff = date2.getTime() - date1.getTime();
                long datee = diff / (1000 * 60 * 60 * 24);
                //Converting long type to int type
                day = (int) datee;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cal = Calendar.getInstance();
            cal.add(Calendar.DATE, day);
            holidayDay = cal.getTime();
            colors();

        }
    }

    public void colors() {
        if (cal != null) {

          //  ColorDrawable green = new ColorDrawable(Color.GREEN);
         //   cal.setBackgroundResourceForDate(green, holidayDay);

         //   cal.setTextColorForDate(R.color.White, holidayDay);
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetSlotTime extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
         /*   pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();  */
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
            s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
            timeList.clear();
            onlineSlotList.clear();
            bookedSlotList.clear();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DEALER_ID, dealer_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, s_date));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getslottime"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();


                                for(int i=0; i<dataObj.length(); i++) {

                                    String count = Integer.toString(i);
                                    JSONObject tmpObj = dataObj.getJSONObject(count);

                                    online_slot = tmpObj.get("online_slot").toString();
                                    slot_time = tmpObj.get("slot_time").toString();
                                    booked_slot = tmpObj.get("booked_slot").toString();

                                    timeList.add(slot_time);
                                    onlineSlotList.add(online_slot);
                                    bookedSlotList.add(booked_slot);

                                    System.out.println(tmpObj.get("online_slot"));
                                    System.out.println(tmpObj.get("slot_time"));
                                    System.out.println(tmpObj.get("booked_slot"));
                                }


                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    online_slot = tmpObj.get("online_slot").toString();
                                    slot_time = tmpObj.get("slot_time").toString();
                                    booked_slot = tmpObj.get("booked_slot").toString();


                                    // timeList.add(slot_time);

                                  //  System.out.println(tmpObj.get("online_slot"));
                                  //  System.out.println(tmpObj.get("slot_time"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        /*    if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            timeListArr = new String[timeList.size()];
            timeListArr = timeList.toArray(timeListArr);

            onlineSlotArr = new String[onlineSlotList.size()];
            onlineSlotArr = onlineSlotList.toArray(onlineSlotArr);

            bookedSlotArr = new String[bookedSlotList.size()];
            bookedSlotArr = bookedSlotList.toArray(bookedSlotArr);

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new JsonGetSlotTime().execute();
                    }
                    else {
                       // customAdapter = new CustomAdapter(getActivity(), timeListArr);
                       // listView.setAdapter(customAdapter);
                        SettingsUtils.getInstance().putListString("timelist", timeList);
                        SettingsUtils.getInstance().putListString("onlineslotlist", onlineSlotList);
                        SettingsUtils.getInstance().putListString("bookedslotlist", bookedSlotList);
                      //  showResultDialogFail(timeList + "");
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetSlotTime().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }


  /*  public void onBackPressed() {

        startActivity(new Intent(getActivity(), HomeActivity.class));
        getActivity().finish();
    }*/

}


