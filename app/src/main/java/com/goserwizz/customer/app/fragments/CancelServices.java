package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/12/2016.
 */
public class CancelServices extends Fragment {

    String[] reasons ={"Due to other commitments", "Proceeding with Other service center", "Other reasons"};
    TextView autoReasons;
    RadioButton radioBtn;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_S_ID = "s_id";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_REASONS = "reason";
    private static final String TAG_GUID = "gu_id";

    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";

    String status, resultmsg, dealer_id, gu_id;
    String reasonsText, Comments;
    Button cancelService;
    EditText cancel_Comments;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.cancel_services, container, false);

     //   radioBtn = (RadioButton)v.findViewById(R.id.cancelRadioBtn);
        cancelService = (Button)v.findViewById(R.id.proceedCancel);
        cancel_Comments = (EditText)v.findViewById(R.id.cancel_Comments);

        radioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (radioBtn.isChecked() == true) {
                    radioBtn.setChecked(false);
                }
            }
        });

        cancelService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateSelectReasons()) {
                    return;
                }

                if (!validateCancelComments()) {
                    return;
                }

                reasonsText= autoReasons.getText().toString();
                Comments = cancel_Comments.getText().toString();

                if(Connectivity.isConnected(getActivity())) {

                    new JsonGetCancelService().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(getActivity());
                }

            }
        });



        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(),android.R.layout.select_dialog_item,reasons);

        autoReasons= (TextView)v.findViewById(R.id.SelectReason);
      /*  autoReasons.setThreshold(1);
        autoReasons.setAdapter(adapter);

        autoReasons.addTextChangedListener(new MyTextWatcher(autoReasons)); */

        return v;

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            String other = autoReasons.getText().toString();

            if(other.equals("Other reasons")) {

                cancel_Comments.setVisibility(View.VISIBLE);

            }


        }
    }

    private boolean validateSelectReasons() {
        if (autoReasons.getText().toString().trim().isEmpty()) {
            autoReasons.setError(getString(R.string.err_servicedetails));
            requestFocus(autoReasons);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCancelComments() {
        if (cancel_Comments.getText().toString().trim().isEmpty()) {
            cancel_Comments.setError(getString(R.string.err_comments));
            requestFocus(cancel_Comments);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetCancelService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            dealer_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALERID, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_S_ID, "BACB16134313"));
            nameValuePairs.add(new BasicNameValuePair(TAG_REASONS, reasonsText));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, Comments));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "cancelbooking"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetCancelService().execute();
                    }
                    else {

                        showResultDialogFail(resultmsg);

                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetCancelService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Book My Services");

                            BookMyServices fragment = new BookMyServices();
                            android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame, fragment);
                            fragmentTransaction.commit();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

}
