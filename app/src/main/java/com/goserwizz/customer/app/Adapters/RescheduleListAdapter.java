package com.goserwizz.customer.app.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.RescheduleSlot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Kaptas on 4/22/2016.
 */
public class RescheduleListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] dateTime, regNO, Scenter, Stype, bookstatus, serviceId;
    ArrayList<String> datelist, dealeridList;

    public RescheduleListAdapter(Activity context, String[] date, String[] regno, String[] serviceCenter, String[] servicetype, String[] bookstatus, String[] serviceId) {
        super(context, R.layout.reschedule_items, regno);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.dateTime=date;
        this.regNO=regno;
        this.Scenter=serviceCenter;
        this.Stype=servicetype;
        this.bookstatus=bookstatus;
        this.serviceId=serviceId;
    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {
        TextView datetime, regno, servicecenter, resch_slot_det;
    }

    public View getView(final int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.reschedule_items, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        TextView Reschedule_Item_datetime = (TextView) rowView.findViewById(R.id.Reschedule_Item_datetime);
        TextView R_Item_regno = (TextView) rowView.findViewById(R.id.R_Item_regno);
        TextView R_Item_service_center = (TextView) rowView.findViewById(R.id.R_Item_service);
        TextView Resch_Slot_Detail = (TextView) rowView.findViewById(R.id.resch_slot_det);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.Reschedule_Item_datetime, viewHolder.datetime);
        rowView.setTag(R.id.R_Item_regno, viewHolder.regno);
        rowView.setTag(R.id.R_Item_service, viewHolder.servicecenter);
        rowView.setTag(R.id.resch_slot_det);

        Reschedule_Item_datetime.setText(dateTime[position]);
        R_Item_regno.setText(regNO[position]);
        R_Item_service_center.setText(Scenter[position]);
        String status = bookstatus[position];
        String s_id = serviceId[position];
        String stype= Stype[position];

        datelist = SettingsUtils.getInstance().getListString(SettingsUtils.KEY_BOOKEDDATE);
        datelist.get(position);

        dealeridList = SettingsUtils.getInstance().getListString(SettingsUtils.KEY_DEALER_ID_LIST);
        dealeridList.get(position);


        Resch_Slot_Detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date strDate = null;
                try {
                    strDate = sdf.parse(datelist.get(position));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (new Date().before(strDate)) {

                 //   Toast.makeText(getContext(), "Booked date not completed" + strDate, Toast.LENGTH_LONG).show();

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_REGNO, regNO[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SCENTER, Scenter[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STYPE, Stype[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SERVICEID, serviceId[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_BOOKSTATUS, bookstatus[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_S_DATETIME, dateTime[position]);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_R_DATETIME, dateTime[position]);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALERID, dealeridList.get(position));

                    context.startActivity(new Intent(getContext(), RescheduleSlot.class));

                }
                else {
                    //Toast.makeText(getContext(), "Your Booked Date is completed" + strDate, Toast.LENGTH_LONG).show();

                    showResultDialog("This Booked Service is over date, so you cannot be able to Reschedule the Service ");
                }



             //   Toast.makeText(context, serviceId[position] +"  ,  " + regNO[position], Toast.LENGTH_LONG).show();

            }
        });

        return rowView;

    }

    private void showResultDialog(final String result) {
        if (context != null && !context.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(context)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


}
