package com.goserwizz.customer.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.Amenities;
import com.goserwizz.customer.app.fragments.LocationFragment;

/**
 * Created by Kaptas on 3/8/2016.
 */
public class ServiceCenter extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView mdateImage;
    private ImageView mtimeImage;

    private ViewPager mViewPager;
    private LinearLayout mdateLay;
    private LinearLayout mtimeLays;
    TextView date, time;
    View view1, view2;

    private Context context;
    private Activity activity;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PERMISSION_WExternalStorage_REQUEST_CODE = 2;
    private View view;
    private static final long SPLASH_TIME = 3000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_center);

        toolbar = (Toolbar) findViewById(R.id.toolbarService);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Service center");

        SettingsUtils.init(this);

        mdateImage = (ImageView) findViewById(R.id.date_img);
        mtimeImage = (ImageView) findViewById(R.id.time_img);
        date = (TextView) findViewById(R.id.dateText);
        time = (TextView) findViewById(R.id.timeText);
        view1 = (View) findViewById(R.id.viewLocation);
        view2 = (View) findViewById(R.id.viewAmenties);


        mdateLay = (LinearLayout) findViewById(R.id.date_lay1);
        mtimeLays = (LinearLayout) findViewById(R.id.time_lay1);

        mdateLay.setOnClickListener(this);
        mtimeLays.setOnClickListener(this);

        mdateImage.setBackgroundResource(R.drawable.location1);
        mViewPager = (ViewPager) findViewById(R.id.pager_service_center);
        mViewPager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    float elevation = 2;
                    mdateImage.setBackgroundResource(R.drawable.location1);
                    mtimeImage.setBackgroundResource(R.drawable.amenties2);
                    date.setTextColor(Color.WHITE);
                    time.setTextColor(Color.parseColor("#ff9999"));
                    view1.setBackgroundColor(Color.WHITE);
                    view1.setElevation(elevation);
                    view2.setBackgroundColor(Color.parseColor("#cc0000"));

                } else if (position == 1) {

                    float elevation = 2;
                    mdateImage.setBackgroundResource(R.drawable.location2);
                    mtimeImage.setBackgroundResource(R.drawable.amenties1);
                    date.setTextColor(Color.parseColor("#ff9999")); // Slight red color
                    time.setTextColor(Color.WHITE);
                    view2.setBackgroundColor(Color.WHITE);
                    view2.setElevation(elevation);
                    view1.setBackgroundColor(Color.parseColor("#cc0000"));

                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {

            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("viewpager:");
            switch (position) {
                case 0:

                    if (!checkPermission()) {

                        requestPermission();

                    } else {

                        Log.d("Permission granted", "Access fine location");

                     //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

                    }

                    LocationFragment mFrag1 = new LocationFragment();
                    return mFrag1;


                case 1:

                    if (!checkWriteExternalStoragePermission()) {

                        requestWriteExternalStoragePermission();

                    } else {

                        Log.d("Permission granted", "Access write external storage for sdcard");

                        //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

                    }


                    Amenities mFrag2 = new Amenities();
                    return mFrag2;

                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new Fragment();
                    return fragment;
            }

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.date_lay1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.time_lay1:
                mViewPager.setCurrentItem(1);
                break;

        }

    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)){

            Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_CODE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access location data.");
                         //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access location data.");
                          //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;

            case PERMISSION_WExternalStorage_REQUEST_CODE:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }


    private boolean checkWriteExternalStoragePermission(){
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestWriteExternalStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            Toast.makeText(ServiceCenter.this, "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_WExternalStorage_REQUEST_CODE);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
               // NavUtils.navigateUpFromSameTask(this);
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
