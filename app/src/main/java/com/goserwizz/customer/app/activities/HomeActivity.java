package com.goserwizz.customer.app.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.BookMyServices;
import com.goserwizz.customer.app.fragments.CancelServiceList;
import com.goserwizz.customer.app.fragments.Content;
import com.goserwizz.customer.app.fragments.FAQfragment;
import com.goserwizz.customer.app.fragments.HowItWorks;
import com.goserwizz.customer.app.fragments.RescheduleSlotList;
import com.goserwizz.customer.app.fragments.Signout;
import com.goserwizz.customer.app.fragments.SimpleFrag;
import com.goserwizz.customer.app.fragments.Feedback;

import java.io.IOException;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ImageView profileImg;
    private int PICK_IMAGE_REQUEST = 1;

    LinearLayout mDrawerContentLayout;
    DrawerLayout drawer;
    View previousParentView = null, previousSubParentView = null;
    ImageView previousImgLogo = null, previousSubImgLogo = null;
    TextView previousTxtTitle = null, previousSubTxtTitle = null;
    int previouskey, parentPreviousKey;
    TextView home_email, home_name;
    String UserEmail, UserName;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PERMISSION_WExternalStorage_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SettingsUtils.init(this);

        if (!checkWriteExternalStoragePermission()) {

            requestWriteExternalStoragePermission();

        } else {

            Log.d("Permission granted", "Access write external storage for sdcard");

            //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

        }

        if (!checkPhoneCallPermission()) {

            requesPhoneCallPermission();

        } else {

            Log.d("Permission granted", "Access call phone");

            //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerContentLayout = (LinearLayout) findViewById(R.id.navigation_content);

        initDrawerContent();

        LinearLayout headerLinearlayout = (LinearLayout) findViewById(R.id.headerlayout);
        headerLinearlayout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, Profile.class));
                drawer.closeDrawer(GravityCompat.START);

            }

        });
        profileImg = (ImageView) findViewById(R.id.profile_image);
        profileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*   Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
*/

            }
        });
        ImageView editlayout = (ImageView) findViewById(R.id.editProfile);
        editlayout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, Profile.class));
                drawer.closeDrawer(GravityCompat.START);

            }

        });

        SettingsUtils.init(this);
        home_email = (TextView)findViewById(R.id.home_email);
        home_name = (TextView)findViewById(R.id.home_name);
        UserEmail = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
        UserName = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        home_name.setText(UserName);
        home_email.setText(UserEmail);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
          //  super.onBackPressed();


            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


          /*  moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);

            Intent newIntent = new Intent(this, HomeActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(newIntent);  */
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {  //&& isNightModeToggled
            /*Intent a = new Intent(this,MainActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a); */

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

           // finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
     //   int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {


            getSupportActionBar().setTitle("Settings");
            Settings fragment = new Settings();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.commit();
           // navigationView.getMenu().getItem(9).setChecked(true);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

            return true;
        }
     */

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Myservices) {

            getSupportActionBar().setTitle("Services");
            SimpleFrag fragment = new SimpleFrag();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;


        }
        if (id == R.id.TrackyourVehicle) {
            getSupportActionBar().setTitle("Track Your Vehicle");

            startActivity(new Intent(HomeActivity.this, TrackVehicle.class));
        } else if (id == R.id.nav_rateapp) {
            RateThisApp();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                profileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void initDrawerContent() {
        mDrawerContentLayout.removeAllViews();
        for (int i = 0; i < 14; i++) {
            switch (i) {
                case 13:
                    View naviMenu = getLayoutInflater().inflate(R.layout.row_navigation_menu_logo, null, false);
                    mDrawerContentLayout.addView(naviMenu);
                    break;
                case 1:
                    View collapsableview = getLayoutInflater().inflate(R.layout.row_item_collapsible, null, false);
                    final LinearLayout mSubContent = (LinearLayout) collapsableview.findViewById(R.id.layuout_subcontent);
                    final TextView txtCollapseTitle = (TextView) collapsableview.findViewById(R.id.textView_title);
                    final ImageView imgLogo = (ImageView) collapsableview.findViewById(R.id.imageView_logo);
                    final ImageView imgDropdown = (ImageView) collapsableview.findViewById(R.id.imageView_dropdown);
                    imgLogo.setImageResource(R.drawable.my_service);
                    imgDropdown.setImageResource(R.drawable.ic_dropdown);
                    txtCollapseTitle.setText("My services");
                    final View Collapse = (View) ((RelativeLayout) collapsableview.findViewById(R.id.maincontent_layout));
                    collapsableview.setTag(0);
                    collapsableview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setNormalMainMenu();
                            if (v.getTag().equals(0)) {
                                Collapse.setBackgroundResource(R.drawable.shape_list_selected);
                                imgLogo.setImageResource(R.drawable.my_service_selected);
                                imgDropdown.setImageResource(R.drawable.ic_dropdown_selected);
                                txtCollapseTitle.setTextColor(Color.parseColor("#c42525"));
                                v.setTag(1);
                                mSubContent.setVisibility(View.VISIBLE);
                            } else {
                                Collapse.setBackgroundResource(R.drawable.shape_list_normal);
                                imgLogo.setImageResource(R.drawable.my_service);
                                imgDropdown.setImageResource(R.drawable.ic_dropdown);
                                txtCollapseTitle.setTextColor(Color.parseColor("#616161"));
                                v.setTag(0);
                                mSubContent.setVisibility(View.GONE);
                                setNormalSubView();
                            }
                        }
                    });

                    for (int j = 0; j < 2; j++) {
                        View collapsableSubView = getLayoutInflater().inflate(R.layout.row_drawer_item_normal, null, false);
                        final TextView txtTitle = (TextView) collapsableSubView.findViewById(R.id.textView2);
                        final ImageView imgSubLogo = (ImageView) collapsableSubView.findViewById(R.id.imageView_logo);
                        final View SubCollapse = collapsableSubView;
                        final int k = j;
                        switch (j) {
                           /* case 0:
                                imgSubLogo.setImageResource(R.drawable.add_vehicle);
                                txtTitle.setText("Add your vehicles");

                                collapsableSubView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imgSubLogo.setImageResource(R.drawable.add_vehicle_selectred);
                                        setSelectedSubView(SubCollapse, imgSubLogo, txtTitle, k);
                                        getSupportActionBar().setTitle("Add your Vehicles");
                                        AddVehicle fragment = new AddVehicle();
                                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.frame, fragment);
                                        fragmentTransaction.commit();
                                        closeNavigation();

                                    }
                                });
                                break; */
                            case 0:
                                imgSubLogo.setImageResource(R.drawable.schedule_slot);
                                txtTitle.setText("Reschedule slot");
                                collapsableSubView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imgSubLogo.setImageResource(R.drawable.schedule_slot_selected);
                                        setSelectedSubView(SubCollapse, imgSubLogo, txtTitle, k);
                                        startActivity(new Intent(HomeActivity.this, RescheduleSlotList.class));
                                        closeNavigation();

                                    }
                                });
                                break;
                            case 1:
                                imgSubLogo.setImageResource(R.drawable.cancel_service);
                                txtTitle.setText("Cancel services");
                                collapsableSubView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        imgSubLogo.setImageResource(R.drawable.cancel_service_selected);
                                        setSelectedSubView(SubCollapse, imgSubLogo, txtTitle, k);
                                        getSupportActionBar().setTitle("Cancel Services");
                                        CancelServiceList fragment = new CancelServiceList();
                                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.frame, fragment);
                                        fragmentTransaction.commit();
                                        closeNavigation();

                                    }
                                });

                                break;
                        }

                        mSubContent.addView(collapsableSubView);
                    }
                    mDrawerContentLayout.addView(collapsableview);
                    break;
                case 3:
                case 6:
                    View header = getLayoutInflater().inflate(R.layout.row_navigvation_header_view, null, false);
                    TextView txtTitle = (TextView) header.findViewById(R.id.textView_title);
                    switch (i) {
                        case 3:
                            txtTitle.setText("HELP");
                            break;
                        case 6:
                            txtTitle.setText("MORE");
                            break;
                    }
                    mDrawerContentLayout.addView(header);
                    break;
                default:
                    View collapsableMenuView = getLayoutInflater().inflate(R.layout.row_drawer_item_normal, null, false);
                    final TextView txtMenuTitle = (TextView) collapsableMenuView.findViewById(R.id.textView2);
                    final ImageView imgMenuLogo = (ImageView) collapsableMenuView.findViewById(R.id.imageView_logo);
                    final View menuV = collapsableMenuView;
                    final int mkey = i;
                    switch (i) {
                        case 0:
                            txtMenuTitle.setText("Book my services");
                            imgMenuLogo.setImageResource(R.drawable.book_my_service);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.book_my_service_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Book My Services");
                                    BookMyServices fragment = new BookMyServices();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    closeNavigation();

                                }
                            });
                            collapsableMenuView.performClick();
                            break;

                        case 2:
                            txtMenuTitle.setText("History");
                            imgMenuLogo.setImageResource(R.drawable.history);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.history_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
                                    closeNavigation();

                                }
                            });
                            break;
                     /*   case 3:
                            txtMenuTitle.setText("Payment");
                        imgMenuLogo.setImageResource(R.drawable.payment);
                        collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                imgMenuLogo.setImageResource(R.drawable.payment_selected);
                                setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                getSupportActionBar().setTitle("Payment");
                                Payment fragment = new Payment();
                                android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.frame, fragment);
                                fragmentTransaction.commit();
                                closeNavigation();

                            }
                        });

                        break;
                        case 3:
                            txtMenuTitle.setText("Track Your Vehicle");
                            imgMenuLogo.setImageResource(R.drawable.my_service);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.my_service_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    startActivity(new Intent(HomeActivity.this, TrackVehicle.class));
                                    closeNavigation();

                                }
                            });

                            break;
                        case 6:
                            txtMenuTitle.setText("Emergency Assistant");
                            imgMenuLogo.setImageResource(R.drawable.emergency);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.emergency_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Emergency Assistant");
                                    EmergencyAssistant fragment = new EmergencyAssistant();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;
                            */
                        case 4:
                            txtMenuTitle.setText("Call support");
                            imgMenuLogo.setImageResource(R.drawable.call_support);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.call_support_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Call Support");

                                    if ( Build.VERSION.SDK_INT >= 23 &&
                                            ContextCompat.checkSelfPermission( HomeActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    }

                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:"+"+91 9663544674"));
                                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(callIntent);


                                  /*  Content fragment = new Content();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "callsupport");
                                    */

                                    closeNavigation();

                                }
                            });
                            break;
                     /*   case 8:
                            txtMenuTitle.setText("Settings");
                            imgMenuLogo.setImageResource(R.drawable.settings);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.settings_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Settings");
                                    Settings fragment = new Settings();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;
                            */
                        case 5:
                            txtMenuTitle.setText("Feedback");
                            imgMenuLogo.setImageResource(R.drawable.feedback);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.feedback_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Feedback");
                                    Feedback fragment = new Feedback();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;
                    /*    case 11:
                            txtMenuTitle.setText("Rate this app");
                            imgMenuLogo.setImageResource(R.drawable.rating_normal);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.rating_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    RateThisApp();

                                    closeNavigation();

                                }
                            });
                            break;
                            */
                        case 7:
                            txtMenuTitle.setText("How it works");
                            imgMenuLogo.setImageResource(R.drawable.how_it_work);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.how_it_work_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("How it works");
                                    HowItWorks fragment = new HowItWorks();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;
                        case 8:
                            txtMenuTitle.setText("FAQ");
                            imgMenuLogo.setImageResource(R.drawable.faq);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.faq_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("FAQ");
                                    FAQfragment fragment = new FAQfragment();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    closeNavigation();

                                }
                            });
                            break;
                        case 9:
                            txtMenuTitle.setText("Terms & Conditions");
                            imgMenuLogo.setImageResource(R.drawable.terms_condition);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.terms_condition_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Terms & Conditions");
                                    Content fragment = new Content();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "terms");

                                    closeNavigation();

                                }
                            });
                            break;
                        case 10:
                            txtMenuTitle.setText("Privacy policy");
                            imgMenuLogo.setImageResource(R.drawable.privacy);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.privacy_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Privacy policy");
                                    Content fragment = new Content();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "privacypolicy");


                                    closeNavigation();

                                }
                            });
                            break;
                        case 11:
                            txtMenuTitle.setText("About");
                            imgMenuLogo.setImageResource(R.drawable.about);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.about_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("About");
                                    Content fragment = new Content();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CONTENT, "aboutus");

                                    closeNavigation();
                                }
                            });
                            break;

                        case 12:
                            txtMenuTitle.setText("Signout");
                            imgMenuLogo.setImageResource(R.drawable.settings);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.settings_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Signout");
                                    Signout fragment = new Signout();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;

                    }
                    mDrawerContentLayout.addView(collapsableMenuView);
                    break;

            }
        }


    }

    public void closeNavigation() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void setSelectedView(View parent, ImageView imgLogo, TextView txtTitle, int key) {
        parent.setBackgroundResource(R.drawable.shape_list_selected);
        txtTitle.setTextColor(Color.parseColor("#c42525"));
        if (parent!=previousParentView)
             setNormalMainMenu();

        previousImgLogo = imgLogo;
        previousTxtTitle = txtTitle;
        previousParentView = parent;
        parentPreviousKey = key;
    }

    public void setSelectedSubView(View parent, ImageView imgLogo, TextView txtTitle, int key) {
        parent.setBackgroundResource(R.drawable.shape_list_selected);
        txtTitle.setTextColor(Color.parseColor("#c42525"));
        if (parent!=previousSubParentView)
               setNormalSubView();

        previousSubParentView = parent;
        previousSubImgLogo = imgLogo;
        previousSubTxtTitle = txtTitle;
        previouskey = key;

    }

    public void setNormalSubView() {

        if (previousSubParentView != null) {
            previousSubParentView.setBackgroundResource(R.drawable.shape_submenu_normal);
            switch (previouskey) {
            /*    case 0:
                    if (previousSubParentView != null) {
                        previousSubParentView.setBackgroundResource(R.drawable.shape_submenu_normal);
                        previousSubImgLogo.setImageResource(R.drawable.add_vehicle);
                        previousSubTxtTitle.setTextColor(Color.parseColor("#616161"));
                    }
                    break; */
                case 0:
                    if (previousSubParentView != null) {
                        previousSubParentView.setBackgroundResource(R.drawable.shape_submenu_normal);
                        previousSubImgLogo.setImageResource(R.drawable.reschdule_slot);
                        previousSubTxtTitle.setTextColor(Color.parseColor("#616161"));
                    }
                    break;
                case 1:
                    if (previousSubParentView != null) {
                        previousSubParentView.setBackgroundResource(R.drawable.shape_submenu_normal);
                        previousSubImgLogo.setImageResource(R.drawable.cancel_service);
                        previousSubTxtTitle.setTextColor(Color.parseColor("#616161"));
                    }
                    break;
            }

        }

    }

    public void setNormalMainMenu() {
        switch (parentPreviousKey) {

            case 0:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.book_my_service);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 2:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.history);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
       /*     case 3:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.payment);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 4:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.history);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 6:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.emergency);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;   */
            case 4:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.call_support);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
        /*    case 8:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.settings);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;  */
            case 5:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.feedback);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
          /*  case 11:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.history);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;  */
            case 7:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.how_it_work);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 8:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.faq);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 9:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.terms_condition);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 10:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.privacy);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 11:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.about);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;

            case 12:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.settings);  // signout
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;

        }

    }

    /* Rate this app Dialog fragment */

    public void RateThisApp() {

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle("Custom Dialog");
        ImageView image = (ImageView) dialog.findViewById(R.id.ratingImg);
        image.setImageResource(R.drawable.rating);
        dialog.show();
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.cubeactive.qnotelistfree"));
                if (!MyStartActivity(intent)) {
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.cubeactive.qnotelistfree"));
                    if (!MyStartActivity(intent)) {
                        //   Toast.makeText(this, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_WExternalStorage_REQUEST_CODE:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;

            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }


    private boolean checkWriteExternalStoragePermission(){
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestWriteExternalStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            Toast.makeText(HomeActivity.this, "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_WExternalStorage_REQUEST_CODE);

        }
    }

    private boolean checkPhoneCallPermission(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requesPhoneCallPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)){

            Toast.makeText(HomeActivity.this, "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.CALL_PHONE},MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }
    }



}
