package com.goserwizz.customer.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.HomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 4/22/2016.
 */
public class RescheduleSlotDetail extends Fragment implements View.OnClickListener {

    private Toolbar toolbar;
    EditText r_reschedule_comments;
    TextView r_vehi_regno, r_vehi_servicetype, r_vehi_rescheduleto, r_service_dateon;
    Button RescheduleBtn;
    String regno, stype, sdateon, reschduleto, bookstatus, serviceid;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custDet.do";
    // JSON Node names
    private static final String TAG_S_ID = "s_id";
    private static final String TAG_DEALER_ID = "d_id";
    private static final String TAG_SDATE = "sdate";
    private static final String TAG_STIME = "stime";
    private static final String TAG_SCOMMENTS = "scmts";
    private static final String TAG_COMMENTS = "cmts";
    private static final String TAG_REASONS = "reasons";
    private static final String TAG_UPDATE_ID = "update_id";
    private static final String TAG_UPDATE_BY = "update_by";
    private static final String TAG_GUID = "gu_id";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";

    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";

    String status, resultmsg, dealer_id, s_date, s_time, gu_id, comments;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
            dealerState, dealerEmail, serviceId, addr, email, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
            paystatus, slotDate, dealerPerson, dealerMob;
    String v_dateon, v_reschduleto;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.reschedule_slot_detail, container, false);

        r_vehi_regno = (TextView) v.findViewById(R.id.resch_vehi_regno);
        r_vehi_servicetype = (TextView) v.findViewById(R.id.r_vehi_servicetype);
        r_service_dateon = (TextView) v.findViewById(R.id.r_service_dateon);
        r_vehi_rescheduleto = (TextView) v.findViewById(R.id.r_rescheduleto);
        r_reschedule_comments = (EditText) v.findViewById(R.id.r_reschedule_comments);
        RescheduleBtn = (Button) v.findViewById(R.id.ResheduleBtn);

        LinearLayout reschParent = (LinearLayout)v.findViewById(R.id.rescheduleParent);
        setupUI(reschParent);

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FRAGMENT, "reschedule");

        slotdetails();

        RescheduleBtn.setOnClickListener(this);
        r_reschedule_comments.setOnClickListener(this);

        return v;
    }

    public void slotdetails() {

        regno = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_REGNO, "");
        stype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_STYPE, "");
        bookstatus = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_SERVICEID, "");
        serviceid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_BOOKSTATUS, "");
        sdateon = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_S_DATETIME, "");
        reschduleto = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_R_DATETIME, "");


        r_vehi_regno.setText(regno);
        r_vehi_servicetype.setText(stype);
        r_service_dateon.setText(sdateon);
        r_vehi_rescheduleto.setText(reschduleto);

    }

    @Override
    public void onClick(View v) { // Parameter v stands for the view that was clicked.

        String dateValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        String timeValue =  SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");


        switch (v.getId()) {
            case R.id.ResheduleBtn:

                comments = r_reschedule_comments.getText().toString();

                v_dateon = r_service_dateon.getText().toString();
                v_reschduleto = r_vehi_rescheduleto.getText().toString();

                if(v_dateon.equals(v_reschduleto)) {

                    showResultDialog("Select Another Date and Time");
                }
                else if (!validateServiceComments()) {
                    return;
                }
                else {
                    Log.d("Reschedule datetime:", "YES");


                    if(Connectivity.isConnected(getActivity())) {

                        new JsonGetRescheduleService().execute();
                    }
                    else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }
                }



                break;

            case R.id.r_reschedule_comments:

                if(dateValue.equals("") || timeValue.equals("")) {

                    InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    showResultDialog("Select Date and Time");
                } else {

                }

                break;

        }

    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private boolean validateServiceComments() {
        if (r_reschedule_comments.getText().toString().trim().isEmpty()) {
            r_reschedule_comments.setError(getString(R.string.err_comments));
            requestFocus(r_reschedule_comments);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetRescheduleService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();


            regno = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_REGNO, "");
            stype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_STYPE, "");
            serviceid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_SERVICEID, "");
            bookstatus = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_BOOKSTATUS, "");
            sdateon = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_S_DATETIME, "");
            reschduleto = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_R_DATETIME, "");
            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");
            s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_SDATE, "");
            s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_RESCH_STIME, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_S_ID, serviceid));
            nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, s_date));
            nameValuePairs.add(new BasicNameValuePair(TAG_STIME, s_time));
            nameValuePairs.add(new BasicNameValuePair(TAG_STATUS, bookstatus));
            nameValuePairs.add(new BasicNameValuePair(TAG_SCOMMENTS, ""));
            nameValuePairs.add(new BasicNameValuePair(TAG_REASONS, "Other reasons"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COMMENTS, comments));
            nameValuePairs.add(new BasicNameValuePair(TAG_UPDATE_ID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_UPDATE_BY, "Customer"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "rescheduleservice"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure")) {
                        new JsonGetRescheduleService().execute();
                    }
                    else {

                        showResultDialogok(resultmsg);

                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetRescheduleService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            //  getActivity().finish();
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, "");
                        //    startActivity(new Intent(getActivity(), HomeActivity.class));

                            Intent newIntent = new Intent(getActivity(), HomeActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(newIntent);

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogok(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_TIME, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_SDATE, "");
                            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_RESCH_STIME, "");
                            startActivity(new Intent(getActivity(), HomeActivity.class));

                            getActivity().finish();
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .setCancelable(false)
                    .show();
        }
    }

}
