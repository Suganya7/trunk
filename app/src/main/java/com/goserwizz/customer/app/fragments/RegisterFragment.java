package com.goserwizz.customer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.MainActivity;
import com.goserwizz.customer.app.activities.PrivacyPolicy;
import com.goserwizz.customer.app.activities.TermsCondition;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RegisterFragment extends Fragment  {

    EditText firstName, lastName, email, mobile, password;
    LinearLayout signup;

    public RegisterFragment() {
        // Required empty public constructor
    }
    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/custLogin.do";
    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";

    String fname, lname, emailid, pwd, mobileNum;
    String status, digest, resultmsg, resultmsg1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_register, container, false);
        firstName = (EditText) view.findViewById(R.id.reg_firstname);
        lastName = (EditText) view.findViewById(R.id.reg_lastname);
        email = (EditText) view.findViewById(R.id.reg_email);
        mobile = (EditText) view.findViewById(R.id.reg_mobile);
        password = (EditText) view.findViewById(R.id.reg_pwd);
        signup = (LinearLayout) view.findViewById(R.id.SignupLayout);

        password.setTypeface( Typeface.DEFAULT );

        SettingsUtils.init(getActivity());
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_EMAIL, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_PWD, "");
      //  SettingsUtils.getInstance().clearValue(SettingsUtils.KEY_INPUT_EMAIL, "");

        firstName.setText("");
        lastName.setText("");
        email.setText("");
        mobile.setText("");
        password.setText("");

        firstName.addTextChangedListener(new MyTextWatcher(firstName));
        lastName.addTextChangedListener(new MyTextWatcher(lastName));
        email.addTextChangedListener(new MyTextWatcher(email));
        mobile.addTextChangedListener(new MyTextWatcher(mobile));
        password.addTextChangedListener(new MyTextWatcher(password));

      /*  String inputfname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_FNAME, "");
        String inputlname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_LNAME, "");
        String inputmobile = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_MOBILE, "");
        String inputemail = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_REGEMAIL, "");
        String inputpwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_INPUT_REGPWD, "");


        if(!inputfname.isEmpty()) {
            firstName.setText(inputfname);

        }
        if(!inputlname.isEmpty()) {
            lastName.setText(inputlname);

        }
        if(!inputmobile.isEmpty()) {
            mobile.setText(inputmobile);

        }
        if(!inputemail.isEmpty()) {
            email.setText(inputemail);

        }
        if(!inputpwd.isEmpty()) {
            password.setText(inputpwd);

        }

*/

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        SettingsUtils.init(getActivity());

        TextView terms = (TextView) view.findViewById(R.id.sagreeText1);
        TextView condition = (TextView) view.findViewById(R.id.sagreeText2);
        TextView privacypolicy = (TextView) view.findViewById(R.id.sagreeText4);

        terms.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans = (Spannable) terms.getText();
        ClickableSpan clickSpan = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans.setSpan(clickSpan, 0, spans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//
        condition.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans1 = (Spannable) condition.getText();
        ClickableSpan clickSpan1 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans1.setSpan(clickSpan1, 0, spans1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
        privacypolicy.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans2 = (Spannable) privacypolicy.getText();
        ClickableSpan clickSpan2 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {

             startActivity(new Intent(getActivity(), PrivacyPolicy.class));
            }
        };
        spans2.setSpan(clickSpan2, 0, spans2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        return view;
    }


    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateName()) {
            return;
        }
        if (!validateLastName()) {
            return;
        }
        if (!validateMobileNum()) {
            return;
        }
        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }


        if(Connectivity.isConnected(getActivity())) {

            getUserInfo();
            new GetRegisterData().execute();

        }
        else {
            Connectivity.showNoConnectionDialog(getActivity());
        }



      //  Toast.makeText(getActivity(), "Thank You for register!", Toast.LENGTH_SHORT).show();

      //  startActivity(new Intent(getActivity(), HomeActivity.class));
    }

    public void getUserInfo() {
        fname = firstName.getText().toString();
        lname = lastName.getText().toString();
        mobileNum = mobile.getText().toString();
        emailid = email.getText().toString();
        pwd = password.getText().toString();
    }

    private boolean validateName() {
        if (firstName.getText().toString().trim().isEmpty()) {
            firstName.setError(getString(R.string.err_msg_name));
            requestFocus(firstName);
            return false;
        } else {
           // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName() {
        if (lastName.getText().toString().trim().isEmpty()) {
            lastName.setError(getString(R.string.err_msg_lastname));
            requestFocus(lastName);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobileNum() {
        if (mobile.getText().toString().trim().isEmpty()) {
            mobile.setError(getString(R.string.err_msg_mobile));
            requestFocus(mobile);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String emailreg = email.getText().toString().trim();

        if (emailreg.isEmpty() || !isValidEmail(emailreg)) {
            email.setError(getString(R.string.err_msg_email));
            requestFocus(email);
            return false;
        } else {
            //email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            password.setError(getString(R.string.err_msg_password));
            requestFocus(password);
            return false;
        } else {
          //  password.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reg_firstname:
                    validateName();

                    String input_fname = firstName.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_FNAME, input_fname);

                    break;
                case R.id.reg_lastname:
                    validateLastName();

                    String input_lname = lastName.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_LNAME, input_lname);


                    break;
                case R.id.reg_mobile:
                    validateMobileNum();

                    String input_mobile = mobile.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_MOBILE, input_mobile);

                    break;
                case R.id.reg_email:
                    validateEmail();

                    String input_email = email.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGEMAIL, input_email);

                    break;
                case R.id.reg_pwd:
                    validatePassword();

                    String input_pwd = password.getText().toString();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGPWD, input_pwd);

                    break;
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetRegisterData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_FNAME, fname));
            nameValuePairs.add(new BasicNameValuePair(TAG_LNAME, lname));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, emailid));
            nameValuePairs.add(new BasicNameValuePair(TAG_MOBILE, mobileNum));
            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signup"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");
                        digest = object.getString(TAG_DIGEST);

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DIGEST, digest);
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                    new GetRegisterData().execute();
                }
                else {

                   // new GetRegisterVerfify().execute();
                  //  showResultDialog(resultmsg);

                    showResultDialogN("Please verify your registered mail id to access goserwizz app.");

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_FNAME, "");
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_LNAME, "");
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_MOBILE, "");
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGEMAIL, "");
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGPWD, "");
                }


            }
            else if(result.equals("fail")) {
                if (resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                    new GetRegisterData().execute();
                }
                else { showResultDialogFail(resultmsg); }
            }
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetRegisterVerfify extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Signup Verfification...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            digest = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DIGEST, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_DIGEST, digest));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, emailid));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signupverify"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg1 = jsonObj.getString(TAG_MSG);

                    }
                    else if(status.equals("fail")) {
                        resultmsg1 = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView (The last packet)
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg1.contains("Communications link failure") || resultmsg1.contains("The last packet successfully")) {
                        new GetRegisterVerfify().execute();
                    }
                    else {
                        showResultDialogN("Please verify your registered mail id to access goserwizz app.");

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_FNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_LNAME, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_MOBILE, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGEMAIL, "");
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_INPUT_REGPWD, "");

                       // showResultDialogN(resultmsg1);
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg1.contains("Communications link failure") || resultmsg1.contains("The last packet successfully")) {
                        new GetRegisterVerfify().execute();
                    }
                    else { showResultDialogFail(resultmsg1); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again" );
            }

        }
    }

    private void showResultDialogN(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_register_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                          //  listener.onConnectionChecked(result);
                        firstName.setText("");
                        lastName.setText("");
                        email.setText("");
                        mobile.setText("");
                        password.setText("");

                        new GetRegisterVerfify().execute();

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

}
