package com.goserwizz.customer.app.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.activities.RescheduleDateTime;

/**
 * Created by Kaptas on 3/11/2016.
 */


public class RescheduleSlot extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
   // private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_tab_call, R.drawable.ic_tab_contacts,
    };

    SharedPreferences prefDate, DbleTapTime;
    SharedPreferences.Editor edit;

    private ViewPager viewPager;
    private ImageView mdateImage;
    private ImageView mtimeImage;

    private ViewPager mViewPager;
    private LinearLayout mdateLay;
    private LinearLayout mtimeLays;
    TextView date, time;
    View view1, view2;
    GestureDetector gestureDetector;
    String dateValue = "DATE", s_date = "Date", s_time = "Time";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedule_slot);

        prefDate = getSharedPreferences("S_Date", MODE_PRIVATE);
        dateValue = prefDate.getString("date", null);

        DbleTapTime = getSharedPreferences("time", MODE_PRIVATE);
        edit = DbleTapTime.edit();

        SettingsUtils.init(this);
        s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");


        toolbar = (Toolbar)findViewById(R.id.Rescheduletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reschedule Slot");

        mdateImage = (ImageView) findViewById(R.id.date_img);
        mtimeImage = (ImageView) findViewById(R.id.time_img);
        date = (TextView) findViewById(R.id.dateText);
        time = (TextView) findViewById(R.id.timeText);
        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);

        mdateLay = (LinearLayout) findViewById(R.id.date_lay1);
        mtimeLays = (LinearLayout) findViewById(R.id.time_lay1);

        mdateLay.setOnClickListener(this);
        mtimeLays.setOnClickListener(this);

        if(s_date.isEmpty() || s_date.equals("")) {
            date.setText("Date");
        }
        else {
            date.setText(s_date);
        }
        if(s_time.isEmpty() || s_time.equals("")) {
            time.setText("Time");
        }
        else {
            time.setText(s_time);
        }


        mdateLay.setOnTouchListener(
                new LinearLayout.OnTouchListener() {
                    public boolean onTouch(View v,
                                           MotionEvent event) {
                        if (gestureDetector.onTouchEvent(event)) {
                            Log.v("tag", "screen touched");

                            return true;
                        } else {

                            edit.putString("time", "date");
                            edit.commit();

                            return false;
                        }
                    }
                }
        );

        mtimeLays.setOnTouchListener(
                new LinearLayout.OnTouchListener() {
                    public boolean onTouch(View v,
                                           MotionEvent event) {
                        if (gestureDetector.onTouchEvent(event)) {
                            Log.v("tag", "screen touched");

                            return true;
                        } else {

                            edit.putString("time", "time");
                            edit.commit();

                            return false;
                        }
                    }
                }
        );


        gestureDetector = new GestureDetector(new MyGestureDetector());


        mdateImage.setBackgroundResource(R.drawable.date1);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    float elevation = 2;
                    mdateImage.setBackgroundResource(R.drawable.date1);
                    mtimeImage.setBackgroundResource(R.drawable.time1);
                    date.setTextColor(Color.WHITE);
                    time.setTextColor(Color.parseColor("#ff9999"));
                    view1.setBackgroundColor(Color.WHITE);
                    view1.setElevation(elevation);
                    view2.setBackgroundColor(Color.parseColor("#cc0000"));

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }

                } else if (position == 1) {

                    float elevation = 2;
                    mdateImage.setBackgroundResource(R.drawable.date2);
                    mtimeImage.setBackgroundResource(R.drawable.time2);
                    date.setTextColor(Color.parseColor("#ff9999"));
                    time.setTextColor(Color.WHITE);
                    view1.setBackgroundColor(Color.parseColor("#cc0000"));
                    view2.setBackgroundColor(Color.WHITE);
                    view2.setElevation(elevation);

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    public class ViewPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {

            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("viewpager:");
            switch (position) {
                case 0:
                    RescheduleSlotDetail mFrag1 = new RescheduleSlotDetail();
                    return mFrag1;

                case 1:
                    TimeFragment mFrag2 = new TimeFragment();
                    return mFrag2;

                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new Fragment();
                    return fragment;
            }

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.date_lay1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.time_lay1:
                mViewPager.setCurrentItem(1);
                break;

        }

    }

    public class MyGestureDetector extends GestureDetector.SimpleOnGestureListener implements GestureDetector.OnGestureListener {

        @Override
        public boolean onDoubleTap(MotionEvent e) {
                //  Toast.makeText(getBaseContext(), "onDoubleTapEvent", Toast.LENGTH_LONG).show();


            startActivity(new Intent(RescheduleSlot.this, RescheduleDateTime.class));

            return super.onDoubleTap(e);

        }
    }


    public boolean onTouch(View v, MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            Log.v("tag", "screen touched");
            return true;
        } else {
            return false;
        }
    }

    public void onBackPressed() {

        finish();
    }


    }
