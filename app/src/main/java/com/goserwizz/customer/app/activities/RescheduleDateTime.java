package com.goserwizz.customer.app.activities;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.fragments.DateFragment;
import com.goserwizz.customer.app.fragments.TimeFragment;

/**
 * Created by Kaptas on 4/15/2016.
 */
public class RescheduleDateTime extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_tab_call, R.drawable.ic_tab_contacts,
    };
    SharedPreferences prefDate, DbleTapTime;
    String dateValue = "DATE",  s_date = "Date", s_time = "Time";
    String timeChar = "null";

    SharedPreferences.Editor edit;

    private ImageView mdateImage;
    private ImageView mtimeImage;

    private ViewPager mViewPager;
    private LinearLayout mdateLay;
    private LinearLayout mtimeLays;
    TextView date, time;
    View view1, view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reschedule_slot);

        prefDate = getSharedPreferences("S_Date", MODE_PRIVATE);
        dateValue = prefDate.getString("date", null);

        DbleTapTime = getSharedPreferences("time", MODE_PRIVATE);
        timeChar = DbleTapTime.getString("time", "");

        SettingsUtils.init(this);
        s_date = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DATE, "");
        s_time = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_TIME, "");

        toolbar = (Toolbar) findViewById(R.id.Rescheduletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Date");

        mdateImage = (ImageView) findViewById(R.id.date_img);
        mtimeImage = (ImageView) findViewById(R.id.time_img);
        date = (TextView) findViewById(R.id.dateText);
        time = (TextView) findViewById(R.id.timeText);
        //  date.setText("date");

        if(s_date.isEmpty() || s_date.equals("")) {
            date.setText("Date");
        }
        else {
            date.setText(s_date);
        }
        if(s_time.isEmpty() || s_time.equals("")) {
            time.setText("Time");
        }
        else {
            time.setText(s_time);
        }



        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);

        mdateLay = (LinearLayout) findViewById(R.id.date_lay1);
        mtimeLays = (LinearLayout) findViewById(R.id.time_lay1);

        mdateLay.setOnClickListener(this);
        mtimeLays.setOnClickListener(this);

        mdateImage.setBackgroundResource(R.drawable.date1);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager
                .setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                    getSupportActionBar().setTitle("Date");
                    mdateImage.setBackgroundResource(R.drawable.date1);
                    mtimeImage.setBackgroundResource(R.drawable.time1);
                    date.setTextColor(Color.WHITE);
                    time.setTextColor(Color.parseColor("#ff9999"));
                    view1.setBackgroundColor(Color.WHITE);
                    view2.setBackgroundColor(Color.parseColor("#cc0000"));

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }

                } else if (position == 1) {

                    getSupportActionBar().setTitle("Time");
                    mdateImage.setBackgroundResource(R.drawable.date2);
                    mtimeImage.setBackgroundResource(R.drawable.time2);
                    date.setTextColor(Color.parseColor("#ff9999"));
                    time.setTextColor(Color.WHITE);
                    view1.setBackgroundColor(Color.parseColor("#cc0000"));
                    view2.setBackgroundColor(Color.WHITE);

                    if(s_date.isEmpty() || s_date.equals("")) {
                        date.setText("Date");
                    }
                    else {
                        date.setText(s_date);
                    }
                    if(s_time.isEmpty() || s_time.equals("")) {
                        time.setText("Time");
                    }
                    else {
                        time.setText(s_time);
                    }
                }
                mViewPager.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if(timeChar.equals("time")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Time");
            mViewPager.setCurrentItem(1);
        }
        else if(timeChar.equals("date")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Date");
            mViewPager.setCurrentItem(0);
        }

    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {

            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println("viewpager:");
            switch (position) {
                case 0:
                    DateFragment mFrag1 = new DateFragment();
                    return mFrag1;

                case 1:
                    TimeFragment mFrag2 = new TimeFragment();
                    return mFrag2;

                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new Fragment();
                    return fragment;
            }

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.date_lay1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.time_lay1:
                mViewPager.setCurrentItem(1);
                break;

        }

    }



  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {  //&& isNightModeToggled
            Intent a = new Intent(this,MainActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
 */
    public void showDate(String text) {
        TextView tv = (TextView) findViewById(R.id.dateText);

        tv.setText(text);
    }

    public void showTime(String text) {
        TextView tv = (TextView) findViewById(R.id.timeText);

        tv.setText(text);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // NavUtils.navigateUpFromSameTask(this);
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
