package com.goserwizz.customer.app.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.customer.app.Database.DBHandler;
import com.goserwizz.customer.app.ImageLoader.ImageLoader;
import com.goserwizz.customer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.customer.app.R;
import com.goserwizz.customer.app.Util.Connectivity;
import com.goserwizz.customer.app.Util.SettingsUtils;
import com.goserwizz.customer.app.Util.UserAddvehicle;
import com.goserwizz.customer.app.Views.CustomAutoCompleteView;
import com.goserwizz.customer.app.activities.ServiceCenter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 07-03-2016.
 */
public class BookMyServices extends Fragment {


    String[] vehiclemake = { "Make" };
    String[] vehiclemodel = { "Model" };
    String[] vehicleservicecenter = { "Select service center" };

    String[] modelBikeitems = { "Model", "FZ V 2.0",
            "YZF-R3", "FI V 2.0", "MT-09","Vmax" };

    Spinner spinner, vehicle, make, model, year;
    CustomAutoCompleteView CityName;
    Switch carswitch, bikeswitch, bodyshopswitch, serviceswitch;
    LinearLayout vehiLay2, vehiLay1, vehiLay3, vehiLay4;
    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url = "http://goserwizz.com/bookServ.do";

    private static String url2 = "http://goserwizz.com/custDet.do";

    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_MODE = "mode";
    private static final String TAG_STERM = "sterm", TAG_CITY ="city";
    private static final String TAG_VTYPE = "vtype", TAG_VMODEL = "vmodel";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";
    private static final String TAG_VMAKE = "vmake";

    private static final String TAG_GUID = "gu_id";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";
    private static final int PERMISSION_WExternalStorage_REQUEST_CODE = 2;

    String scarbikeSwitch;
    String status, digest, resultmsg;
    String sTermText, vmake_id, vmake_name, vmake_logo, vmodel_id, vmodel_name, dealer_id, dealer_city, dealer_comp_name, dealer_lat, dealer_long, dealer_addr;
    ArrayList<String> sTermCity, sVMakeName, sVMakeId, sVMakeLogo, sVModelId, sVModelName, sDealer_ID, sDealer_Name , sDealer_City, sDealer_lati, sDealer_long, sDealer_addr, sDealer_pinpoint;
    ArrayAdapter sTermCityAdapter;
    String[] cityArr, VMakeName= {"Make"}, VMakeId, VMakeLogo, VModelId, VModelName = {"Model"}, Dealer_ID, Dealer_Name, Dealer_City, Dealer_lati, Dealer_long;
    Button book_addVehiBtn;
    DBHandler db;
    List<String> db_vehileList;
    String[] vehileList = {"Select"};
    LinearLayout book_servicesLayout;
    RadioGroup service_radioGroup;
    RadioButton service_radioBtn, thirdservice_radioBtn;
    String fname, lname, mob, email, gu_id;
    String servicetype="";
    ImageView vehicle_logo;
    View viewType;

    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.book_my_services,container,false);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        RelativeLayout relativeLayout = (RelativeLayout)v.findViewById(R.id.ParentLay);
        setupUI(relativeLayout);


        SettingsUtils.init(getActivity());
        db = new DBHandler(getActivity());
        CityName = (CustomAutoCompleteView)v.findViewById(R.id.CityName);
        spinner = (Spinner)v.findViewById(R.id.selecttype);
        carswitch = (Switch)v.findViewById(R.id.CarSwitchBtn);
      //  bikeswitch = (Switch)v.findViewById(R.id.BikeswitchBtn);
        bodyshopswitch = (Switch)v.findViewById(R.id.bodyshopSwitch);
        serviceswitch = (Switch)v.findViewById(R.id.serviceSwitch);
        service_radioGroup = (RadioGroup)v.findViewById(R.id.service_RadioGroup);
        thirdservice_radioBtn = (RadioButton)v.findViewById(R.id.thirdservice_radioBtn);
        vehicle_logo = (ImageView)v.findViewById(R.id.vehicle_logo);
        viewType = (View)v.findViewById(R.id.viewType);

      //  vehiLay1 = (LinearLayout)v.findViewById(R.id.vehilay1);
        vehiLay2 = (LinearLayout)v.findViewById(R.id.vehiLay2);
        vehiLay3 = (LinearLayout)v.findViewById(R.id.vehiLay3);
        vehiLay4 = (LinearLayout)v.findViewById(R.id.vehiLay4);

       // vehicle = (Spinner)v.findViewById(R.id.spinnerVehicle);
        make = (Spinner)v.findViewById(R.id.spinnerMake);
        model = (Spinner)v.findViewById(R.id.spinnerModel);

        book_servicesLayout = (LinearLayout)v.findViewById(R.id.book_servicesLayout);

      //  book_addVehiBtn = (Button)v.findViewById(R.id.book_addVehiBtn);

        CityName.addTextChangedListener(new MyTextWatcher(CityName));
        sTermCity = new ArrayList<String>();
        sVMakeId = new ArrayList<String>();
        sVMakeName = new ArrayList<String>();
        sVMakeLogo = new ArrayList<String>();
        sVModelId = new ArrayList<String>();
        sVModelName = new ArrayList<String>();
        sDealer_ID = new ArrayList<String>();
        sDealer_Name = new ArrayList<String>();
        sDealer_City = new ArrayList<String>();
        sDealer_addr = new ArrayList<String>();
        sDealer_lati = new ArrayList<String>();
        sDealer_long = new ArrayList<String>();
        sDealer_pinpoint = new ArrayList<String>();

        db_vehileList = new ArrayList<String>();

        checkGPSStatus();

        sVMakeId.add("0");
        sVMakeName.add("Make");
        sVMakeLogo.add("0");
        sVModelId.add("0");
        sVModelName.add("Model");


        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));
        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));


       // db_vehileList.clear();
       // db_vehileList.add("Select");


     /*   carswitch.setChecked(true);
        if(carswitch.isChecked()) {
         //   bikeswitch.setChecked(false);
            scarbikeSwitch="Car";

            UserSwitchVehicle();

            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);
        }

        */


     /*   bikeswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    // carswitch.setEnabled(false);
                    carswitch.setChecked(false);
                    scarbikeSwitch = "Bike";

                    UserVehicleRegNo();
               //     new JsonGetVehicleMake().execute();

                    sVModelName.clear();

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                } else {

                    carswitch.setChecked(true);
                    scarbikeSwitch="Car";

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                }

            }
        });
*/

        carswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    //     bikeswitch.setChecked(false);
                    scarbikeSwitch = "Car";
                    UserSwitchVehicle();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                    if (!checkWriteExternalStoragePermission()) {

                        requestWriteExternalStoragePermission();

                    } else {

                        Log.d("Permission granted", "Access write external storage for sdcard");

                        //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

                    }


                } else {
                    make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, vehiclemake));
                    model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, vehiclemodel));
                    spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, vehicleservicecenter));
                }

       /*         else{

                    bikeswitch.setChecked(true);
                    scarbikeSwitch="Bike";

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, scarbikeSwitch);

                }
                */

            }
        });

        bodyshopswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, "Body Repair");
                    servicetype = "Body Repair";
                    serviceswitch.setChecked(false);
                    book_servicesLayout.setVisibility(View.GONE);
                    viewType.setVisibility(View.GONE);


                } else {

                    serviceswitch.setChecked(true);
                    book_servicesLayout.setVisibility(View.VISIBLE);
                    viewType.setVisibility(View.VISIBLE);

                    //***** Added on May 14th 2016*****//
                    if(servicetype.equals("Body Repair")) {
                        servicetype = "";
                    }

                }

            }
        });

        service_radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int selectedId = service_radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                service_radioBtn = (RadioButton)group.findViewById(selectedId);

             //   SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, service_radioBtn.getText().toString());

                servicetype = service_radioBtn.getText().toString();

                if(servicetype.equals("Running Repairs")) {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, "Running Service");
                    servicetype = "Running Service";
                }
                else {
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICE_CATEGORY, service_radioBtn.getText().toString());

                }

                // added on may10
                String dealername = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
                if(dealername!= null ) {

                   if(sTermText!= null) {
                       String city = sTermText;
                       String city2 = city;

                       spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));
                    }


                   // spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));
                 /*   if (Connectivity.isConnected(getActivity())) {

                        new JsonGetDealerByModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }  */
                }

                //added on may10

              //  Toast.makeText(getActivity(),service_radioBtn.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        serviceswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {

                    bodyshopswitch.setChecked(false);
                    book_servicesLayout.setVisibility(View.VISIBLE);

                   String nissan = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKE, "");
                    String vmake = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
                    if(vmake.equals("11") || vmake.equals("19")) {

                        thirdservice_radioBtn.setVisibility(View.GONE);
                        viewType.setVisibility(View.GONE);
                    }
                    else {
                        thirdservice_radioBtn.setVisibility(View.VISIBLE);
                        viewType.setVisibility(View.VISIBLE);

                    }


                } else {


                    bodyshopswitch.setChecked(true);
                    book_servicesLayout.setVisibility(View.GONE);
                }

                // added on may9

             /*   String dealername = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DEALER_NAME, "");
                if(!dealername.isEmpty()) {
                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetDealerByModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }
                }  */

            }
        });


        CityName.setThreshold(1);
      /*  ArrayAdapter adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,cityname);
        CityName.setAdapter(adapter);
        */

      /*  vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
               // System.out.println("it works...   " + item);

                if (item.equals("Select")) {
                    System.out.println("it works...   " + item);
                } else {
                    System.out.println("it works...   " + item);

                    String vmodel = db.getVehicleModelID(item);
                    String mileage = db.getVehicleMileage(item);
                    String modelname = db.getVehicleModel(item);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELid, vmodel);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEH_REGNO, item);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEH_MILEAGE, mileage);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VEH_MODEL, modelname);
                    System.out.println("vmodel ID...   " + vmodel);

                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetDealerByModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
*/

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = parent.getItemAtPosition(pos).toString();
                //  System.out.println("it works...   " + item);


                if (item.equals("Select service center")) {
                    System.out.println("it works...   " + item);


                } else {
                    System.out.println("it works...   " + item);

                    if (!servicetype.equals("")) {

                        startActivity(new Intent(getActivity(), ServiceCenter.class));

                        String d_id = sDealer_ID.get(pos).toString();
                        String d_city = sDealer_City.get(pos).toString();

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALERID, d_id);
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_CITY, d_city);
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_NAME, sDealer_Name.get(pos).toString());
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_ADDR, sDealer_addr.get(pos).toString());
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_LAT, sDealer_lati.get(pos).toString());
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_LONG, sDealer_long.get(pos).toString());
                    } else {
                        showResultDialogFailed("Select your service type");
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

     /*   book_addVehiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((HomeActivity) getActivity()).getSupportActionBar().setTitle("Add your Vehicles");

                AddVehicle fragment = new AddVehicle();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.commit();

            }
        });  */

        make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                System.out.println("it works...   " + item);

                if (item.equals("Make")) {

                    System.out.println("it works...   " + item);

                    vehicle_logo.setImageResource(R.drawable.circle);
                } else {
                    System.out.println("it works...   " + item);

                    //   model.notify();
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKEid, sVMakeId.get(position));
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKE, sVMakeName.get(position));

                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetVehicleModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }


                    ///**** Set Logo for the imageview near make spinner ****///
                    String logo = sVMakeLogo.get(position);

                    ImageLoader imgLoader = new ImageLoader(getActivity());
                    int loader = R.drawable.circle;
                    imgLoader.DisplayImage(logo, loader, vehicle_logo);

                    /* Hide Third Free Service for Nissan only */
                    String nissan = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKE, "");
                    if(nissan.equals("Nissan")) {

                        thirdservice_radioBtn.setVisibility(View.GONE);
                    }
                    else {
                        thirdservice_radioBtn.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();
                System.out.println("it works...   " + item);

                if (item.equals("Model")) {
                    System.out.println("it works...   " + item);
                } else {
                    System.out.println("it works...   " + item);

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELid, sVModelId.get(position));
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODEL, sVModelName.get(position));

                    String model = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODEL, "");

                    if (Connectivity.isConnected(getActivity())) {

                        new JsonGetDealerByModel().execute();
                    } else {
                        Connectivity.showNoConnectionDialog(getActivity());
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // get user details
        if(getActivity() != null) {

            if (Connectivity.isConnected(getActivity())) {

                new JsonGetUserDetails().execute();

            } else {
                Connectivity.showNoConnectionDialog(getActivity());
            }
        }

        return v;
    }

    public void UserSwitchVehicle() {
        sVMakeId.clear();
        sVMakeName.clear();
        sVMakeLogo.clear();
        sVModelId.clear();
        sVModelName.clear();
        sVMakeId.add("0");
        sVMakeName.add("Make");
        sVMakeLogo.add("0");
        sVModelId.add("0");
        sVModelName.add("Model");

        sDealer_ID.clear();
        sDealer_Name.clear();
        sDealer_lati.clear();
        sDealer_long.clear();
        sDealer_City.clear();
        sDealer_addr.clear();
        sDealer_pinpoint.clear();

        sDealer_ID.add("0");
        sDealer_Name.add("Select service center");
        sDealer_lati.add("0");
        sDealer_long.add("0");
        sDealer_City.add("0");
        sDealer_addr.add("0");
        sDealer_pinpoint.add("0");


        VMakeName = new String[sVMakeName.size()];
        VMakeName = sVMakeName.toArray(VMakeName);

        VModelName = new String[sVModelName.size()];
        VModelName = sVModelName.toArray(VModelName);

        Dealer_Name = new String[sDealer_Name.size()];
        Dealer_Name = sDealer_Name.toArray(Dealer_Name);

        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));
        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));
        spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));

        if (Connectivity.isConnected(getActivity())) {

            new JsonGetVehicleMake().execute();

        } else {
            Connectivity.showNoConnectionDialog(getActivity());
        }
    }


    public void UserVehicleRegNo() {

        // Reading all Vehicles
        db_vehileList.clear();
        db_vehileList.add("Select");
        Log.d("Reading: ", "Reading all vehicles..");
        List<UserAddvehicle> Vehicles = db.getVehiclesByType(scarbikeSwitch);

        for (UserAddvehicle vehi : Vehicles) {
            String log = "Id: " + vehi.getId() + " ,Name: " + vehi.getVmake()+ " ,modelid: " + vehi.getVmodelId() + vehi.getVmakeId() + " , name: email and mob " + vehi.getName() +" , "+vehi.getEmail()+  " , " + vehi.getMobile();
            // Writing shops  to log
            Log.d("UserAddvehicle: : ", log);
            Log.d("all item:", Vehicles + "");
            Log.d("register item:", vehi.getVregno().toString() + "");

            db_vehileList.add(vehi.getVregno().toString());
        }
        vehileList = new String[db_vehileList.size()];
        vehileList = db_vehileList.toArray(vehileList);
        vehicle.setAdapter(new MyAdapter(getActivity(), R.layout.spinner_row, vehileList));
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.CityName:

                    if(!CityName.getText().toString().trim().isEmpty()) {
                        int citysize = CityName.getText().toString().trim().length();
                        if (citysize == 1) {

                            sTermText = CityName.getText().toString();

                            carswitch.setChecked(false); // newly addedon may11

                            if (Connectivity.isConnected(getActivity())) {

                                new JsonGetDealerCity().execute();
                            } else {
                                Connectivity.showNoConnectionDialog(getActivity());
                            }
                        }
                    }

                    break;

            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


    // Adapter class for spinner control
    public class MyAdapter extends ArrayAdapter<String> {

        public MyAdapter(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(vehileList[position]);
            }
            else {
                label.setText(vehileList[position]);
            }

            return row;
        }
    }

    // Adapter class for spinner control
    public class MyAdapterMake extends ArrayAdapter<String> {

        public MyAdapterMake(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(VMakeName[position]);
            }
            else {
                label.setText(vehiclemake[position]);
            }

            return row;
        }
    }

    // Adapter class for spinner control
    public class MyAdapterModel extends ArrayAdapter<String> {

        public MyAdapterModel(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(VModelName[position]);

            }
            else {
                label.setText(vehiclemodel[position]);
            }

            return row;
        }
    }


    // Adapter class for spinner control
    public class MyAdapterServiceType extends ArrayAdapter<String> {

        public MyAdapterServiceType(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getActivity().getLayoutInflater();
            View row=inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label=(TextView)row.findViewById(R.id.selectVehicle);

            if(carswitch.isChecked()) {
                label.setText(Dealer_Name[position]);

            }
            else {
                label.setText(vehicleservicecenter[position]);
            }

            return row;
        }
    }

    private void checkGPSStatus() {
        LocationManager locationManager = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (locationManager == null) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage("Enable GPS Location");
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //this will navigate user to the device location settings screen
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        }
        else {

        }
    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetDealerCity extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
       /*     pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();  */
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            sTermCity.clear();

            digest = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_DIGEST, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_STERM, sTermText));
            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, "Car")); // scarbikeSwitch
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getdealercity"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);

            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                JSONObject jObj = new JSONObject(jsonStr.trim());
                Iterator<?> jObj_Keys = jObj.keys();

                status = jsonObj.getString(TAG_STATUS);
                if(status.equals("success")) {

                    resultmsg = jsonObj.getString(TAG_MSG);

                    while(jObj_Keys.hasNext() ) {
                        String jObj_key = (String)jObj_Keys.next();

                        if(jObj_key.equals("data")){
                            JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                            Iterator<?> dataObj_keys = dataObj.keys();

                            while(dataObj_keys.hasNext() ) {
                                String dataObj_key = (String)dataObj_keys.next();

                                JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                tmpObj.get("value").toString();
                                tmpObj.get("info").toString();
                                sTermCity.add(tmpObj.get("value").toString());

                             //   SettingsUtils.getInstance().putValue(SettingsUtils.KEY_CITY, tmpObj.get("value").toString());

                                System.out.println(tmpObj.get("value"));
                                System.out.println(tmpObj.get("info"));
                            }
                        }
                    }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
         /*   if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetDealerCity().execute();
                    }
                    else {

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        cityArr = new String[sTermCity.size()];
                        cityArr = sTermCity.toArray(cityArr);
                        sTermCityAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, cityArr);
                        CityName.setAdapter(sTermCityAdapter);
                        sTermCityAdapter.notifyDataSetChanged();
                    }
                });

                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {
                        new JsonGetDealerCity().execute();
                    }
                    else { showResultDialogFail(resultmsg);
                    }
                }

            } else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetVehicleMake extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

            sTermText = CityName.getText().toString();
            sTermText = sTermText.toLowerCase();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, scarbikeSwitch));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, sTermText));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getvehiclemake"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sVMakeId.clear();
                    sVMakeName.clear();
                    sVMakeLogo.clear();

                    sVMakeId.add("0");
                    sVMakeName.add("Make");
                    sVMakeLogo.add("0");

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    vmake_id = tmpObj.get("id").toString();
                                    vmake_name = tmpObj.get("name").toString();
                                    vmake_logo = tmpObj.get("logo").toString();

                                    sVMakeId.add(vmake_id);
                                    sVMakeName.add(vmake_name);
                                    sVMakeLogo.add("http://goserwizz.com/uploads/" + vmake_logo);

                                    System.out.println(tmpObj.get("id"));
                                    System.out.println(tmpObj.get("name"));
                                    System.out.println(tmpObj.get("logo"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetVehicleMake().execute();
                    }
                    else {
                        VMakeName = new String[sVMakeName.size()];
                        VMakeName = sVMakeName.toArray(VMakeName);

                        make.setAdapter(new MyAdapterMake(getActivity(), R.layout.spinner_row, VMakeName));

                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetVehicleMake().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetVehicleModel extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            String vmake = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
        //    String city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VMAKE, vmake));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, sTermText));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getvmodel"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sVModelId.clear();
                    sVModelName.clear();

                    sVModelId.add("0");
                    sVModelName.add("Model");
                    HashMap<String,String> map = new HashMap<String,String>();


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    String value = dataObj.getString(dataObj_key);
                                    map.put(dataObj_key,value);

                                    vmodel_id = dataObj_key;
                                    vmodel_name = value;

                                    sVModelName.add(vmodel_name);
                                    sVModelId.add(vmodel_id);
                                    System.out.println(value);
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetVehicleModel().execute();
                    }
                    else {

                        VModelName = new String[sVModelName.size()];
                        VModelName = sVModelName.toArray(VModelName);
                        model.setAdapter(new MyAdapterModel(getActivity(), R.layout.spinner_row, VModelName));

                        /* Make Service center values to be empty */

                        sDealer_ID.clear();
                        sDealer_Name.clear();
                        sDealer_lati.clear();
                        sDealer_long.clear();
                        sDealer_City.clear();
                        sDealer_addr.clear();
                        sDealer_pinpoint.clear();

                        sDealer_ID.add("0");
                        sDealer_Name.add("Select service center");
                        sDealer_lati.add("0");
                        sDealer_long.add("0");
                        sDealer_City.add("0");
                        sDealer_addr.add("0");
                        sDealer_pinpoint.add("0");

                        Dealer_Name = new String[sDealer_Name.size()];
                        Dealer_Name = sDealer_Name.toArray(Dealer_Name);

                        spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));

                         /* Make Service center values to be empty */

                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetVehicleModel().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetDealerByModel extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            String vtype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");
       //     String city = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_CITY, "");
            String vmodel = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELid, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, vtype));
            nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
            nameValuePairs.add(new BasicNameValuePair(TAG_CITY, sTermText));
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "10"));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getdealerbyvmodel"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);
                    sDealer_ID.clear();
                    sDealer_Name.clear();
                    sDealer_lati.clear();
                    sDealer_long.clear();
                    sDealer_City.clear();
                    sDealer_addr.clear();
                    sDealer_pinpoint.clear();

                    sDealer_ID.add("0");
                    sDealer_Name.add("Select service center");
                    sDealer_lati.add("0");
                    sDealer_long.add("0");
                    sDealer_City.add("0");
                    sDealer_addr.add("0");
                    sDealer_pinpoint.add("0");

                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while(dataObj_keys.hasNext() ) {
                                    String dataObj_key = (String)dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    dealer_comp_name = tmpObj.get("compName").toString();
                                    dealer_id = tmpObj.get("d_id").toString();
                                    dealer_lat = tmpObj.get("lat").toString();
                                    dealer_long = tmpObj.get("long").toString();
                                    dealer_city = tmpObj.get("city").toString();
                                    dealer_addr = tmpObj.get("addr").toString();

                                    sDealer_ID.add(dealer_id);
                                    sDealer_Name.add(dealer_comp_name);
                                    sDealer_lati.add(dealer_lat);
                                    sDealer_long.add(dealer_long);
                                    sDealer_City.add(dealer_city);
                                    sDealer_addr.add(dealer_addr);

                                String vmakeid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
                                    sDealer_pinpoint.add("http://goserwizz.com/images/pinpoint-" + vmakeid + ".png");

                                  //  System.out.println(tmpObj.get("id"));
                                   // System.out.println(tmpObj.get("compName"));
                                }
                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            SettingsUtils.getInstance().putListString("dealer_latitude", sDealer_lati);
            SettingsUtils.getInstance().putListString("dealer_longitude", sDealer_long);
            SettingsUtils.getInstance().putListString("dealer_compName", sDealer_Name);

            String vmakeid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKEid, "");
            String pinpoint_url = "http://goserwizz.com/images/pinpoint-"+ vmakeid +".png";
            SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DEALER_PINPOINT, pinpoint_url);


            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetDealerByModel().execute();
                    }
                    else {

                        Dealer_Name = new String[sDealer_Name.size()];
                        Dealer_Name = sDealer_Name.toArray(Dealer_Name);

                        spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));
                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetDealerByModel().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }




    private void showResultDialogN(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.dialog_success))
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    private void showResultDialogFailed(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            spinner.setAdapter(new MyAdapterServiceType(getActivity(), R.layout.spinner_row, Dealer_Name));


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
         /*   pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();  */
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            gu_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GU_ID, "");

            nameValuePairs.add(new BasicNameValuePair(TAG_GUID, gu_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getusrdet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                fname = dataObj.get("fname").toString();
                                lname = dataObj.get("lname").toString();
                                mob = dataObj.get("mob").toString();
                                email = dataObj.get("email").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, lname);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);

                             /*   pincode = dataObj.get("pincode").toString();
                                city = dataObj.get("city").toString();
                                location = dataObj.get("location").toString();
                                state = dataObj.get("state").toString();
                                address = dataObj.get("addr").toString();

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOCATION, location);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_ADDRESS, address);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_CITY, city);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_STATE, state);
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PINCODE, pincode);
*/
                                System.out.println(dataObj.get("fname"));
                                System.out.println(dataObj.get("mob"));

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
          /*  if (pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {


                    }
                } else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_WExternalStorage_REQUEST_CODE:

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }


    private boolean checkWriteExternalStoragePermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requestWriteExternalStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            Toast.makeText(getActivity(), "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WExternalStorage_REQUEST_CODE);

        }
    }


}
